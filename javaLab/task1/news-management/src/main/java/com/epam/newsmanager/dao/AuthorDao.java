package com.epam.newsmanager.dao;

import com.epam.newsmanager.entity.Author;

public interface AuthorDao {
	/**
	 * Add news author to database
	 * @return author id in database
	 */
	public Long addAuthor(Author author) throws DaoException;
	/**
	 * Set author as expired
	 */
	public void deleteAuthor(Long idAuthor) throws DaoException;
	/**
	 * Link news and author in database
	 */
	public void linkNewsWithAuthor(Long idAuthor,Long idNews) throws DaoException;
	/**
	 * Destroy link in database between news and author
	 */
	public void detachNewsFromAuthorByNewsId(Long idNews) throws DaoException;
	/**
	 * Get all info about author of certain news
	 * @param idNews news id in database
	 */
	public Author getAuthorByNewsId(Long idNews) throws DaoException;
}
