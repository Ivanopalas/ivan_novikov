package com.epam.newsmanager.dao;

/**
 * Wraps all exceptions thrown in dao layer
 */
public class DaoException extends Exception{
	private static final long serialVersionUID = -1232936267707952533L;
	
	public DaoException(Exception ex){
        super(ex);
    }
    public DaoException(String message,Exception ex){
        super(message,ex);
    }
}
