package com.epam.newsmanager.dao;

import java.util.List;

import com.epam.newsmanager.entity.Tag;

public interface TagDao {
    /**
     * Create new tag in database.
     * @param tag Tag data.
     * @return id of added tag
     */
	public Long addTag(Tag tag) throws DaoException;
    /**
     * Get list of tags by news id which is linked with news.
     * @param idNews id of news in database
     * @return list of tags.
     */
	public List<Tag> getTagsByNewsId(Long idNews) throws DaoException;
    /**
     * 
     * @param idNews
     * @param idTags
     * @throws DaoException
     */
	public void linkTagsWithNews(Long idNews, List<Long> idTags) throws DaoException;
	public void detachTagFromNews(Long idTag,Long idNews) throws DaoException;
	public void detachTagsFromNews(Long idNews) throws DaoException;
}
 