package com.epam.newsmanager.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.util.DataSourceHelper;
import com.epam.newsmanager.entity.Author;

public class DBAuthorDao implements AuthorDao {
	private DataSource dataSource;

	private static final String EXCEPTION_ACT_MESSAGE = "Sql actions exception";

	private static final String INSERT_AUTHOR_QUERY = "INSERT INTO Authors values('',?,'')";
	private static final String DELETE_AUTHOR_QUERY = "UPDATE Authors SET EXPIRED = SYSDATE WHERE AUTHOR_ID = ?";
	private static final String LINK_AUTHOR_AND_NEWS_QUERY = "INSERT INTO News_Authors values(?,?)";
	private static final String DETACH_AUTHOR_AND_NEWS_QUERY = "DELETE FROM News_Authors WHERE NEWS_ID=?";
	private static final String GET_AUTHOR_OF_NEWS_QUERY = "SELECT Authors.AUTHOR_ID,AUTHOR_NAME, EXPIRED "
			+ " FROM Authors JOIN News_Authors ON News_Authors.AUTHOR_ID=Authors.AUTHOR_ID WHERE NEWS_ID=?";

    public static final String AUTHOR_ID = "AUTHOR_ID";
    public static final String AUTHOR_NAME = "AUTHOR_NAME";
    public static final String EXPIRED = "EXPIRED";

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Long addAuthor(Author author) throws DaoException {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
            ps = connection.prepareStatement(INSERT_AUTHOR_QUERY, new String[]{AUTHOR_ID});

            ps.setString(1, author.getName());
            ps.executeUpdate();

			resultSet = ps.getGeneratedKeys();
			Long idAuthor = null;
			if (resultSet.next()) {
				idAuthor = resultSet.getLong(1);
			}
			return idAuthor;

		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, ps, resultSet);
		}
	}

	public void deleteAuthor(Long idAuthor) throws DaoException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
            ps = connection.prepareStatement(DELETE_AUTHOR_QUERY);

            ps.setLong(1, idAuthor);
            ps.executeUpdate();

		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, ps);
		}
	}

	public void linkNewsWithAuthor(Long idAuthor, Long idNews) throws DaoException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
            ps = connection.prepareStatement(LINK_AUTHOR_AND_NEWS_QUERY);

            ps.setLong(1, idAuthor);
            ps.setLong(2, idNews);
            ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, ps);
		}
	}

	public void detachNewsFromAuthorByNewsId(Long idNews) throws DaoException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
            ps = connection.prepareStatement(DETACH_AUTHOR_AND_NEWS_QUERY);

            ps.setLong(1, idNews);
            ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, ps);
		}
	}

	public Author getAuthorByNewsId(Long idNews) throws DaoException {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {

			connection = DataSourceUtils.doGetConnection(dataSource);
            ps = connection.prepareStatement(GET_AUTHOR_OF_NEWS_QUERY);

            ps.setLong(1, idNews);

			resultSet = ps.executeQuery();
			Author author = new Author();

			if (resultSet.next()) {

				String authorName = resultSet.getString(AUTHOR_NAME);
				Timestamp expiredDate = resultSet.getTimestamp(EXPIRED);
				Long idAuthor = resultSet.getLong(AUTHOR_ID);

				author.setExpiredDate(expiredDate);
				author.setName(authorName);
				author.setIdAuthor(idAuthor);
			}
			return author;
		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, ps, resultSet);
		}
	}
}
