package com.epam.newsmanager.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanager.entity.Comment;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.NewsDao;
import com.epam.newsmanager.dao.util.DataSourceHelper;
import com.epam.newsmanager.entity.News;
import com.epam.newsmanager.entity.SearchCriteria;
import com.epam.newsmanager.entity.Tag;

public class DBNewsDao implements NewsDao {
	private DataSource dataSource;

	private static final String EXCEPTION_ACT_MESSAGE = "Sql actions exception";

	private static final String ADD_NEWS_QUERY =
            "INSERT INTO News values('',?,?,?, SYSDATE ,SYSDATE)";
	private static final String UPDATE_NEWS_QUERY =
            "UPDATE News SET TITLE = ? , SHORT_TEXT = ? , FULL_TEXT = ? ," +
                    " MODIFICATION_DATE = SYSDATE WHERE NEWS_ID = ?";

	private static final String DELETE_NEWS_QUERY = "DELETE FROM News WHERE NEWS_ID=?";

	private static final String GET_SINGLE_NEWS_QUERY =
            "SELECT TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE," +
                    " MODIFICATION_DATE FROM News WHERE NEWS_ID=?";

	private static final String GET_NEWS_NUMBER_QUERY = "SELECT count(*) as COUNT from News";

	private static final String GET_SORTED_NEWS_DATA_QUERY =
            "SELECT DISTINCT News.NEWS_ID, News.TITLE, News.SHORT_TEXT,"
			+ " News.FULL_TEXT, News.CREATION_DATE, News.MODIFICATION_DATE, Tags.TAG_ID, Tags.TAG_NAME,"
			+ " Authors.AUTHOR_ID, Authors.AUTHOR_NAME, Authors.EXPIRED,"
			+ " Comments.COMMENT_ID, Comments.COMMENT_TEXT, Comments.CREATION_DATE from News "
			+ "left join News_Tags on News_Tags.NEWS_ID = News.NEWS_ID "
			+ "left join Tags on News_Tags.TAG_ID = Tags.TAG_ID "
			+ "left join Comments on Comments.NEWS_ID = News.NEWS_ID "
			+ "left join  News_Authors on News_Authors.NEWS_ID = News.NEWS_ID "
			+ "left join Authors on News_Authors.AUTHOR_ID = Authors.AUTHOR_ID "
			+ "order by (select count(*) from Comments where Comments.NEWS_ID = News.NEWS_ID) DESC, "
			+ "News.MODIFICATION_DATE DESC";

	private static final String SEARCH_BY_AUTHOR_QUERY = 
			"SELECT n.NEWS_ID,TITLE,SHORT_TEXT, FULL_TEXT,"
			+ " CREATION_DATE, MODIFICATION_DATE from News n"
			+ " JOIN News_Authors na on na.News_ID=n.NEWS_ID where AUTHOR_ID = ?";
	
	private static final String SEARCH_BY_TAGS_QUERY_NOT_FULL = 
			"SELECT n.NEWS_ID,TITLE,SHORT_TEXT, FULL_TEXT,"
			+ " CREATION_DATE, MODIFICATION_DATE from News n"
			+ " JOIN News_Tags nt on nt.NEWS_ID = n.NEWS_ID where TAG_ID IN ";
	
	private static final String SEARCH_BY_TAGS_AND_AUTHORS_QUERY_NOT_FULL = 
			"SELECT n.NEWS_ID,TITLE,SHORT_TEXT, FULL_TEXT,"
			+ " CREATION_DATE, MODIFICATION_DATE from News n"
			+ " JOIN News_Tags nt on nt.NEWS_ID = n.NEWS_ID"
			+ " JOIN News_Authors na on na.News_ID=n.NEWS_ID"
			+ " where AUTHOR_ID = ? AND TAG_ID IN ";

	private static final String GET_NEWS_QUERY = 
			"SELECT n.NEWS_ID,TITLE,SHORT_TEXT, FULL_TEXT,"
			+ " n.CREATION_DATE, MODIFICATION_DATE from News n"
			+ " left join Comments on Comments.NEWS_ID = n.NEWS_ID "
			+ "order by (select count(*) from Comments where Comments.NEWS_ID = n.NEWS_ID) DESC, "
			+ "n.MODIFICATION_DATE DESC";


    private static final String SEARCH_PARAMETERS_QUERY_PART =
            "SELECT DISTINCT n.NEWS_ID,TITLE,SHORT_TEXT, FULL_TEXT,"
                    + " CREATION_DATE, MODIFICATION_DATE from News n";

    private static final String AUTHOR_JOIN_SEARCH_QUERY_PART =
            " JOIN News_Authors na on na.News_ID = n.NEWS_ID";
    private static final String TAG_JOIN_SEARCH_QUERY_PART =
            " JOIN News_Tags nt on nt.NEWS_ID = n.NEWS_ID";
    private static final String WHERE_QUERY_PART =
            " WHERE ";
    private static final String AND_QUERY_PART =
            " AND ";
    private static final String AUTHOR_WHERE_SEARCH_QUERY_PART =
            " AUTHOR_ID = ";
    private static final String TAGS_WHERE_SEARCH_QUERY_PART =
            " TAG_ID IN ";

    public static final String NEWS_ID = "NEWS_ID";
    public static final String TITLE = "TITLE";
    public static final String SHORT_TEXT = "SHORT_TEXT";
    public static final String FULL_TEXT = "FULL_TEXT";
    public static final String CREATION_DATE = "CREATION_DATE";
    public static final String MODIFICATION_DATE = "MODIFICATION_DATE";

    public static final String NEWS_COUNT = "COUNT";

    public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Long addNews(News news) throws DaoException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(ADD_NEWS_QUERY, new String[] { NEWS_ID });

			statement.setString(1, news.getTitle());
			statement.setString(2, news.getShortText());
			statement.setString(3, news.getFullText());

			statement.executeUpdate();

			resultSet = statement.getGeneratedKeys();
			Long idNews = null;
			if (resultSet.next()) {
				idNews = resultSet.getLong(1);
			}
			return idNews;
		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, statement, resultSet);
		}
	}

	public void editNews(News news) throws DaoException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(UPDATE_NEWS_QUERY);

			statement.setString(1, news.getTitle());
			statement.setString(2, news.getShortText());
			statement.setString(3, news.getFullText());
			statement.setLong(4, news.getIdNews());

			statement.executeUpdate();

		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, statement);
		}
	}

	public void deleteNews(Long idNews) throws DaoException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(DELETE_NEWS_QUERY);

			statement.setLong(1, idNews);
			statement.executeUpdate();

		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, statement);
		}
	}

    public List<News> getSortedNews() throws DaoException{
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(GET_NEWS_QUERY);

            resultSet = statement.executeQuery();
            List<News> allNews = new ArrayList<News>();
            while (resultSet.next()) {
                News news = new News();

                news.setIdNews(resultSet.getLong(NEWS_ID));
                news.setTitle(resultSet.getString(TITLE));
                news.setShortText(resultSet.getString(SHORT_TEXT));
                news.setFullText( resultSet.getString(FULL_TEXT));
                news.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
                news.setModificationDate(resultSet.getDate(MODIFICATION_DATE));
                allNews.add(news);
            }
            return allNews;
        } catch (SQLException ex) {
            throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
        } finally {
            DataSourceHelper.closeConnection(connection, dataSource, statement, resultSet);
        }
    }

	public News getSingleNews(Long idNews) throws DaoException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(GET_SINGLE_NEWS_QUERY);

			statement.setLong(1, idNews);

			resultSet = statement.executeQuery();
			News news = new News();
			if (resultSet.next()) {
				news.setIdNews(idNews);
				news.setTitle(resultSet.getString(TITLE));
				news.setShortText(resultSet.getString(SHORT_TEXT));
				news.setFullText(resultSet.getString(FULL_TEXT));
				news.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
				news.setModificationDate(resultSet.getDate(MODIFICATION_DATE));
			}
			return news;
		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, statement, resultSet);
		}
	}

    public Long countNews() throws DaoException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(GET_NEWS_NUMBER_QUERY);
            resultSet = statement.executeQuery();
            Long numberOfNews = null;
            if (resultSet.next()) {
                numberOfNews = resultSet.getLong(NEWS_COUNT);
            }
            return numberOfNews;
        } catch (SQLException ex) {
            throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
        } finally {
            DataSourceHelper.closeConnection(connection, dataSource, statement, resultSet);
        }
    }

    public List<News> searchNews(SearchCriteria criteria) throws DaoException{
        Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
            ps = buildStatement(connection,criteria);
            resultSet = ps.executeQuery();

            List<News> searchedNews = new ArrayList<News>();

            while (resultSet.next()) {
                News news = new News();

                news.setIdNews(resultSet.getLong(NEWS_ID));
                news.setTitle(resultSet.getString(TITLE));
                news.setShortText(resultSet.getString(SHORT_TEXT));
                news.setFullText(resultSet.getString(FULL_TEXT));
                news.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
                news.setModificationDate(resultSet.getDate(MODIFICATION_DATE));

                searchedNews.add(news);
            }
			return searchedNews;
		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, ps, resultSet);
		}
    }

    private PreparedStatement buildStatement(
            Connection con, SearchCriteria criteria) throws SQLException{

        String sqlQuery = buildSqlQuery(criteria);
        PreparedStatement ps = null;
        if(sqlQuery == GET_NEWS_QUERY){
            return con.prepareStatement(sqlQuery);
        }else{
            ps = con.prepareStatement(sqlQuery);
        }
        int setIndex = 1;

        if(criteria.getAuthor() != null){
            Long authorId = criteria.getAuthor().getIdAuthor();
            ps.setLong(setIndex++, authorId);
        }
        if(criteria.getTags() != null && criteria.getTags().size() != 0) {
            List<Tag> tags = criteria.getTags();
            for (Tag tag : tags) {
                ps.setLong(setIndex++, tag.getIdTag());
            }
        }
        return ps;
    }
    private String buildSqlQuery(SearchCriteria criteria){
        StringBuilder searchQueryFirstPart = new StringBuilder(SEARCH_PARAMETERS_QUERY_PART);
        StringBuilder searchQueryLastPart = new StringBuilder(WHERE_QUERY_PART);

        Boolean previousCriteriaAdded = false;

        if(criteria.getAuthor() != null){
            searchQueryFirstPart.append(AUTHOR_JOIN_SEARCH_QUERY_PART);

            if(previousCriteriaAdded) {
                searchQueryLastPart.append(AND_QUERY_PART);
            }
            searchQueryLastPart.append(AUTHOR_WHERE_SEARCH_QUERY_PART);
            searchQueryLastPart.append("?");

            previousCriteriaAdded = true;
        }
        if(criteria.getTags() != null && criteria.getTags().size() != 0){
            searchQueryFirstPart.append(TAG_JOIN_SEARCH_QUERY_PART);

            if(previousCriteriaAdded) {
                searchQueryLastPart.append(AND_QUERY_PART);
            }
            searchQueryLastPart.append(TAGS_WHERE_SEARCH_QUERY_PART);

            searchQueryLastPart.append("( ");

            List<Tag> tags = criteria.getTags();
            Integer tagsAmount = tags.size();
            String prefix = "";
            for(int i = 0 ; i < tagsAmount ; i++){
                searchQueryLastPart.append(prefix);
                prefix = ",";
                searchQueryLastPart.append("?");
            }
            searchQueryLastPart.append(" )");
            previousCriteriaAdded = true;
        }

        if(!previousCriteriaAdded){
           return GET_NEWS_QUERY;
        }

        searchQueryFirstPart.append(searchQueryLastPart);
        return searchQueryFirstPart.toString();
    }
//	private PreparedStatement searchByAuthorOnly(Connection con, SearchCriteria criteria) throws SQLException{
//		PreparedStatement ps = con.prepareStatement(SEARCH_BY_AUTHOR_QUERY);
//		Long idAuthor = criteria.getAuthor().getIdAuthor();
//		ps.setLong(1,idAuthor);
//		return ps;
//	}
//	private PreparedStatement searchByTagsOnly(Connection con, SearchCriteria criteria) throws SQLException{
//		StringBuilder sb = new StringBuilder();
//		List<Tag> tags = criteria.getTags();
//		sb.append(SEARCH_BY_TAGS_QUERY_NOT_FULL);
//
//		sb.append("( ");
//
//		String prefix = "";
//		for(Tag tag: tags){
//			sb.append(prefix);
//			prefix = ",";
//			sb.append(tag.getTagName());
//		}
//		sb.append(" )");
//
//		PreparedStatement ps = con.prepareStatement(sb.toString());
//		return ps;
//	}
//	private PreparedStatement seachByAuthorAndTags(Connection con, SearchCriteria criteria) throws SQLException{
//		StringBuilder sb = new StringBuilder();
//		List<Tag> tags = criteria.getTags();
//		sb.append(SEARCH_BY_TAGS_AND_AUTHORS_QUERY_NOT_FULL);
//
//		sb.append("( ");
//
//		String prefix = "";
//		for(Tag tag: tags){
//			sb.append(prefix);
//			prefix = ",";
//			sb.append(tag.getTagName());
//		}
//		sb.append(" )");
//
//		PreparedStatement ps = con.prepareStatement(sb.toString());
//		Long idAuthor = criteria.getAuthor().getIdAuthor();
//		ps.setLong(1, idAuthor);
//		return ps;
//	}
//	private PreparedStatement getAllNews(Connection con, SearchCriteria criteria) throws SQLException{
//		PreparedStatement ps = con.prepareStatement(GET_NEWS_QUERY);
//		return ps;
//	}
    //	public List<News> searchNews(SearchCriteria criteria) throws DaoException {
//		Connection connection = null;
//		PreparedStatement statement = null;
//		ResultSet resultSet = null;
//		try {
//			connection = DataSourceUtils.doGetConnection(dataSource);
//
//			if(criteria.getTags() == null){
//				if(criteria.getAuthor() == null){
//					statement = getAllNews(connection,criteria);
//				}else{
//					statement = searchByAuthorOnly(connection,criteria);
//				}
//			}else{
//				if(criteria.getAuthor() == null){
//					statement = searchByTagsOnly(connection,criteria);
//				}else{
//					statement = seachByAuthorAndTags(connection,criteria);
//				}
//			}
//			resultSet = statement.executeQuery();
//
//			List<News> allNews = new ArrayList<News>();
//			while (resultSet.next()) {
//				News news = new News();
//
//				news.setIdNews(resultSet.getLong(NEWS_ID));
//				news.setTitle(resultSet.getString(TITLE));
//				news.setShortText(resultSet.getString(SHORT_TEXT));
//				news.setFullText(resultSet.getString(FULL_TEXT));
//				news.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
//				news.setModificationDate(resultSet.getDate(MODIFICATION_DATE));
//
//				allNews.add(news);
//			}
//			return allNews;
//		} catch (SQLException ex) {
//			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
//		} finally {
//			DataSourceHelper.closeConnection(connection, dataSource, statement, resultSet);
//		}
//	}
}
