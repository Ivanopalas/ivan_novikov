package com.epam.newsmanager.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.dao.util.DataSourceHelper;
import com.epam.newsmanager.entity.Tag;

public class DBTagDao implements TagDao{

	private DataSource dataSource;

	private static final String EXCEPTION_ACT_MESSAGE = "Sql actions exception";
	private static final String INSERT_TAG_QUERY = "INSERT INTO Tags values('',?)";
	private static final String GET_TAGS_QUERY = 
			"SELECT Tags.TAG_ID,TAG_NAME "
			+ " FROM Tags JOIN News_Tags ON News_Tags.TAG_ID=Tags.TAG_ID WHERE NEWS_ID=?";
	private static final String DETACH_TAG_AND_NEWS_QUERY = "DELETE FROM News_Tags WHERE NEWS_ID=?";
	private static final String DETACH_SINGLE_TAG_AND_NEWS_QUERY = "DELETE FROM News_Tags WHERE NEWS_ID=? AND TAG_ID=?";
	private static final String LINK_TAG_AND_NEWS_QUERY = "INSERT INTO News_tags values(?,?)";


    public static final String TAG_ID = "TAG_ID";
    public static final String TAG_NAME = "TAG_NAME";

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Long addTag(Tag tag) throws DaoException {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			ps = connection.prepareStatement(INSERT_TAG_QUERY, new String[] { TAG_ID });

			ps.setString(1, tag.getTagName());
			ps.executeUpdate();

			resultSet = ps.getGeneratedKeys();
			Long idAuthor = null;
			if (resultSet.next()) {
				idAuthor = resultSet.getLong(1);
			}
			return idAuthor;

		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, ps, resultSet);
		}
	}

	public List<Tag> getTagsByNewsId(Long idNews) throws DaoException {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			ps = connection.prepareStatement(GET_TAGS_QUERY);
			ps.setLong(1, idNews);

			resultSet = ps.executeQuery();
			
			List<Tag> tags = new ArrayList<Tag>();
			while (resultSet.next()) {
				Tag tag = new Tag();

				String tagName = resultSet.getString(TAG_NAME);
				Long idTag = resultSet.getLong(TAG_ID);

				tag.setIdTag(idTag);
				tag.setTagName(tagName); 
                
				tags.add(tag);
            }
			return tags;
		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, ps, resultSet);
		}
	}

	public void linkTagsWithNews(Long idNews, List<Long> idTags) throws DaoException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			ps = connection.prepareStatement(LINK_TAG_AND_NEWS_QUERY);

			for (Long idTag: idTags) {
			    ps.setLong(1,idNews);
			    ps.setLong(2, idTag);
			    ps.addBatch();
			}
			ps.executeBatch();
			
		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, ps);
		}
	}

//    @Deprecated
//	public List<Long> addTags(List<Tag> tags) throws DaoException {
//		Connection connection = null;
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		try {
//			connection = DataSourceUtils.doGetConnection(dataSource);
//			ps = connection.prepareStatement(INSERT_TAG_QUERY,new String[] {TAG_ID});
//			List<Long> idTags = new ArrayList<Long>();
//			for (Tag tag: tags) {
//			    ps.setString(1, tag.getTagName());
//			    ps.executeUpdate();
//
//				rs  = ps.getGeneratedKeys();
//				if (rs.next()) {
//					idTags.add(rs.getLong(1));
//				}
//			}
//			return idTags;
//		} catch (SQLException ex) {
//			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
//		} finally {
//			DataSourceHelper.closeConnection(connection, dataSource, ps,rs);
//		}
//	}

	public void detachTagFromNews(Long idTag, Long idNews) throws DaoException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			ps = connection.prepareStatement(DETACH_SINGLE_TAG_AND_NEWS_QUERY);

			ps.setLong(1, idNews);
			ps.setLong(2, idTag);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, ps);
		}
	}

	public void detachTagsFromNews(Long idNews) throws DaoException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			ps = connection.prepareStatement(DETACH_TAG_AND_NEWS_QUERY);

			
			ps.setLong(1, idNews);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
		} finally {
			DataSourceHelper.closeConnection(connection, dataSource, ps);
		}
	}
}
