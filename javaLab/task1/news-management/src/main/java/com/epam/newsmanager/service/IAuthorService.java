package com.epam.newsmanager.service;

import com.epam.newsmanager.entity.Author;

public interface IAuthorService {
	/**
	 * Add news author to database
	 * @return author id in database
	 */
	public Long addAuthor(Author author) throws ServiceException;
	/**
	 * Set author as expired
	 */
	public void deleteAuthor(Long idAuthor) throws ServiceException;
	/**
	 * Link news and author in database
	 */
	public void linkWithAuthor(Long idAuthor,Long idNews) throws ServiceException;
	/**
	 * Destroy link in database between news and author
	 */
	public void detachNews(Long idNews) throws ServiceException;
	/**
	 * Get author by news id
	 * @param idNews id of news in database
	 */ 
	public Author getAuthorOfNews(Long idNews) throws ServiceException;
}
