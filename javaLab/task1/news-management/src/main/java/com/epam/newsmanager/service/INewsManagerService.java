package com.epam.newsmanager.service;

import java.util.List;

import com.epam.newsmanager.entity.Author;
import com.epam.newsmanager.entity.Comment;
import com.epam.newsmanager.entity.News;
import com.epam.newsmanager.entity.NewsData;
import com.epam.newsmanager.entity.SearchCriteria;
import com.epam.newsmanager.entity.Tag;

public interface INewsManagerService {
	/**
	 * Add new news and link it with tags and author
	 * 
	 * @param news All news data to add
	 * @return id of added news
	 */
	public Long addNews(final NewsData news) throws ServiceException;

	/**
	 * Edit already situated news
	 * 
	 * @param news just news part, without changing comments author and so on to edit
	 */
	public void editNews(final News news) throws ServiceException;

	/**
	 * Delete news and detach all links in database with current news
	 */
	public void deleteNews(final Long idNews) throws ServiceException;

	/**
	 * Get news sorted by comments number and modification date
	 * 
	 * @return list of sorted news
	 */
	public List<NewsData> getSortedNews() throws ServiceException;

	/**
	 * Get news by id
	 */
	public NewsData getNewsByNewsId(final Long idNews) throws ServiceException;

	/**
	 * Add new author in database
	 * 
	 * @return id of added author
	 */
	public Long addAuthor(final Author author) throws ServiceException;

	/**
	 * Set current author as expired
	 */
	public void deleteAuthor(final Long idAuthor) throws ServiceException;

	/**
	 * Add comment to certain news
	 * 
	 * @return comment id
	 */
	public Long addComment(final Comment comment) throws ServiceException;

	/**
	 * Get list of news sorted by criteria
	 */
	public List<News> searchNews(final SearchCriteria criteria) throws ServiceException;

	/**
	 * Add new tag in database
	 * 
	 * @return id of added tag
	 */
	public Long addTag(final Tag tag) throws ServiceException;

	/**
	 * Link tags and news in database. Make dependence between two tables
	 */
	public void attachTagToNews(final Long news, final List<Long> idTags) throws ServiceException;

	/**
	 * Delete single comment in database
	 */
	public void deleteSingleComment(final Long idComment) throws ServiceException;

	/**
	 * Count all news in database
	 */
	public Long countAllNews() throws ServiceException;
}
