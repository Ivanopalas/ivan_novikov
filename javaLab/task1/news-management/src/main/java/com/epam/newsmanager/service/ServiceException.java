package com.epam.newsmanager.service;

public class ServiceException extends Exception{

	private static final long serialVersionUID = 889508310181708305L;
	
	public ServiceException(Exception ex){
        super(ex);
    }
    public ServiceException(String message,Exception ex){
        super(message,ex);
    }
}
