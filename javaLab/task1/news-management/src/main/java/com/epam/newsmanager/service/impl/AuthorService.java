package com.epam.newsmanager.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.entity.Author;
import com.epam.newsmanager.service.IAuthorService;
import com.epam.newsmanager.service.ServiceException;

public class AuthorService implements IAuthorService{
	private final static Logger logger = LogManager.getLogger(AuthorService.class);
	
	private final static String linkNewsErrorMessage = "Can't link news with author";
	private final static String detachNewsErrorMessage = "Can't detach news and author";
	
	private AuthorDao authorDao;
	
	
	public AuthorDao getAuthorDao() {
		return authorDao;
	}

	public void setAuthorDao(AuthorDao authorDao) {
		this.authorDao = authorDao;
	}

	public Long addAuthor(Author author) throws ServiceException {
		Long idAuthor = null;
		try{
			idAuthor = authorDao.addAuthor(author);
		}catch(DaoException ex){
			throw new ServiceException(ex);
		}
		return idAuthor;
	}

	public void deleteAuthor(Long idAuthor) throws ServiceException {
		try {
			authorDao.deleteAuthor(idAuthor);
		} catch (DaoException ex) {
			throw new ServiceException(ex);
		}
	}

	public void linkWithAuthor(Long idAuthor, Long idNews) throws ServiceException{
		try{
			authorDao.linkNewsWithAuthor(idAuthor, idNews);
		}catch(DaoException ex){
			logger.error(linkNewsErrorMessage);
			throw new ServiceException(ex);
		}
	}

	public void detachNews(Long idNews) throws ServiceException {
		try{
			authorDao.detachNewsFromAuthorByNewsId(idNews);
		}catch(DaoException ex){
			logger.error(detachNewsErrorMessage);
			throw new ServiceException(ex);
		}
		
	}

	public Author getAuthorOfNews(Long idNews) throws ServiceException {
		Author author = null;
		try{
			author = authorDao.getAuthorByNewsId(idNews);
		}catch(DaoException ex){
			logger.error(detachNewsErrorMessage);
			throw new ServiceException(ex);
		}
		return author;
	}



}
