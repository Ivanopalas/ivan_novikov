package com.epam.newsmanager.service.impl;

import java.util.List;

import com.epam.newsmanager.dao.CommentDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.entity.Comment;
import com.epam.newsmanager.service.ICommentService;
import com.epam.newsmanager.service.ServiceException;

public class CommentService implements ICommentService {

	private CommentDao commentDao;

	public CommentDao getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

	public Long addComment(Comment comment) throws ServiceException {
		Long idComment = null;
		try {
			idComment = commentDao.addComment(comment);
		} catch (DaoException ex) {
			throw new ServiceException(ex);
		}
		return idComment;

	}

	public void deleteCommentsFromNews(Long idNews) throws ServiceException {
		try {
			commentDao.deleteAllCommentsFromNews(idNews);
		} catch (DaoException ex) {
			throw new ServiceException(ex);
		}

	}

	public void deleteComment(Long idComment) throws ServiceException {
		try {
			commentDao.deleteComment(idComment);
		} catch (DaoException ex) {
			throw new ServiceException(ex);
		}
	}

	public List<Comment> getComments(Long idNews) throws ServiceException {
		List<Comment> comments = null;
		try {
			comments =  commentDao.getCommentsByNewsID(idNews);
		} catch (DaoException ex) {
			throw new ServiceException(ex);
		}
		return comments;
	}

	

}
