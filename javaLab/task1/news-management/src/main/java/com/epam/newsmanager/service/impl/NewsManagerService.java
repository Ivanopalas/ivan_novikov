package com.epam.newsmanager.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanager.entity.Author;
import com.epam.newsmanager.entity.Comment;
import com.epam.newsmanager.entity.News;
import com.epam.newsmanager.entity.NewsData;
import com.epam.newsmanager.entity.SearchCriteria;
import com.epam.newsmanager.entity.Tag;
import com.epam.newsmanager.service.IAuthorService;
import com.epam.newsmanager.service.ICommentService;
import com.epam.newsmanager.service.INewsManagerService;
import com.epam.newsmanager.service.INewsService;
import com.epam.newsmanager.service.ITagService;
import com.epam.newsmanager.service.ServiceException;


@Transactional(rollbackFor=ServiceException.class)
public class NewsManagerService implements INewsManagerService {
	private final static Logger logger = LogManager.getLogger(NewsManagerService.class);

	
	
	private final static String ADD_NEWS_ERROR = "Can't add new news";
	private final static String EDIT_NEWS_ERROR = "Edit news problems";
	private final static String DELETE_NEWS_ERROR = "Can't delete news";
	private final static String GET_NEWS_ERROR = "Can't get news";
	private final static String ADD_AUTHOR_ERROR = "Can't add author";
	private final static String DELETE_AUTHOR_ERROR = "Can't add author";
	private final static String SEARCH_NEWS_ERROR = "Error, can't find news";
	private final static String ADD_TAG_ERROR = "Error, can't add tag";
	private final static String attachTagError = "Error, can't attach tag to news";
	private final static String DELETE_COMMENT_ERROR = "Can't delete comment";
	private final static String COUNT_NEWS_ERROR = "Can't get news number";
	private final static String ADD_COMMENT_ERROR = "Can't add new comment";


	private IAuthorService authorService;
	private ICommentService commentService;
	private INewsService newsService;
	private ITagService tagService;



	public Long addNews(NewsData news) throws ServiceException {
		Long idNews = null;
		try {
			News transferNews = news.getNews();
			idNews = newsService.addNews(transferNews);

			Author author = news.getAuthor();
			Long idAuthor = author.getIdAuthor();

			List<Tag> tags = news.getTags();
			List<Long> idTags = new ArrayList<Long>();
			for(Tag i: tags){
				idTags.add(i.getIdTag());
			}
			tagService.linkWithTags(idNews, idTags);
			authorService.linkWithAuthor(idAuthor, idNews);
		} catch (ServiceException ex) {
			logger.error(ADD_NEWS_ERROR);
			throw ex;
		}
		return idNews;
	}


    public List<NewsData> getSortedNews() throws ServiceException {
        List<NewsData> sortedNewsData = null;
        List<News> sortedNews = null;
        try {
            sortedNews = newsService.getSortedNews();
            sortedNewsData = new ArrayList<NewsData>();

            for(News currentNews : sortedNews){

                NewsData currentNewsData = new NewsData();
                currentNewsData.setNews(currentNews);

                List<Tag> tags = tagService.getTags(currentNews.getIdNews());
                currentNewsData.setTags(tags);

                List<Comment> comments = commentService.getComments(currentNews.getIdNews());
                currentNewsData.setComments(comments);

                Author author = authorService.getAuthorOfNews(currentNews.getIdNews());
                currentNewsData.setAuthor(author);

                sortedNewsData.add(currentNewsData);
            }
        } catch (ServiceException ex) {
            logger.error(DELETE_NEWS_ERROR);
            throw ex;
        }
        return sortedNewsData;
    }
	public void editNews(News news) throws ServiceException {
		try {
			newsService.editNews(news);
		} catch (ServiceException ex) {
			logger.error(EDIT_NEWS_ERROR);
			throw ex;
		}
	}

	public void deleteNews(Long idNews) throws ServiceException {
		try {
			commentService.deleteCommentsFromNews(idNews);
			tagService.detachTags(idNews);
			authorService.detachNews(idNews);
			newsService.deleteNews(idNews);
		} catch (ServiceException ex) {
			logger.error(DELETE_NEWS_ERROR);
			throw ex;
		}
	}
	
	
	public NewsData getNewsByNewsId(Long idNews) throws ServiceException {
		try {
			NewsData newsData = new NewsData();
			News news = newsService.getNews(idNews);
			List<Comment> comments = commentService.getComments(idNews);
			Author author = authorService.getAuthorOfNews(idNews);
			List<Tag> tags = tagService.getTags(idNews);
			
			newsData.setNews(news);
			newsData.setAuthor(author);
			newsData.setTags(tags);
			newsData.setComments(comments);
			
			return newsData;
		} catch (ServiceException ex) {
			logger.error(GET_NEWS_ERROR);
			throw ex;
		}
	}

	public Long addAuthor(Author author) throws ServiceException {
		Long idAuthor = null;
		try {
			idAuthor = authorService.addAuthor(author);
		} catch (ServiceException ex) {
			logger.error(ADD_AUTHOR_ERROR);
			throw ex;
		}
		return idAuthor;
	}

	public List<News> searchNews(SearchCriteria criteria) throws ServiceException {
		List<News> sortedNews = null;
		try {
			sortedNews = newsService.searchByCriteria(criteria);
		} catch (ServiceException ex) {
			logger.error(SEARCH_NEWS_ERROR);
			throw ex;
		}
		return sortedNews;
	}

	public Long addTag(Tag tag) throws ServiceException {
		Long idTag = null;
		try {
			idTag = tagService.addTag(tag);
		} catch (ServiceException ex) {
			logger.error(ADD_TAG_ERROR);
			throw ex;
		}
		return idTag;
	}

	public void attachTagToNews(Long idNews, List<Long> idTags) throws ServiceException {
		try {
			tagService.linkWithTags(idNews, idTags);
		} catch (ServiceException ex) {
			logger.error(attachTagError);
			throw ex;
		}
	}

	public void deleteSingleComment(Long idComment) throws ServiceException {
		try {
			commentService.deleteComment(idComment);
		} catch (ServiceException ex) {
			logger.error(DELETE_COMMENT_ERROR);
			throw ex;
		}
	}

	public Long countAllNews() throws ServiceException {
		Long newsLength = null;
		try {
			newsLength = newsService.countNews();
		} catch (ServiceException ex) {
			logger.error(COUNT_NEWS_ERROR);
			throw ex;
		}
		return newsLength;
	}

	public Long addComment(Comment comment) throws ServiceException {
		try {
			commentService.addComment(comment);
		} catch (ServiceException ex) {
			logger.error(ADD_COMMENT_ERROR);
			throw ex;
		}
		return null;
	}

	public void deleteAuthor(Long idAuthor) throws ServiceException {
		try {
			authorService.deleteAuthor(idAuthor);
		} catch (ServiceException ex) {
			logger.error(DELETE_AUTHOR_ERROR);
			throw ex;
		}

	}


    public IAuthorService getAuthorService() {
        return authorService;
    }

    public void setAuthorService(IAuthorService authorService) {
        this.authorService = authorService;
    }

    public ICommentService getCommentService() {
        return commentService;
    }

    public void setCommentService(ICommentService commentService) {
        this.commentService = commentService;
    }

    public INewsService getNewsService() {
        return newsService;
    }

    public void setNewsService(INewsService newsService) {
        this.newsService = newsService;
    }

    public ITagService getTagService() {
        return tagService;
    }

    public void setTagService(ITagService tagService) {
        this.tagService = tagService;
    }


}
