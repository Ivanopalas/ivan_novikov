package com.epam.newsmanager.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.NewsDao;
import com.epam.newsmanager.entity.News;
import com.epam.newsmanager.entity.NewsData;
import com.epam.newsmanager.entity.SearchCriteria;
import com.epam.newsmanager.service.INewsService;
import com.epam.newsmanager.service.ServiceException;

public class NewsService implements INewsService{
	private final static Logger logger = LogManager.getLogger(NewsService.class);
	
	private final static String ADD_NEWS_ERROR = "can't add news";
	private final static String DELETE_NEWS_ERROR = "can't delete news";

	private NewsDao newsDao;

	public NewsDao getNewsDao() {
		return newsDao;
	}

	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}

	public Long addNews(News news) throws ServiceException{
		Long idNews = null;
		try{
			idNews = newsDao.addNews(news);
		}catch (DaoException ex){
			logger.error(ADD_NEWS_ERROR);
			throw new ServiceException(ex);
		}
		return idNews;
	}

	public void editNews(News news) throws ServiceException {
		try{
			newsDao.editNews(news);
		}catch (DaoException ex) {
			throw new ServiceException(ex);
		}
	}

	public void deleteNews(Long idNews)throws ServiceException {
		try{
			newsDao.deleteNews(idNews);
		}catch (DaoException ex) {
			logger.error(DELETE_NEWS_ERROR);
			throw new ServiceException(ex);
		}
	}

	public List<News> getSortedNews() throws ServiceException {
		List<News> sortedNews = null;
		try{
			sortedNews = newsDao.getSortedNews();
		}catch (DaoException ex) {
			throw new ServiceException(ex);
		}
		return sortedNews;
	}

	public News getNews(Long idNews) throws ServiceException {
		News singleNews = null;
		try{
			singleNews = newsDao.getSingleNews(idNews);
		}catch (DaoException ex) {
			throw new ServiceException(ex);
		}
		return singleNews;
	}

	public List<News> searchByCriteria(SearchCriteria criteria) throws ServiceException{
		List<News> searchedNews = null;
		try{
			searchedNews = newsDao.searchNews(criteria);
		}catch(DaoException ex){
			throw new ServiceException(ex);
		}
		return searchedNews;
	}

	public Long countNews() throws ServiceException {
		Long numberOfNews = null;
		try{
			numberOfNews = newsDao.countNews();
		}catch(DaoException ex){
			throw new ServiceException(ex);
		}
		return numberOfNews;
	}

}
