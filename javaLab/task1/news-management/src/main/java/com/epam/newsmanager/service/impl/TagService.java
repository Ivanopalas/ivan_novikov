package com.epam.newsmanager.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.entity.Tag;
import com.epam.newsmanager.service.ITagService;
import com.epam.newsmanager.service.ServiceException;

public class TagService implements ITagService {
	private final static Logger logger = LogManager.getLogger(TagService.class);
	private final static String LINK_TAG_ERROR = "can't link tag with news";
	private final static String DETACH_TAG_ERROR = "can't detach tag from news";

	private TagDao tagDao;

	public TagDao getTagDao() {
		return tagDao;
	}

	public void setTagDao(TagDao tagDao) {
		this.tagDao = tagDao;
	}

	public List<Tag> getTags(Long idNews) throws ServiceException {
		List<Tag> tags = null;
		try {
			tags = tagDao.getTagsByNewsId(idNews);
		} catch (DaoException ex) {
			throw new ServiceException(ex);
		}
		return tags;
	}

	public void linkWithTags(Long idNews, List<Long> tags) throws ServiceException {
		try {
			tagDao.linkTagsWithNews(idNews, tags);
		} catch (DaoException ex) {
			logger.error(LINK_TAG_ERROR);
			throw new ServiceException(ex);
		}
	}

	public void detachTags(Long idNews) throws ServiceException {
		try {
			tagDao.detachTagsFromNews(idNews);
		} catch (DaoException ex) {
			logger.error(DETACH_TAG_ERROR);
			throw new ServiceException(ex);
		}

	}

	public void detachTag(Long idTag, Long idNews) throws ServiceException {
		try {
			tagDao.detachTagFromNews(idTag, idNews);
		} catch (DaoException ex) {
			logger.error(DETACH_TAG_ERROR);
			throw new ServiceException(ex);
		}

	}


	public Long addTag(Tag tag) throws ServiceException {
		Long idTag = null;
		try {
			idTag = tagDao.addTag(tag);
		} catch (DaoException ex) {
			throw new ServiceException(ex);
		}
		return idTag;
	}

}
