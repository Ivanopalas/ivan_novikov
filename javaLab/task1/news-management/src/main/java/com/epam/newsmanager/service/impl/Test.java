package com.epam.newsmanager.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.epam.newsmanager.entity.*;
import org.springframework.context.ApplicationContext;

import com.epam.newsmanager.service.ServiceException;
import com.epam.newsmanager.util.NewsManagerApplicationContext;

public class Test {

	public static void main(String[] args) throws ServiceException {




		ApplicationContext ca = NewsManagerApplicationContext.getApplicationContext();
		// AuthorService ss = (AuthorService) ca.getBean("authorService");
		NewsManagerService ss = (NewsManagerService) ca.getBean("newsManagerService");
		// Comment comment = new Comment();
		// comment.setCommentText("text text text");
		CommentService sas = (CommentService) ca.getBean("commentService");
		// sas.addComment(comment, 1L);
		TagService tag = (TagService) ca.getBean("tagService");
		//System.out.println(ss.countNews());

//        long timer = -System.currentTimeMillis();
//        List<NewsData> aa = ss.getSortedNews();
//        timer += System.currentTimeMillis();
//        System.out.println(timer);
//        for(NewsData a : aa){
//            System.out.println(a);
//        }

        SearchCriteria criteria = new SearchCriteria();

        Author author = new Author();
        author.setIdAuthor(1L);

        List<Tag> tags = new ArrayList<Tag>();

        Tag tag1 = new Tag();
        tag1.setIdTag(1L);

        Tag tag2 = new Tag();
        tag2.setIdTag(2L);

        tags.add(tag1);
        tags.add(tag2);

   //     criteria.setTags(tags);
   //     criteria.setAuthor(author);

        List<News> news = ss.searchNews(criteria);
        System.out.println(news);
    }
}
