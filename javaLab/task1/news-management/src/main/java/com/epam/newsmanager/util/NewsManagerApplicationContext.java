package com.epam.newsmanager.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class NewsManagerApplicationContext {

	private static final ApplicationContext CONTEXT = new ClassPathXmlApplicationContext("springConfiguration.xml");

	public static ApplicationContext getApplicationContext() {
		return CONTEXT;
	}
}
