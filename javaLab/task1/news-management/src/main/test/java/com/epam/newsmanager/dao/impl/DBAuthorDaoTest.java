package com.epam.newsmanager.dao.impl;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.util.DataSourceHelper;
import com.epam.newsmanager.entity.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

import static org.junit.Assert.*;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {
        "/spring/springConfigurationTest.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@Transactional
public class DBAuthorDaoTest {
    private final static String AUTHOR_COUNT_QUERY = "select count(1) from Authors";
    private final static String CHECK_IS_EXPIRED_AUTHOR_QUERY = "select EXPIRED from Authors where AUTHOR_ID = ?";
    private final static String CHECK_IS_LINKED_NEWS_AND_AUTHOR_QUERY =
            "select NEWS_ID from News_Authors where AUTHOR_ID = ?";

    @Autowired
    private DBAuthorDao authorDao;
    @Autowired
    private DataSource dataSource;

    @Test
    public void addAuthorTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);

        Statement st = con.createStatement();
        ResultSet rs = null;

        rs = st.executeQuery(AUTHOR_COUNT_QUERY);

        rs.next();
        int authorsCountBefore = rs.getInt(1);

        Author author = new Author();
        author.setName("test author");
        authorDao.addAuthor(author);

        rs = st.executeQuery(AUTHOR_COUNT_QUERY);
        int authorsCountAfter = -1;
        if(rs.next()) {
            authorsCountAfter = rs.getInt(1);
        }

        assertEquals(authorsCountBefore + 1, authorsCountAfter);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }

    @DatabaseSetup(value = "/testdata/author/singleAuthorData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void deleteAuthorTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);

        Long testId = 1L;

        authorDao.deleteAuthor(testId);

        PreparedStatement st = con.prepareStatement(CHECK_IS_EXPIRED_AUTHOR_QUERY);
        st.setLong(1, testId);
        ResultSet rs;
        rs = st.executeQuery();
        rs.next();
        Timestamp authorExpiredDate =  rs.getTimestamp(1);

        assertNotNull(authorExpiredDate);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }

    @DatabaseSetup(value = "/testdata/author/detachedAuthorAndNewsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void linkNewsWithAuthorTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);

        Long testId = 1L;

        authorDao.linkNewsWithAuthor(testId, testId);

        PreparedStatement st = con.prepareStatement(CHECK_IS_LINKED_NEWS_AND_AUTHOR_QUERY);
        st.setLong(1, testId);
        ResultSet rs;
        rs = st.executeQuery();
        rs.next();
        Long idNews =  rs.getLong(1);

        assertEquals(testId, idNews);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }

    @DatabaseSetup(value = "/testdata/author/linkedAuthorAndNewsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void detachNewsFromAuthorByNewsIdTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);
        Long testId = 1L;
        authorDao.detachNewsFromAuthorByNewsId(testId);

        PreparedStatement st = con.prepareStatement(CHECK_IS_LINKED_NEWS_AND_AUTHOR_QUERY);
        st.setLong(1, testId);
        ResultSet rs;
        rs = st.executeQuery();
        Long idNews = null;
        if(rs.next()) {
            idNews =  rs.getLong(1);
        }
        assertNull(idNews);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }

    @DatabaseSetup(value = "/testdata/author/linkedAuthorAndNewsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void getAuthorOfNewsTest() throws DaoException,SQLException{
        Long testId = 1L;

        Author author = authorDao.getAuthorByNewsId(testId);

        Long actualId = author.getIdAuthor();
        String actualName = author.getName();

        String expectedName = "test author";
        Long expectedId = 1L;

        assertEquals(expectedId, actualId);
        assertEquals(expectedName, actualName);
    }
}
