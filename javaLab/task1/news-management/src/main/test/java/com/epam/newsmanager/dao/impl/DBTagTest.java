package com.epam.newsmanager.dao.impl;

import static org.junit.Assert.*;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.util.DataSourceHelper;
import com.epam.newsmanager.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"/spring/springConfigurationTest.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@Transactional
public class DBTagTest {

    private final static String TAG_COUNT_QUERY = "select count(1) from Tags";

    private final static String CHECK_IS_NEWS_AND_TAGS_LINKED_QUERY =
            "select NEWS_ID from News_Tags where TAG_ID IN (?,?,?,?)";

    private final static String CHECK_IS_NEWS_AND_TAG_LINKED_QUERY =
            "select NEWS_ID from News_Tags where TAG_ID = ?";

    @Autowired
    private DBTagDao tagDao;
    @Autowired
    private DataSource dataSource;

    @Test
    public void addTagTest() throws DaoException,SQLException {
        Connection con = DataSourceUtils.doGetConnection(dataSource);

        Statement st = con.createStatement();
        ResultSet rs = null;

        rs = st.executeQuery(TAG_COUNT_QUERY);

        rs.next();
        int authorsCountBefore = rs.getInt(1);

        Tag tag = new Tag();
        tag.setTagName("test tag");
        tagDao.addTag(tag);

        rs = st.executeQuery(TAG_COUNT_QUERY);
        rs.next();
        int authorsCountAfter = rs.getInt(1);

        assertEquals(authorsCountBefore + 1, authorsCountAfter);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }

    @DatabaseSetup(value = "/testdata/tag/linkedTagsAndNewsData.xml", type= DatabaseOperation.INSERT)
    @Test
    public void getTagsByNewsIdTest() throws DaoException,SQLException{

        Long testId = 1L;

        List<Tag> tags = tagDao.getTagsByNewsId(testId);

        Integer expectedLength = 4;
        Integer actualLength = tags.size();
        assertEquals(expectedLength, actualLength);
    }

    @DatabaseSetup(value = "/testdata/tag/detachedTagsAndNewsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void linkTagWithNewsTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);

        Long testId = 1L;

        List<Long> tags = new ArrayList<Long>();

        Long firstTagId = 1L;
        Long secondTagId = 2L;
        Long thirdTagId = 3L;
        Long fourthTagId = 4L;

        tags.add(firstTagId);
        tags.add(secondTagId);
        tags.add(thirdTagId);
        tags.add(fourthTagId);

        tagDao.linkTagsWithNews(testId, tags);

        PreparedStatement st = con.prepareStatement(CHECK_IS_NEWS_AND_TAGS_LINKED_QUERY);
        st.setLong(1,firstTagId);
        st.setLong(2,secondTagId);
        st.setLong(3,thirdTagId);
        st.setLong(4,fourthTagId);

        ResultSet rs;
        rs = st.executeQuery();
        if(rs.next()) {
            assertEquals(testId, new Long(rs.getLong(1)));
        }
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }

    @DatabaseSetup(value = "/testdata/tag/linkedTagsAndNewsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void detachTagFromNewsTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);
        Long testNewsId = 1L;
        Long testTagId = 1L;
        tagDao.detachTagFromNews(testNewsId,testTagId);

        PreparedStatement st = con.prepareStatement(CHECK_IS_NEWS_AND_TAG_LINKED_QUERY);
        st.setLong(1, testTagId);
        ResultSet rs;
        rs = st.executeQuery();
        Long newsId = null;
        if(rs.next()) {
            newsId =  rs.getLong(1);
        }
        assertNull(newsId);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }


    @DatabaseSetup(value = "/testdata/author/linkedAuthorAndNewsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void detachTagsFromNewsTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);

        Long newId = 1L;
        tagDao.detachTagsFromNews(newId);

        PreparedStatement st = con.prepareStatement(CHECK_IS_NEWS_AND_TAG_LINKED_QUERY);
        st.setLong(1, newId);
        ResultSet rs;
        rs = st.executeQuery();
        Long tagId = null;
        if(rs.next()) {
            tagId =  rs.getLong(1);
        }
        assertNull(tagId);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }

}
