package com.epam.newsmanager.service.impl;

import com.sun.org.apache.bcel.internal.generic.NEW;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import com.epam.newsmanager.entity.Author;
import com.epam.newsmanager.entity.Comment;
import com.epam.newsmanager.entity.News;
import com.epam.newsmanager.entity.NewsData;
import com.epam.newsmanager.entity.SearchCriteria;
import com.epam.newsmanager.entity.Tag;
import com.epam.newsmanager.service.ServiceException;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations= {
        "classpath:/springConfiguration.xml"})
@RunWith(MockitoJUnitRunner.class)
public class NewsManagerServiceTest {
	@Mock
	private AuthorService authorService;
	@Mock
	private CommentService commentService;
	@Mock
	private NewsService newsService;
	@Mock
	private TagService tagService;

	@InjectMocks
	private NewsManagerService newsManagerService;

	@Test
	public void addNewsTest() throws ServiceException {
		NewsData newsData = mock(NewsData.class);
		Author author = mock(Author.class);
		News news = newsData.getNews();
		
		when(newsData.getAuthor()).thenReturn(author);
		when(newsData.getNews()).thenReturn(news);
		
		newsManagerService.addNews(newsData);

		verify(newsService).addNews(news);
		verify(tagService).linkWithTags(anyLong(), anyListOf(Long.class));
		verify(authorService).linkWithAuthor(anyLong(), anyLong());
	}

	@Test
	public void editNewsTest() throws ServiceException {
		News news = mock(News.class);

		newsManagerService.editNews(news);

		verify(newsService).editNews(news);
	}

	@Test
	public void deleteNews() throws ServiceException {
		Long idNews = anyLong();
		newsManagerService.deleteNews(idNews);

		verify(authorService).detachNews(idNews);
		verify(commentService).deleteCommentsFromNews(idNews);
		verify(tagService).detachTags(idNews);
		verify(newsService).deleteNews(idNews);
	}

	@Test
	public void getSortedNewsTest() throws ServiceException {
        List<News> newsData = new ArrayList<News>();
        News testNews = mock(News.class);
        newsData.add(testNews);
        when(newsService.getSortedNews()).thenReturn(newsData);
		newsManagerService.getSortedNews();

		verify(newsService).getSortedNews();
        verify(tagService).getTags(anyLong());
        verify(commentService).getComments(anyLong());
        verify(authorService).getAuthorOfNews(anyLong());
	}

	@Test
	public void getNewsByIdTest() throws ServiceException {
		newsManagerService.getNewsByNewsId(anyLong());

		verify(newsService).getNews(anyLong());
	}

	@Test
	public void addAuthor() throws ServiceException {
		Author author = any(Author.class);
		newsManagerService.addAuthor(author);

		verify(authorService).addAuthor(author);
	}

	@Test
	public void searchNewsTest() throws ServiceException {
		SearchCriteria criteria = any(SearchCriteria.class);
		newsManagerService.searchNews(criteria);

		verify(newsService).searchByCriteria(criteria);
	}

	@Test
	public void addTagTest() throws ServiceException {
		Tag tag = any(Tag.class);
		newsManagerService.addTag(tag);

		verify(tagService).addTag(tag);
	}

	@Test
	public void attachTagToNewsTest() throws ServiceException {
		Long idNews = anyLong();
		List<Long> tags = anyListOf(Long.class);
		newsManagerService.attachTagToNews(idNews, tags);

		verify(tagService).linkWithTags(idNews, tags);
	}

	@Test
	public void deleteCommentTest() throws ServiceException {
		Long idComment = anyLong();
		newsManagerService.deleteSingleComment(idComment);

		verify(commentService).deleteComment(idComment);
	}

	@Test
	public void countNewsTest() throws ServiceException {
		newsManagerService.countAllNews();

		verify(newsService).countNews();
	}
	
	@Test
	public void addCommentTest() throws ServiceException {
		Comment comment = mock(Comment.class);
		Long idNews = anyLong();
		newsManagerService.addComment(comment);

		verify(commentService).addComment(comment);
	}

	@Test
	public void deleteAuthorTest() throws ServiceException {
		Long idAuthor = anyLong();
		newsManagerService.deleteAuthor(idAuthor);

		verify(authorService).deleteAuthor(idAuthor);
	}

	
	
	
	
	
	
	
}
