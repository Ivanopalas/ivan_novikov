package com.epam.newsmanager.service.impl;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.NewsDao;
import com.epam.newsmanager.entity.News;
import com.epam.newsmanager.entity.SearchCriteria;
import com.epam.newsmanager.service.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

    @Mock private NewsDao newsDao;

    @InjectMocks private NewsService newsService = new NewsService();

    @Test
    public void addNewsTest() throws ServiceException, DaoException{
        News news = any(News.class);

        newsService.addNews(news);

        verify(newsDao).addNews(news);
    }

    @Test(expected=ServiceException.class)
    public void addNewsExceptionTest() throws ServiceException, DaoException{
        doThrow(DaoException.class).when(newsDao).addNews(any(News.class));

        newsService.addNews(any(News.class));
    }

    @Test
    public void editNewsTest() throws ServiceException, DaoException{
        News news = any(News.class);

        newsService.editNews(news);

        verify(newsDao).editNews(news);
    }

    @Test(expected=ServiceException.class)
    public void editNewsExceptionTest() throws ServiceException, DaoException{
        doThrow(DaoException.class).when(newsDao).editNews(any(News.class));

        newsService.editNews(any(News.class));
    }

    @Test
    public void deleteNewsTest() throws ServiceException, DaoException{
        Long idNews = anyLong();

        newsService.deleteNews(idNews);

        verify(newsDao).deleteNews(idNews);
    }

    @Test(expected=ServiceException.class)
    public void deleteNewsExceptionTest() throws ServiceException, DaoException{
        doThrow(DaoException.class).when(newsDao).deleteNews(anyLong());

        newsService.deleteNews(anyLong());
    }

    @Test
    public void getSortedNewsTest() throws ServiceException, DaoException{
        newsService.getSortedNews();

        verify(newsDao).getSortedNews();
    }

    @Test(expected=ServiceException.class)
    public void getSortedNewsExceptionTest() throws ServiceException, DaoException{
        doThrow(DaoException.class).when(newsDao).getSortedNews();

        newsService.getSortedNews();
    }

    @Test
    public void getNewsTest() throws ServiceException, DaoException{
        Long idNews = anyLong();

        newsService.getNews(idNews);

        verify(newsDao).getSingleNews(idNews);
    }

    @Test(expected=ServiceException.class)
    public void getNewsExceptionTest() throws ServiceException, DaoException{
        doThrow(DaoException.class).when(newsDao).getSingleNews(anyLong());

        newsService.getNews(anyLong());
    }

    @Test
    public void searchByCriteriaTest() throws ServiceException, DaoException{
        SearchCriteria sc = any(SearchCriteria.class);

        newsService.searchByCriteria(sc);

        verify(newsDao).searchNews(sc);
    }

    @Test(expected=ServiceException.class)
    public void searchByCriteriaExceptionTest() throws ServiceException, DaoException{
        doThrow(DaoException.class).when(newsDao).searchNews(any(SearchCriteria.class));

        newsService.searchByCriteria(any(SearchCriteria.class));
    }


    @Test
    public void countNewsTest() throws ServiceException, DaoException{
        newsService.countNews();

        verify(newsDao).countNews();
    }

    @Test(expected=ServiceException.class)
    public void countNewsExceptionTest() throws ServiceException, DaoException{
        doThrow(DaoException.class).when(newsDao).countNews();

        newsService.countNews();
    }
}
