package com.epam.newsmanager.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.entity.Tag;
import com.epam.newsmanager.service.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {
	
    @Mock private TagDao tagDao;

    @InjectMocks private TagService tagService;

    @Test
    public void addTagTest() throws ServiceException, DaoException{
        Tag tag= any(Tag.class);

        tagService.addTag(tag);

        verify(tagDao).addTag(tag);
    }

    @Test(expected=ServiceException.class)
    public void addTagExceptionTest() throws ServiceException, DaoException{
        doThrow(DaoException.class).when(tagDao).addTag(any(Tag.class));

        tagService.addTag(any(Tag.class));
    }
    
    @Test
    public void detachTagTest() throws ServiceException, DaoException{
        Long idTag = anyLong();
        Long idNews = anyLong();

        tagService.detachTag(idTag, idNews);

        verify(tagDao).detachTagFromNews(idTag, idNews);
    }

    @Test(expected=ServiceException.class)
    public void detachTagExceptionTest() throws ServiceException, DaoException{
        doThrow(DaoException.class).when(tagDao).detachTagFromNews(anyLong(), anyLong());

        tagService.detachTag(anyLong(), anyLong());
    }
    
    @Test
    public void detachTagsTest() throws ServiceException, DaoException{
        Long idNews = anyLong();

        tagService.detachTags(idNews);

        verify(tagDao).detachTagsFromNews(idNews);
    }

    @Test(expected=ServiceException.class)
    public void detachTagsExceptionTest() throws ServiceException, DaoException{
        doThrow(DaoException.class).when(tagDao).detachTagsFromNews(anyLong());

        tagService.detachTags(anyLong());
    }

    @Test
    public void linkWithTagsTest() throws ServiceException, DaoException{
        Long idNews = anyLong();
        List<Long> idTags = anyListOf(Long.class);
        tagService.linkWithTags(idNews, idTags);

        verify(tagDao).linkTagsWithNews(idNews, idTags);
    }

    @Test(expected=ServiceException.class)
    public void linkWithTagsExceptionTest() throws ServiceException, DaoException{
        doThrow(DaoException.class).when(tagDao).linkTagsWithNews( anyLong(),anyListOf(Long.class));

        tagService.linkWithTags(anyLong(),anyListOf(Long.class));
    }
}
