package com.epam.newsmanageradmin.controller;

import com.epam.newsmanager.entity.Author;
import com.epam.newsmanager.entity.News;
import com.epam.newsmanager.entity.NewsData;
import com.epam.newsmanager.entity.SearchCriteria;
import com.epam.newsmanager.entity.Tag;
import com.epam.newsmanager.service.INewsManagerService;
import com.epam.newsmanager.service.ServiceException;
import com.epam.newsmanager.service.impl.NewsManagerService;
import com.epam.newsmanageradmin.controller.util.TagEditor;
import com.epam.newsmanageradmin.entity.NewsIdData;
import com.epam.newsmanageradmin.entity.PageData;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@SessionAttributes({ "searchCriteria" })
public class NewsController {

	private final static Logger logger = LogManager.getLogger(NewsController.class);
	private static final Long PAGE_SIZE = 6L;
	@Autowired
	private INewsManagerService newsManagerService;
	private @Autowired TagEditor tagEditor;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Tag.class, this.tagEditor);
	}

	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String getAllNews(Model model, @ModelAttribute("searchCriteria") SearchCriteria criteria,
			@ModelAttribute("pageData") PageData pageData,
			@RequestParam(value = "p", required = false, defaultValue = "0") Long page) {
		try {

			pageData.setPageSize(PAGE_SIZE);

			Long offset = page * PAGE_SIZE;
			criteria.setNewsStartIndex(offset);
			criteria.setNewsLastIndex(offset + PAGE_SIZE);

			List<NewsData> allNews = newsManagerService.searchNews(criteria);
			Long allNewsCount = newsManagerService.countNews(criteria);

			pageData.setNewsDataList(allNews);
			pageData.setCurrentPage(page);
			pageData.setAllNewsCount(allNewsCount);

		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "news";
	}

	@RequestMapping(value = "/news", method = RequestMethod.POST)
	public String getFilteredNews(Model model, @ModelAttribute("searchCriteria") SearchCriteria criteria,
			@ModelAttribute("pageData") PageData pageData, BindingResult result) {
		try {
			criteria.setNewsStartIndex(0L);
			criteria.setNewsLastIndex(PAGE_SIZE);

			List<NewsData> allNews = newsManagerService.searchNews(criteria);
			Long allNewsCount = newsManagerService.countNews(criteria);

			pageData.setPageSize(PAGE_SIZE);
			pageData.setCurrentPage(0L);
			pageData.setNewsDataList(allNews);
			pageData.setAllNewsCount(allNewsCount);

		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "redirect:/news";
	}

	@RequestMapping(value = "/news/resetFilter", method = RequestMethod.POST)
	public String resetFilteredNews(Model model, @ModelAttribute("searchCriteria") SearchCriteria criteria,
			@ModelAttribute("pageData") PageData pageData, BindingResult result) {

		criteria.setNewsStartIndex(0L);
		criteria.setNewsLastIndex(PAGE_SIZE);
		criteria.setAuthorId(null);
		criteria.setTagsId(null);

		return "redirect:/news";
	}

	@RequestMapping(value = "/news/delete", method = RequestMethod.POST)
	public String deleteNews(Model model, @ModelAttribute("newsIdList") NewsIdData newsIdData,
			@ModelAttribute("pageData") PageData pageData, BindingResult result) {
		try {
			if(newsIdData.getNewsIdList() != null && !newsIdData.getNewsIdList().isEmpty())
			for (Long newId : newsIdData.getNewsIdList()) {
				newsManagerService.deleteNews(newId);
			}
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}

		return "redirect:/news";
	}

	@RequestMapping(value = "/news/resetFilter", method = RequestMethod.GET)
	public String viewFilteredNews() {
		return "404";
	}

	@RequestMapping(value = "/news/{newsOnPageId}", method = RequestMethod.GET)
	public String getCurrentNews(Model model, @PathVariable("newsOnPageId") Long newsOrderId,
			@ModelAttribute("searchCriteria") SearchCriteria criteria,
			@RequestParam(value = "action", required = false) String action) {
		try {

			criteria.setNewsStartIndex(newsOrderId);
			criteria.setNewsLastIndex(newsOrderId + 1);
			PageData pageData = new PageData();
			Long allNewsCount = newsManagerService.countNews(criteria);
			pageData.setAllNewsCount(allNewsCount);
			List<NewsData> allNews = newsManagerService.searchNews(criteria);
			pageData.setNewsDataList(allNews);
			pageData.setCurrentPage(newsOrderId);

			model.addAttribute("pageData", pageData);
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}

		return "currentNews";
	}

	@RequestMapping(value = "/edit/{newsInBaseId}", method = RequestMethod.GET)
	public String viewEditNews(Model model, @PathVariable("newsInBaseId") Long newsId) {
		try {
			if (!model.containsAttribute("newsData")) {
				NewsData news = newsManagerService.getNewsByNewsId(newsId);
				if (news != null) {
					model.addAttribute("newsData", news);
				} else {
					return "404";
				}
			}
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "editNews";
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String doEditNews() {
		return "404";
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String doEditNews(Model model, @Valid NewsData newsData, BindingResult result, RedirectAttributes attr) {
		try {
			if (!result.hasErrors()) {
				newsManagerService.editNews(newsData);
			} else {
				attr.addFlashAttribute("org.springframework.validation.BindingResult.newsData", result);
				attr.addFlashAttribute("newsData", newsData);
			}

		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		Long newsId = newsData.getNews().getIdNews();
		attr.addAttribute("newsInBaseId", newsId);

		return "redirect:/edit/{newsInBaseId}";
	}

	@RequestMapping(value = "/addNews", method = RequestMethod.GET)
	public String viewAddNews(Model model, @ModelAttribute("newsData") NewsData newsData) {
		List<Author> authorList;
		try {
			authorList = newsManagerService.getActualAuthors();
			model.addAttribute("authorList", authorList);
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "addNews";
	}

	@RequestMapping(value = "/addNews", method = RequestMethod.POST)
	public String doAddNews(Model model, RedirectAttributes attr, @Valid NewsData newsData, BindingResult result) {
		try {
			if (!result.hasErrors()) {
				newsManagerService.addNews(newsData);
			} else {
				attr.addFlashAttribute("org.springframework.validation.BindingResult.newsData", result);
				attr.addFlashAttribute("newsData", newsData);
			}
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "addNews";
	}

	@ModelAttribute("searchCriteria")
	public SearchCriteria getSearchCriteria() {
		return new SearchCriteria();
	}

	@ModelAttribute("authorList")
	public List<Author> populateAuthors() {
		List<Author> authorList = null;
		try {
			authorList = newsManagerService.getAllAuthors();
		} catch (ServiceException ex) {
			logger.error(ex);
		}
		return authorList;
	}

	@ModelAttribute("tagList")
	public List<Tag> populateTags() {
		List<Tag> tagList = null;
		try {
			tagList = newsManagerService.getAllTags();

		} catch (ServiceException ex) {
			logger.error(ex);
		}
		return tagList;
	}

	@ModelAttribute("newsIdData")
	public NewsIdData getEmptyNewsIDList() {
		return new NewsIdData();
	}
	@ModelAttribute("pageData")
	public PageData getPageData() {
		return new PageData();
	}

	public INewsManagerService getNewsManagerService() {
		return newsManagerService;
	}

	public void setNewsManagerService(INewsManagerService newsManagerService) {
		this.newsManagerService = newsManagerService;
	}
}
