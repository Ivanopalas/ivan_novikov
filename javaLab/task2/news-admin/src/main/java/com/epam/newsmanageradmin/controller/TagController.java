
package com.epam.newsmanageradmin.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanager.entity.Author;
import com.epam.newsmanager.entity.Tag;
import com.epam.newsmanager.service.INewsManagerService;
import com.epam.newsmanager.service.ServiceException;

@Controller
public class TagController {
	private final static Logger logger = LogManager.getLogger(TagController.class);

	@Autowired
	private INewsManagerService newsManagerService;

	@RequestMapping(value = "/updateTags", method = RequestMethod.GET)
	public String viewUpdateAuthors(Model model) {
		try {
			List<Tag> tagList = null;
			Tag tagData = new Tag();
			tagList = newsManagerService.getAllTags();
			model.addAttribute("tagList", tagList);
			model.addAttribute("tagDataForm", tagData);
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "updateTags";
	}

	@RequestMapping(value = "/updateTags/delete", method = RequestMethod.POST)
	public String doExpireAuthors(Model model, @Valid Tag tagData, BindingResult result) {
		try {
			newsManagerService.deleteTag(tagData);
			Tag tagDataFrom = new Tag();
			model.addAttribute("tagDataForm", tagDataFrom);
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "redirect:/updateTags";
	}

	@RequestMapping(value = "/updateTags/edit", method = RequestMethod.POST)
	public String doEditAuthors(Model model, 
			@ModelAttribute("tagDataForm") @Valid Tag tagData, BindingResult result) {
		try {
			if (result.hasErrors()) {
				return "updateTags";
				
			}
			newsManagerService.editTag(tagData);
			Tag tagDataFrom = new Tag();
			model.addAttribute("tagDataForm", tagDataFrom);
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "redirect:/updateTags";
	}

	@RequestMapping(value = "/addTag", method = RequestMethod.POST)
	public String doSaveAuthors(Model model,
			@ModelAttribute("tagDataForm") @Valid Tag tagData, BindingResult result) {
		try {
			if (result.hasErrors()) {
				return "updateTags";
			}
			newsManagerService.addTag(tagData);
			Tag tagDataFrom = new Tag();
			model.addAttribute("tagDataForm", tagDataFrom);
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "redirect:/updateTags";
	}

	@ModelAttribute("tagList")
	public List<Tag> populateTags() {
		List<Tag> tagList = null;
		try {
			tagList = newsManagerService.getAllTags();

		} catch (ServiceException ex) {
			logger.error(ex);
		}
		return tagList;
	}
	@RequestMapping(value = "/updateTags/edit", method = RequestMethod.GET)
	public String doEditAuthors(Model model) {
		return "redirect:/updateTags";
	}

	@RequestMapping(value = "/addTag", method = RequestMethod.GET)
	public String doSaveAuthors(Model model) {
		return "redirect:/updateTags";
	}

	@RequestMapping(value = "/updateTags/delete", method = RequestMethod.GET)
	public String doExpireAuthors(Model model) {
		return "redirect:/updateTags";
	}

}