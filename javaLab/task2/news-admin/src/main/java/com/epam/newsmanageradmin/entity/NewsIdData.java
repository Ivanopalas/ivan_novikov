package com.epam.newsmanageradmin.entity;


import java.util.List;

public class NewsIdData {
    private List<Long> newsIdList;

    public List<Long> getNewsIdList() {
        return newsIdList;
    }

    public void setNewsIdList(List<Long> newsIdList) {
        this.newsIdList = newsIdList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsIdData that = (NewsIdData) o;

        if (newsIdList != null ? !newsIdList.equals(that.newsIdList) : that.newsIdList != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return newsIdList != null ? newsIdList.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "NewsIdData{" +
                "newsIdList=" + newsIdList +
                '}';
    }
}
