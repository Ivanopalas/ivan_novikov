<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<meta name="_csrf" content="${_csrf.token}"/>
<meta name="_csrf_header" content="${_csrf.headerName}"/>

<fmt:setLocale value="${pageContext.response.locale}" />
<c:set var="newsData" value="${pageData.newsDataList[0]}" />
<table
	style="margin: 0 auto; width: 95%; table-layout: fixed; word-wrap: break-word; align: center;"
	class="table table-hover">
	<tr bgcolor="#cccccc">
		<td><em style="font-size: 12pt;"> <b> <c:out
						value="${newsData.news.title}" />
			</b></em> <span style="font-size: 8pt;">(<spring:message
					code="local.news.author" /> <c:out value="${newsData.author.name}" />)
		</span></td>
		<td align="right"><i><fmt:formatDate value="${newsData.news.modificationDate}" /></i></td>
		
	</tr>
	<tr>
		<td colspan="2"><c:out value="${newsData.news.fullText}" /></td>
	</tr>
</table>
<hr />
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script
	src="${pageContext.request.contextPath}/js/commentFunctionality.js"></script>
<script src="${pageContext.request.contextPath}/js/dateFormatter.js"></script>
<script>
var prefix = '${pageContext.request.contextPath}';
var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");

var deleteCommentInDatabase = function(idComment) {
	        $.ajax({
	        	
	        type: 'POST',
	        url:  prefix + '/deleteComment/' + idComment,
	        beforeSend: function(xhr) {
	            // here it is
	            xhr.setRequestHeader(header, token);
	        },
	        async: true,
	        error: function(jqXHR, textStatus, errorThrown) {
	        		 alert(jqXHR.status + ' ' + jqXHR.responseText);
	        }
	    });
	}


$(document).ready(function() {
    $('#addComment').click(function(){
    	commentText = $('#commentText').val();
    	var commentInfo=  {
	   			"commentText": commentText,
	     		"idNews": "${newsData.news.idNews}"
    	};
    	
	    $.ajax({
	        type: 'POST',
	        url:  prefix + '/addComment',
	        beforeSend: function(xhr) {
	            // here it is
	            xhr.setRequestHeader(header, token);
	        },
	        contentType: 'application/json; charset=utf-8',
	     
	    	data: JSON.stringify(commentInfo),
	        dataType: "json",
	        contentType: "application/json",
	        async: true,
	        success: function(jqXHR) {
	        	createTableDate(jqXHR,'${pageContext.response.locale}');
	        	document.getElementById('commentText').value='';
	        	/*  document.getElementById("newCommentDate").innerHTML += createTableDate(result); 
	        	 document.getElementById("newCommentText").innerHTML += createTableText(result);  */
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	            alert(jqXHR.status + ' cant add comment' + jqXHR.responseText);
	        }
	    });
    });
});
</script>




<table style="width: 100%;">
	<tr>
		<td style="text-align: left"><c:if
				test="${pageData.currentPage  != 0}">
				<spring:url value="/news/${pageData.currentPage - 1}"
					var="pagedLink">
				</spring:url>
				<a href="${pagedLink}"><spring:message
						code="local.page.previous" /></a>
			</c:if></td>
		<td style="text-align: right"><c:if
				test="${pageData.currentPage + 1 ne pageData.allNewsCount}">
				<spring:url value="/news/${pageData.currentPage + 1}"
					var="pagedLink">
				</spring:url>
				<a href="${pagedLink}"><spring:message code="local.page.next" /></a>
			</c:if></td>
	</tr>

</table>

<table id="commentTable" class="table table-condensed table-striped"
	style="margin: 0 auto; width: 30%; table-layout: fixed; word-wrap: break-word;">


	<tbody id="insertComment">
	</tbody>
	<c:if test="${!empty newsData.comments}">

		<c:forEach items="${newsData.comments}" var="comments">
			<tr>
				<th class="col-sm-2"><fmt:formatDate type="both"
						value="${comments.creationDate}" />
					<button class="close"
						onclick="deleteComment(this,${comments.idComment});">&times;</button>
				</th>
			</tr>
			<tr>
				<td bgcolor="#d2d2d2"><c:out value="${comments.commentText}" /></td>
			</tr>
		</c:forEach>
	</c:if>
	<tr>
		<td><textarea style="width: 100%; resize: none;" id="commentText"></textarea>

			<div style="text-align: center;">
				<button id="addComment" style="align: center;" class="btn btn-lg">
					<spring:message code="local.comment.add" />
				</button>
			</div></td>
	</tr>

</table>








