<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/multiple-select.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/single-select.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.css" />
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery.multiple.select.js"></script>


<form:form method="POST" style="width: 350px; margin: 0 auto; "
	modelAttribute="newsData"
	action="${pageContext.request.contextPath}/edit">
	<div class="form-group">
		<form:label path="news.title"><spring:message code="local.news.title" /></form:label>
		<form:input class="form-control" path="news.title"
			value="${news.title}" />
		<form:errors path="news.title"  cssStyle="color: red;"/>
	</div>
	<div class="form-group">
		<form:label path="news.modificationDate"><spring:message code="local.news.modificationDate" /></form:label>
		<form:input class="form-control" type="date"
			value="${news.modificationDate}" path="news.modificationDate" />
			
	</div>
	<div class="form-group">
		<form:label path="news.shortText"><spring:message code="local.news.brief" /></form:label>
		<form:textarea class="form-control" style=" resize:vertical;" rows="2"
			path="news.shortText" value="${news.shortText}" />
		<form:errors path="news.shortText" cssStyle="color: red;" />
			
	</div>
	<div class="form-group">
		<form:label path="news.fullText"><spring:message code="local.news.content" /></form:label>
		<form:textarea class="form-control" style=" resize:vertical;" rows="5"
			path="news.fullText" value="${news.fullText}" />
		<form:errors path="news.fullText"  cssStyle="color: red;"/>
	</div>
	<div align="center">
		<table>
			<tr>
				<td><form:select class="ss" path="author.idAuthor">
						<div class="col-xs-2">
							<form:options items="${authorList}" itemValue="idAuthor"
								itemLabel="name" />
						</div>
					</form:select></td>
					<td><form:select multiple="true" id="ms" path="tags">
						<form:options items="${tagList}" itemValue="idTag"
							itemLabel="tagName" />

					</form:select></td>
			</tr>
		</table>
	</div>
	<form:input type="hidden" path="news.idNews" value="${news.idNews}" />
			<form:errors path="news.idNews" cssStyle="color: red;" />
	
	<hr/>

	<button type="submit" style="width: 100%;" class="btn btn-lg"><spring:message code="local.news.add" /></button>

</form:form>

<script>
	$('#ms').multipleSelect();
</script>

