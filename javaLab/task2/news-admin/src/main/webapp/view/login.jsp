<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.css" />

<form name='loginForm' action="${loginUrl}" method='POST'>

	<div class="form-group">
		<label>User:</label> <input class="form-control" type='text'
			name='username' value=''>
	</div>

	<div class="form-group">
		<label>Password:</label> <input class="form-control" type='password'
			name='password' />
	</div>
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	<button type="submit" class="btn btn-lg">Login</button>

</form>