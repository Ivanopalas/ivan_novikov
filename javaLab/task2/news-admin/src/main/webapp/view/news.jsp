<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/multiple-select.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/single-select.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.css" />
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.multiple.select.js"></script>



<fmt:setLocale value="${pageContext.response.locale}" />
<div align="center">

	<table>
		<tr>
			<form:form action="${pageContext.request.contextPath}/news"
				method="POST" modelAttribute="searchCriteria">
				<td><form:select class="ss" path="authorId">
						<div class="col-xs-2">
							<form:option value="">
								<spring:message code="local.author.choose" />
							</form:option>
							<form:options items="${authorList}" itemValue="idAuthor"
								itemLabel="name" />
						</div>
					</form:select></td>
				<td><form:select id="ms" path="tagsId">
						<form:options items="${tagList}" itemValue="idTag"
							itemLabel="tagName" />
					</form:select></td>
				<td><button class="ss" type="submit">
						<spring:message code="local.button.filter" />
					</button></td>
			</form:form>
			<form:form action="${pageContext.request.contextPath}/news/resetFilter" method="POST">
				<td>
					<button class="ss" type="submit">
						<spring:message code="local.button.cancelFilter" />
					</button>
				</td>
			</form:form>
		</tr>
	</table>

</div>


<script>
	$('#ms').multipleSelect();
</script>

<form:form method="POST" modelAttribute="newsIdData"
	 action="${pageContext.request.contextPath}/news/delete">
<table class="table table-hover">
	<c:forEach items="${pageData.newsDataList}" var="newsData"
		varStatus="newsCount">
		<tr>
			<td>
				<table style="width: 100%;">
					<tr bgcolor="#cccccc">
						<td><em style="font-size: 12pt;"><b><a
									href="${pageContext.request.contextPath}/news/${pageData.currentPage * pageData.pageSize + newsCount.index}"><c:out
											value="${newsData.news.title}" /></a></b></em> <span
							style="font-size: 8pt;">(<spring:message
									code="local.news.author" /> <c:out
									value="${newsData.author.name}" />)
						</span></td>
						<td align="right"><i> <fmt:formatDate
									value="${newsData.news.modificationDate}" /> <%-- <c:out value="${newsData.news.modificationDate}" /> --%>
						</i></td>
					</tr>
					<tr>
						<td colspan="2"><c:out value="${newsData.news.shortText}" />
						</td>
					</tr>
					<tr>
						<td colspan="2" align="right"><span style="font-size: 9pt;">
								<c:if test="${!empty newsData.tags}">
									<b><spring:message code="local.news.tags" /></b>
									<c:forEach items="${newsData.tags}" var="tags">
										<c:out value="${tags.tagName}" />
									</c:forEach>
								</c:if> <b><spring:message code="local.news.comments" />(${newsData.comments.size()})</b>
						</span> <form:checkbox path="newsIdList" value="${newsData.news.idNews}"/>
						<a class="btn btn-default"
							href="${pageContext.request.contextPath}/edit/${newsData.news.idNews}"><spring:message
									code="local.news.edit" /></a></td>
					</tr>
				</table>
			</td>
		</tr>
	</c:forEach>
</table>
<div align="right"><button  type="submit" style="width: 10%;" class="btn btn-default"><spring:message code="local.news.delete" /></button></div>
</form:form>
<spring:url value="/news" var="pagedLink">
	<spring:param name="p" value="~" />
</spring:url>


<div align="center">
	<tg:paging pageData="${pageData}" pagedLink="${pagedLink}" />
</div>








