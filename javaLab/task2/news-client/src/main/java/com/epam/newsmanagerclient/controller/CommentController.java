package com.epam.newsmanagerclient.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.epam.newsmanager.entity.Comment;
import com.epam.newsmanager.service.INewsManagerService;
import com.epam.newsmanager.service.ServiceException;

@Controller
public class CommentController {

	private final static Logger logger = LogManager.getLogger(CommentController.class);
	
	@Autowired
	private INewsManagerService newsManagerService;

	@RequestMapping(value="/addComment",  produces = "application/json",  method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Comment> addComment(@RequestBody Comment comment) {
		try {
			Long commentId = newsManagerService.addComment(comment);
			comment.setIdComment(commentId);

		} catch (ServiceException ex) {
			logger.error(ex);
			ResponseEntity<Comment> responce = new ResponseEntity<Comment>(HttpStatus.BAD_REQUEST);
			return responce;
		}
		ResponseEntity<Comment> responce = new ResponseEntity<Comment>(comment,HttpStatus.ACCEPTED);
		return responce;
    }
	
}
