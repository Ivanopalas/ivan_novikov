package com.epam.newsmanagerclient.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanager.entity.Author;
import com.epam.newsmanager.entity.NewsData;
import com.epam.newsmanager.entity.SearchCriteria;
import com.epam.newsmanager.entity.Tag;
import com.epam.newsmanager.service.INewsManagerService;
import com.epam.newsmanager.service.ServiceException;
import com.epam.newsmanagerclient.entity.PageData;

@Controller
@SessionAttributes({ "searchCriteria" })
public class NewsController {

	private final static Logger logger = LogManager.getLogger(NewsController.class);
	private static final Long PAGE_SIZE = 6L;
	@Autowired
	private INewsManagerService newsManagerService;

	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String getAllNews(Model model, @ModelAttribute("searchCriteria") SearchCriteria criteria,
			@ModelAttribute("pageData") PageData pageData,
			@RequestParam(value = "p", required = false, defaultValue = "0") Long page) {
		try {

			pageData.setPageSize(PAGE_SIZE);

			Long offset = page * PAGE_SIZE;
			criteria.setNewsStartIndex(offset);
			criteria.setNewsLastIndex(offset + PAGE_SIZE);

			List<NewsData> allNews = newsManagerService.searchNews(criteria);
			Long allNewsCount = newsManagerService.countNews(criteria);

			pageData.setNewsDataList(allNews);
			pageData.setCurrentPage(page);
			pageData.setAllNewsCount(allNewsCount);

			return "news";
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
	}

	@RequestMapping(value = "/news", method = RequestMethod.POST)
	public String getFilteredNews(Model model, @ModelAttribute("searchCriteria") SearchCriteria criteria,
			@ModelAttribute("pageData") PageData pageData, BindingResult result) {
		try {
			criteria.setNewsStartIndex(0L);
			criteria.setNewsLastIndex(PAGE_SIZE);

			List<NewsData> allNews = newsManagerService.searchNews(criteria);
			Long allNewsCount = newsManagerService.countNews(criteria);

			pageData.setPageSize(PAGE_SIZE);
			pageData.setCurrentPage(0L);
			pageData.setNewsDataList(allNews);
			pageData.setAllNewsCount(allNewsCount);

		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "redirect:/news";
	}

	@RequestMapping(value = "/news/{newsOnPageId}", method = RequestMethod.GET)
	public String getCurrentNews(Model model, @PathVariable("newsOnPageId") Long newsOrderId,
			@ModelAttribute("searchCriteria") SearchCriteria criteria,
			@RequestParam(value = "action", required = false) String action) {
		try {

			criteria.setNewsStartIndex(newsOrderId);
			criteria.setNewsLastIndex(newsOrderId + 1);
			PageData pageData = new PageData();
			Long allNewsCount = newsManagerService.countNews(criteria);
			pageData.setAllNewsCount(allNewsCount);
			List<NewsData> allNews = newsManagerService.searchNews(criteria);
			pageData.setNewsDataList(allNews);
			pageData.setCurrentPage(newsOrderId);

			model.addAttribute("pageData", pageData);
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}

		return "currentNews";
	}

	@ModelAttribute("searchCriteria")
	public SearchCriteria getSearchCriteria() {
		return new SearchCriteria();
	}

	/*
	 * @ModelAttribute("newsData") public NewsData getNewsData() { return new
	 * NewsData(); }
	 */
	@ModelAttribute("authorList")
	public List<Author> populateAuthors() {
		List<Author> authorList = null;
		try {
			authorList = newsManagerService.getAllAuthors();

		} catch (ServiceException ex) {
			logger.error(ex);
			
		}
		return authorList;
	}

	@ModelAttribute("tagList")
	public List<Tag> populateTags() {
		List<Tag> tagList = null;
		try {
			tagList = newsManagerService.getAllTags();

		} catch (ServiceException ex) {
			logger.error(ex);
			
		}
		return tagList;
	}

	@ModelAttribute("pageData")
	public PageData getPageData() {
		return new PageData();
	}

	public INewsManagerService getNewsManagerService() {
		return newsManagerService;
	}

	public void setNewsManagerService(INewsManagerService newsManagerService) {
		this.newsManagerService = newsManagerService;
	}
}
