package com.epam.newsmanagerclient.util;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages={"com.epam"})
public class SpringRootConfig {
}
