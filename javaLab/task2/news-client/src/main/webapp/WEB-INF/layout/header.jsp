<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<hr />
<table style="width:100%">
    <tr>
        <td align="left">
            <h3>
            	<spring:message code="local.header.title" />
            </h3>
        </td>
        <td align="right">
            <a href="?lang=en">en</a>
            <a href="?lang=ru">ru</a>
        </td>
      
    </tr>
</table>
<hr />