<%@ tag import="org.springframework.util.StringUtils" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ attribute name="pageData" required="true" type="com.epam.newsmanagerclient.entity.PageData" %>
<%@ attribute name="pagedLink" required="true" type="java.lang.String" %>

<link href="css/pagination.css" rel="stylesheet" type="text/css"/>

 

<c:if test="${pageData.allNewsCount > pageData.pageSize}">


<c:if test="${pageData.currentPage != 0}">
        <span class="pagingItem"><a href="<%= StringUtils.replace(pagedLink, "~", String.valueOf(pageData.getCurrentPage()-1)) %>">&lt;</a></span>
    </c:if>
<c:forEach begin="0" end="${pageData.allNewsCount / pageData.pageSize}" var="i">
        <c:choose>
            <c:when test="${pageData.currentPage == i}">
                <span class="pagingItemCurrent">${i+1}</span>
            </c:when>
            <c:otherwise>
                <span class="pagingItem"><a href="<%= StringUtils.replace(pagedLink, "~", String.valueOf(jspContext.getAttribute("i"))) %>">${i+1}</a></span>
            </c:otherwise>
        </c:choose>
    </c:forEach> 
<c:if test="${pageData.currentPage < pageData.allNewsCount/pageData.pageSize - 1}">
        <span class="pagingItem"><a href="<%= StringUtils.replace(pagedLink, "~", String.valueOf(pageData.getCurrentPage()+1)) %>">&gt;</a></span>
    </c:if>
 <%--    
    <c:if test="${pagedListHolder.firstLinkedPage > 0}">
        <span class="pagingItem"><a href="<%= StringUtils.replace(pagedLink, "~", "0") %>">1</a></span>
    </c:if>
    <c:if test="${pagedListHolder.firstLinkedPage > 1}">
        <span class="pagingDots">...</span>
    </c:if> --%>
    
   <%--  <c:if test="${pagedListHolder.lastLinkedPage < pagedListHolder.pageCount - 2}">
        <span class="pagingDots">...</span>
    </c:if>
    <c:if test="${pageData.currentPage < pageData.allNewsCount/pageData.pageSize - 1}">
        <span class="pagingItem"><a href="<%= StringUtils.replace(pagedLink, "~", String.valueOf(pageData.getAllNewsCount()/pageData.getPageSize()-1)) %>">${pageData.pageSize}</a></span>
    </c:if>
     --%>
</c:if>