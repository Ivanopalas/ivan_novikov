package com.epam.newsmanager.dao;

import com.epam.newsmanager.entity.Author;

import java.util.List;

public interface AuthorDao {
	/**
	 * Add news author to database
	 * @return author id in database
	 */
	public Long addAuthor(Author author) throws DaoException;
	/**
	 * Set author as expired
	 */
	public void deleteAuthor(Long idAuthor) throws DaoException;
    /**
     * Link news and author in database
     * @param idAuthor id of author in database
     * @param idNews id of news in database
     */
	public void linkNewsWithAuthor(Long idAuthor,Long idNews) throws DaoException;
	/**
	 * Destroy link in database between news and author
     * @param idNews id of news in database
	 */
	public void detachNewsFromAuthorByNewsId(Long idNews) throws DaoException;
	/**
	 * Get all info about author of certain news
	 * @param idNews news id in database
	 */
	public Author getAuthorByNewsId(Long idNews) throws DaoException;

	public List<Author> getActualAuthors() throws DaoException;
	
	public List<Author> getAllAuthors() throws DaoException;
	
	public void editAuthor(Author author) throws DaoException;
}
