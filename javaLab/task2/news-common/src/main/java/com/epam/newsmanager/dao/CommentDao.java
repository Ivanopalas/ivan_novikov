package com.epam.newsmanager.dao;

import java.util.List;

import com.epam.newsmanager.entity.Comment;

public interface CommentDao {
	/**
	 * Add new comment to curtain news.
	 * @param comment Comment data.
	 * @return id of added comment
	 */
	public Long addComment(Comment comment) throws DaoException;
	/**
	 * Delete comment by id
	 * @param idComment id of comment you want delete.
	 */
	public void deleteComment(Long idComment) throws DaoException;
	/**
	 * Delete all comments by news id.
	 * @param idNews id of news that comments you want delete
	 */
	public void deleteAllCommentsFromNews(Long idNews) throws DaoException;
	/**
	 * * Get all comments to certain news
	 * @param idNews id of news in database
	 * @return list of comments
	 */
	public List<Comment> getCommentsByNewsID(Long idNews) throws DaoException;


    public Comment getCommentById(Long commentId) throws DaoException;
}
