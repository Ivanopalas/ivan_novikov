package com.epam.newsmanager.dao;

import java.util.List;

import com.epam.newsmanager.entity.News;
import com.epam.newsmanager.entity.SearchCriteria;

public interface NewsDao {
	/**
	 * 
	 * @param news entity with all news info
	 * @return id of inserted news
	 */
	public Long addNews(News news) throws DaoException;
	/**
	 * Update news in database by id in entity
	 * @param news entity should contain id of news
	 */
	public void editNews(News news) throws DaoException;
	/**
	 * Delete news by id
	 * @param idNews id of news you want delete.
	 */
	public void deleteNews(Long idNews) throws DaoException;
	/**
	 * Get list of news sorted by comment number and modified date.
	 * @return list of sorted news.
	 */
	public List<News> getSortedNews() throws DaoException;
	/**
	 * Get news data by id
	 * @param idNews id of news data in database
	 * @return News data
	 */
	public News getSingleNews(Long idNews) throws DaoException;
	/**
	 * Get list of news filtered by given criteria
	 * @param criteria author and list of tags inside entity by which all news filtered
	 * @return list of filtered news
	 */
	public List<News> searchNews(SearchCriteria criteria) throws DaoException;
	/**
	 * Return number of news in database
	 * @return number of all news
	 */
	public Long countNews(SearchCriteria criteria) throws DaoException;




}
