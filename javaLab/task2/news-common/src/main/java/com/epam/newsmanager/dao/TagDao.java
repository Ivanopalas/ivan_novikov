package com.epam.newsmanager.dao;

import java.util.List;

import com.epam.newsmanager.entity.Tag;
import com.epam.newsmanager.service.ServiceException;

public interface TagDao {
    /**
     * Create new tag in database.
     * @param tag Tag data.
     * @return id of added tag
     */
	public Long addTag(Tag tag) throws DaoException;
    /**
     * Get list of tags by news id which is linked with news.
     * @param idNews id of news in database
     * @return list of tags.
     */
	public List<Tag> getTagsByNewsId(Long idNews) throws DaoException;
    /**
     * Link news and tags in database
     * @param idTags list of tags id
     * @param idNews id of news in database
     */
	public void linkTagsWithNews(Long idNews, List<Long> idTags) throws DaoException;
    /**
     * Destroy link in database between news and current tag
     * @param idNews id of news in database
     * @param idTag id of tag in database
     */
	public void detachTagFromNews(Long idTag,Long idNews) throws DaoException;
    /**
     * Destroy link in database between news and all tags
     * @param idNews id of news in database
     */
	public void detachTagsFromNews(Long idNews) throws DaoException;
	
    public Tag getTagById(Long tagId) throws DaoException;
    
    public List<Tag> getAllTags() throws DaoException;
    
    public void editTag(Tag tag) throws DaoException;
	
	public void deleteTag(Tag tag) throws DaoException;
	
	public void detachAllLinksFromTag(Long tagId) throws DaoException;
}
 