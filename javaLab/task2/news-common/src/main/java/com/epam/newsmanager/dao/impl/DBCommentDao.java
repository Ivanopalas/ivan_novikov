package com.epam.newsmanager.dao.impl;

import com.epam.newsmanager.dao.CommentDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.util.DataSourceHelper;
import com.epam.newsmanager.entity.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DBCommentDao implements CommentDao {
    @Autowired
    
    private oracle.jdbc.pool.OracleDataSource dataSource;

    private static final String EXCEPTION_ACT_MESSAGE = "Sql actions exception";

    private static final String ADD_COMMENT_QUERY = "INSERT INTO Comments values ('',?,?,SYSDATE)";
    private static final String DELETE_COMMENT_QUERY = "DELETE FROM Comments WHERE COMMENT_ID = ?";
    private static final String DELETE_ALL_COMMENTS_QUERY = "DELETE FROM Comments WHERE NEWS_ID = ?";
    private static final String GET_COMMENTS_QUERY =
            "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE FROM Comments WHERE NEWS_ID = ?  order by CREATION_DATE desc";

    private static final String GET_COMMENT_BY_ID_QUERY =
            "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE FROM Comments WHERE COMMENT_ID = ?";
    public static final String COMMENT_ID = "COMMENT_ID";
    public static final String COMMENT_TEXT = "COMMENT_TEXT";
    public static final String COMMENT_CREATION_DATE = "CREATION_DATE";


    public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(oracle.jdbc.pool.OracleDataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Long addComment(Comment comment) throws DaoException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);

            statement = connection.prepareStatement(
                    ADD_COMMENT_QUERY, new String[] { COMMENT_ID });
            statement.setLong(1, comment.getIdNews());
            statement.setString(2, comment.getCommentText());

            statement.executeUpdate();

            resultSet = statement.getGeneratedKeys();
            Long idComment = null;
            if (resultSet.next()) {
                idComment = resultSet.getLong(1);
            }
            return idComment;

        } catch (SQLException ex) {
            throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
        } finally {
            DataSourceHelper.closeConnection(connection, dataSource, statement, resultSet);
        }
    }

    public void deleteComment(Long idComment) throws DaoException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(DELETE_COMMENT_QUERY);

            statement.setLong(1, idComment);
            statement.executeUpdate();

        } catch (SQLException ex) {
            throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
        } finally {
            DataSourceHelper.closeConnection(connection, dataSource, statement);
        }
    }

    public void deleteAllCommentsFromNews(Long idNews) throws DaoException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(DELETE_ALL_COMMENTS_QUERY);

            statement.setLong(1, idNews);
            statement.executeUpdate();
        } catch (SQLException ex) {
            throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
        } finally {
            DataSourceHelper.closeConnection(connection, dataSource, statement);
        }
    }

    public List<Comment> getCommentsByNewsID(Long idNews) throws DaoException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(GET_COMMENTS_QUERY);

            statement.setLong(1, idNews);

            List<Comment> allComments = new ArrayList<Comment>();
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Comment currentComment = new Comment();

                Long idComment = resultSet.getLong(COMMENT_ID);
                String commentText = resultSet.getString(COMMENT_TEXT);
                Timestamp creationDate = resultSet.getTimestamp(COMMENT_CREATION_DATE);

                currentComment.setIdComment(idComment);
                currentComment.setCommentText(commentText);
                currentComment.setCreationDate(creationDate);

                allComments.add(currentComment);
            }
            return allComments;
        } catch (SQLException ex) {
            throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
        } finally {
            DataSourceHelper.closeConnection(connection, dataSource, statement, resultSet);
        }
    }

 
    public Comment getCommentById(Long commentId) throws DaoException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(GET_COMMENT_BY_ID_QUERY);

            statement.setLong(1, commentId);

            Comment comment = new Comment();
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Long idComment = resultSet.getLong(COMMENT_ID);
                String commentText = resultSet.getString(COMMENT_TEXT);
                Timestamp creationDate = resultSet.getTimestamp(COMMENT_CREATION_DATE);

                comment.setIdComment(idComment);
                comment.setCommentText(commentText);
                comment.setCreationDate(creationDate);

            }
            return comment;
        } catch (SQLException ex) {
            throw new DaoException(EXCEPTION_ACT_MESSAGE, ex);
        } finally {
            DataSourceHelper.closeConnection(connection, dataSource, statement, resultSet);
        }
    }
}
