package com.epam.newsmanager.service;

import java.util.List;

import com.epam.newsmanager.entity.Comment;

public interface ICommentService {
	/**
	 * Add comment to news
	 * @return comment id in database
	 */
	public Long addComment(final Comment comment) throws ServiceException;
	/**
	 * Delete comment.
	 * @param idComment id of comment in database
	 */
	public void deleteComment(final Long idComment) throws ServiceException;
	/**
	 * Delete all comments from news.
	 */
	public void deleteCommentsFromNews(final Long idNews) throws ServiceException;
	/**
	 * Get all comments to certain news
	 */
	public List<Comment> getComments(final Long idNews) throws ServiceException;

    public Comment getCommentById(Long commentId) throws ServiceException;
}
