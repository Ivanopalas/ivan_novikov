package com.epam.newsmanager.service;

import java.util.List;

import com.epam.newsmanager.entity.Tag;

public interface ITagService {
	/**
	 * Get all tags linked with current news.
	 * @param idNews news id which is linked with tags
	 * @return list of tags linked with news
	 */
	public List<Tag> getTags(Long idNews) throws ServiceException;
	/**
	 * Add new tag to database
	 * @param tag entity with all tag parameters
	 * @return id of added tag
	 */
	public Long addTag(Tag tag) throws ServiceException;

	/**
	 * Delete link in database witch connect tag and news
	 * @param idTag id of tag
	 * @param idNews id of news
	 */
	public void detachTag(Long idTag, Long idNews) throws ServiceException;
	/**
	 * Detach all tags from certain news
	 * @param idNews id of news in database
	 */
	public void detachTags(Long idNews) throws ServiceException;
	/**
	 * Attach list of tags to news
	 * @param idNews id of news in database
	 * @param tags list of id of tags in database
	 */
	public void linkWithTags(Long idNews, List<Long> tags) throws ServiceException;

    public List<Tag> getAllTags() throws ServiceException;

    public Tag getTagById(Long tagId) throws ServiceException;
    
	public void editTag(final Tag tag) throws ServiceException;
	
	public void deleteTag(final Tag tag) throws ServiceException;
	
	public void detachAllLinksFromTag(Long tagId) throws ServiceException;
}
 