package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.entity.Tag;
import com.epam.newsmanager.service.ITagService;
import com.epam.newsmanager.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagService implements ITagService {
    private final static Logger logger = LogManager.getLogger(TagService.class);
    private final static String LINK_TAG_ERROR = "can't link tag with news";
    private final static String DETACH_TAG_ERROR = "can't detach tag from news";

    @Autowired
    private TagDao tagDao;

    public List<Tag> getTags(Long idNews) throws ServiceException {
        List<Tag> tags = null;
        try {
            tags = tagDao.getTagsByNewsId(idNews);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        return tags;
    }

    public Tag getTagById(Long tagId) throws ServiceException {
        Tag tag = null;
        try {
            tag = tagDao.getTagById(tagId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        return tag;
    }
    public void linkWithTags(Long idNews, List<Long> tags) throws ServiceException {
        try {
            tagDao.linkTagsWithNews(idNews, tags);
        } catch (DaoException ex) {
            logger.error(LINK_TAG_ERROR);
            throw new ServiceException(ex);
        }
    }


    public List<Tag> getAllTags() throws ServiceException {
        List<Tag> allTags;
        try {
            allTags = tagDao.getAllTags();
        } catch (DaoException ex) {
            logger.error(LINK_TAG_ERROR);
            throw new ServiceException(ex);
        }
        return allTags;
    }

    public void detachTags(Long idNews) throws ServiceException {
        try {
            tagDao.detachTagsFromNews(idNews);
        } catch (DaoException ex) {
            logger.error(DETACH_TAG_ERROR);
            throw new ServiceException(ex);
        }

    }

    public void detachTag(Long idTag, Long idNews) throws ServiceException {
        try {
            tagDao.detachTagFromNews(idTag, idNews);
        } catch (DaoException ex) {
            logger.error(DETACH_TAG_ERROR);
            throw new ServiceException(ex);
        }

    }


    public Long addTag(Tag tag) throws ServiceException {
        Long idTag = null;
        try {
            idTag = tagDao.addTag(tag);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        return idTag;
    }

	public void editTag(Tag tag) throws ServiceException {
        try {
            tagDao.editTag(tag);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
	}

	public void deleteTag(Tag tag) throws ServiceException {
		 try {
	            tagDao.deleteTag(tag);
	        } catch (DaoException ex) {
	            throw new ServiceException(ex);
	        }
	}

	public void detachAllLinksFromTag(Long tagId) throws ServiceException {
		try {
            tagDao.detachAllLinksFromTag(tagId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
	}

}
