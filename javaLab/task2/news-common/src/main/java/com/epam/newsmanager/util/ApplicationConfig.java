package com.epam.newsmanager.util;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;


@Configuration
@ImportResource({"classpath:/springConfiguration.xml"})
public class ApplicationConfig {
}
