package com.epam.newsmanager.dao.impl;

import static org.junit.Assert.*;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.util.DataSourceHelper;
import com.epam.newsmanager.entity.*;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"/spring/springConfigurationTest.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@Transactional
public class DBNewsTest {

    private final static String NEWS_COUNT_QUERY = "SELECT count(1) FROM News";
    private final static String CHECK_IS_DELETED_NEWS =
            "select NEWS_ID from Comments where NEWS_ID = ?";

    @Autowired
    private DBNewsDao newsDao;
    @Autowired
    private DataSource dataSource;

    @Test
    public void addNewsTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);
        Statement st = con.createStatement();
        ResultSet rs = null;

        rs = st.executeQuery(NEWS_COUNT_QUERY);

        rs.next();
        int newsCountBefore = rs.getInt(1);

        News news = new News();
        news.setFullText("test full text");
        news.setShortText("test short text");
        news.setTitle("test title");

        newsDao.addNews(news);
        rs = st.executeQuery(NEWS_COUNT_QUERY);
        int newsCountAfter = -1;
        if(rs.next()) {
            newsCountAfter = rs.getInt(1);
        }
        assertEquals(newsCountBefore + 1, newsCountAfter);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }

    @DatabaseSetup(value = "/testdata/news/newsData.xml", type= DatabaseOperation.INSERT)
    @Test
    public void editNewsTest() throws DaoException,SQLException{
        String expectedFullText = "edited test full text";
        String expectedShortText = "edited test short text";
        String expectedTitle = "edited test title";

        News news = new News();
        news.setIdNews(1L);
        news.setFullText(expectedFullText);
        news.setShortText(expectedShortText);
        news.setTitle(expectedTitle);

        newsDao.editNews(news);

        News editedNews = newsDao.getSingleNews(1L);

        assertEquals( expectedFullText,editedNews.getFullText());
        assertEquals( expectedShortText,editedNews.getShortText());
        assertEquals( expectedTitle,editedNews.getTitle());
    }

    @DatabaseSetup(value = "/testdata/news/newsData.xml", type= DatabaseOperation.INSERT)
    @Test
    public void deleteNewsTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);

        Long testId = 1L;

        newsDao.deleteNews(testId);

        PreparedStatement st = con.prepareStatement(CHECK_IS_DELETED_NEWS);
        st.setLong(1, testId);
        ResultSet rs;
        rs = st.executeQuery();
        Long newsId = null;
        if(rs.next()){
            newsId  =  rs.getLong(1);
        }
        assertNull(newsId);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }


    @DatabaseSetup(value = "/testdata/news/newsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void getSingleNewsTest() throws DaoException,SQLException{
        Long testId = 1L;

        News news = newsDao.getSingleNews(testId);

        String expectedFullText = "test full";
        String expectedShortText = "test short";
        String expectedTitle = "test title";

        assertEquals(expectedFullText,news.getFullText());
        assertEquals(expectedShortText,news.getShortText());
        assertEquals(expectedTitle,news.getTitle());
    }

    @DatabaseSetup(value = "/testdata/news/newsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void getSortedNewsTest() throws DaoException,SQLException{
        List<News> news = newsDao.getSortedNews();

        int expectedLength = 4;
        int actualLength = news.size();
        assertEquals(expectedLength, actualLength);
    }

    @DatabaseSetup(value = "/testdata/news/newsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void countNewsTest() throws DaoException,SQLException{
        SearchCriteria criteria = new SearchCriteria();
        criteria.setNewsLastIndex(100L);
        criteria.setNewsStartIndex(0L);
        Long actualLength = newsDao.countNews(criteria);

        Long expectedLength = 4L;
        assertEquals(expectedLength, actualLength);
    }

    @DatabaseSetup(value = "/testdata/news/linkedNewsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void searchNewsTest() throws DaoException,SQLException{
        List<News> news = newsDao.getSortedNews();

        SearchCriteria criteria = new SearchCriteria();

        criteria.setNewsLastIndex(100L);
        criteria.setNewsStartIndex(0L);
        List<News> searchedNews = newsDao.searchNews(criteria);
        int expectedLength = 4;
        assertEquals(expectedLength, searchedNews.size());


        criteria.setAuthorId(1L);

        searchedNews = newsDao.searchNews(criteria);
        expectedLength = 2;
        assertEquals(expectedLength, searchedNews.size());

        List<Long> tags = new ArrayList<Long>();


        tags.add(1L);
        criteria.setTagsId(tags);

        searchedNews = newsDao.searchNews(criteria);
        expectedLength = 1;
        assertEquals(expectedLength, searchedNews.size());

        criteria = new SearchCriteria();

        criteria.setNewsLastIndex(100L);
        criteria.setNewsStartIndex(0L);
        tags.add(2L);
        criteria.setTagsId(tags);

        searchedNews = newsDao.searchNews(criteria);
        expectedLength = 3;
        assertEquals(expectedLength, searchedNews.size());
    }

}
