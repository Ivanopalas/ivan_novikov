package com.epam.newsmanager.util;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;


@Configuration
@ImportResource({"classpath:/spring/springConfigurationTest.xml"})
public class ApplicationConfigTest {
}
