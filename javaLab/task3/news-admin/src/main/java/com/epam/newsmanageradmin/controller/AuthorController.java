
package com.epam.newsmanageradmin.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanager.entity.Author;
import com.epam.newsmanager.service.INewsManagerService;
import com.epam.newsmanager.service.ServiceException;

@Controller
public class AuthorController {
	private final static Logger logger = LogManager.getLogger(AuthorController.class);

	@Autowired
	private INewsManagerService newsManagerService;

	@RequestMapping(value = "/updateAuthors", method = RequestMethod.GET)
	public String viewUpdateAuthors(Model model) {
		try {
			List<Author> authorList = null;
			Author authorData = new Author();
			authorList = newsManagerService.getActualAuthors();
			model.addAttribute("authorList", authorList);
			model.addAttribute("authorDataForm", authorData);
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "updateAuthors";
	}

	@RequestMapping(value = "/updateAuthors/expire", method = RequestMethod.POST)
	public String doExpireAuthors(Model model, Author authorData, BindingResult result) {
		try {
			newsManagerService.deleteAuthor(authorData.getIdAuthor());
			Author authorDataFrom = new Author();
			model.addAttribute("authorDataForm", authorDataFrom);
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "redirect:/updateAuthors";
	}

	@RequestMapping(value = "/updateAuthors/edit", method = RequestMethod.POST)
	public String doEditAuthors(Model model,
			@ModelAttribute("authorDataForm") @Valid Author authorData,
			BindingResult result) {
		try {
			if (result.hasErrors()) {
				return "updateAuthors";
			}
			newsManagerService.editAuthor(authorData);
			Author authorDataFrom = new Author();
			model.addAttribute("authorDataForm", authorDataFrom);
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "redirect:/updateAuthors";
	}

	@RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
	public String doSaveAuthors(Model model,
			@ModelAttribute("authorDataForm") @Valid Author authorData,
			BindingResult result) {
		try {
			if (result.hasErrors()) {
				return "updateAuthors";
			}
			newsManagerService.addAuthor(authorData);

			Author authorDataFrom = new Author();
			model.addAttribute("authorDataForm", authorDataFrom);
		} catch (ServiceException ex) {
			logger.error(ex);
			return "error";
		}
		return "redirect:/updateAuthors";
	}

	@ModelAttribute("authorList")
	public List<Author> populateAuthors() {
		List<Author> authorList = null;
		try {
			authorList = newsManagerService.getActualAuthors();

		} catch (ServiceException ex) {
			logger.error(ex);
		}
		return authorList;
	}

	@RequestMapping(value = "/updateAuthors/edit", method = RequestMethod.GET)
	public String doEditAuthors(Model model) {
		return "redirect:/updateAuthors";
	}

	@RequestMapping(value = "/addAuthor", method = RequestMethod.GET)
	public String doSaveAuthors(Model model) {
		return "redirect:/updateAuthors";
	}

	@RequestMapping(value = "/updateAuthors/expire", method = RequestMethod.GET)
	public String doExpireAuthors(Model model) {
		return "redirect:/updateAuthors";
	}
}