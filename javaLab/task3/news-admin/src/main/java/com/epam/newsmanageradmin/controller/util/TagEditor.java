package com.epam.newsmanageradmin.controller.util;


import com.epam.newsmanager.entity.Tag;
import com.epam.newsmanager.service.INewsManagerService;
import com.epam.newsmanager.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

@Component
public class TagEditor  extends PropertyEditorSupport {
    @Autowired
    private INewsManagerService newsManagerService;

    @Override
    public void setAsText(String text) {
        try {
            Tag tag = newsManagerService.getTagById(Long.valueOf(text));
            this.setValue(tag);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }
}
