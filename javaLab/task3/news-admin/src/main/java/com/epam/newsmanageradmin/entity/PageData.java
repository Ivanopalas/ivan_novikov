package com.epam.newsmanageradmin.entity;

import com.epam.newsmanager.entity.NewsData;

import java.io.Serializable;
import java.util.List;


public class PageData implements Serializable{

    private static final long serialVersionUID = 889508330121708305L;

    private List<NewsData> newsDataList;
    private Long allNewsCount;
    private Long currentPage;
    private Long pageSize;

    public List<NewsData> getNewsDataList() {
        return newsDataList;
    }

    public void setNewsDataList(List<NewsData> newsDataList) {
        this.newsDataList = newsDataList;
    }

    public Long getAllNewsCount() {
        return allNewsCount;
    }

    public void setAllNewsCount(Long allNewsCount) {
        this.allNewsCount = allNewsCount;
    }

    public Long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PageData pageData = (PageData) o;

        if (allNewsCount != null ? !allNewsCount.equals(pageData.allNewsCount) : pageData.allNewsCount != null)
            return false;
        if (currentPage != null ? !currentPage.equals(pageData.currentPage) : pageData.currentPage != null)
            return false;
        if (newsDataList != null ? !newsDataList.equals(pageData.newsDataList) : pageData.newsDataList != null)
            return false;
        if (pageSize != null ? !pageSize.equals(pageData.pageSize) : pageData.pageSize != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = newsDataList != null ? newsDataList.hashCode() : 0;
        result = 31 * result + (allNewsCount != null ? allNewsCount.hashCode() : 0);
        result = 31 * result + (currentPage != null ? currentPage.hashCode() : 0);
        result = 31 * result + (pageSize != null ? pageSize.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PageData{" +
                "newsDataList=" + newsDataList +
                ", allNewsCount=" + allNewsCount +
                ", currentPage=" + currentPage +
                ", pageSize=" + pageSize +
                '}';
    }
}
