package com.epam.newsmanageradmin.util;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages={"com.epam.newsmanageradmin","com.epam.newsmanager.service"})
public class SpringRootConfig {
}
