package com.epam.newsmanageradmin.util.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

import javax.sql.DataSource;

@Configuration
@EnableWebMvcSecurity
public class SecurityConfig  extends WebSecurityConfigurerAdapter {

    private static final String USERS_BY_USERNAME_QUERY =
            "select USER_NAME as username, PASSWORD as password , 1 as enabled " +
                    "from Users where USER_NAME = ?";
    private static final String AUTHORITIES_BY_USERNAME_QUERY  =
            "select USER_NAME as username, ROLE_NAME as authority " +
                    "from Users u " +
                    "join Roles r on r.USER_ID = u.USER_ID " +
                    " where USER_NAME = ?";
    @Autowired
    private DataSource dataSource;

 
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
       //         .passwordEncoder(new BCryptPasswordEncoder())
                .usersByUsernameQuery(USERS_BY_USERNAME_QUERY)
                .authoritiesByUsernameQuery(AUTHORITIES_BY_USERNAME_QUERY);
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        http.addFilterBefore(filter, CsrfFilter.class);
//        http.csrf().disable().formLogin()//.loginPage("/view/login.jsp")
//                .and().authorizeRequests()
//                .antMatchers("/news/**").hasRole("role")
//                .antMatchers("/").permitAll();
        http
        .authorizeRequests()
                .antMatchers("/login").permitAll()
                .antMatchers("/addNews/**").hasAuthority("ADMIN_ROLE")
                .antMatchers("/news/**").hasAuthority("ADMIN_ROLE")
                .antMatchers("/edit/**").hasAuthority("ADMIN_ROLE")
                .antMatchers("/updateAuthors/**").hasAuthority("ADMIN_ROLE")
                .antMatchers("/updateTags/**").hasAuthority("ADMIN_ROLE")
        .and()
                .formLogin()
                    .loginPage("/login").failureUrl("/login?error")
          		  	.usernameParameter("username").passwordParameter("password")
                    .defaultSuccessUrl("/news",true)
                    .permitAll()
        .and()
                .logout()
                    .permitAll()
        .and()
        		.logout().logoutUrl("/logout")
        			.logoutSuccessUrl("/login");
        /*.and()
        		.csrf().disable();*/
    }
}
