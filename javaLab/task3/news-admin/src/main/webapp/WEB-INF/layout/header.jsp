<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>



<table style="width: 100%">
	<tr>
		<td align="left">
			<h3>
				<spring:message code="local.header.title" />
			</h3>
		</td>
		<td align="right">Login: <sec:authentication
				property="principal.username" />
		</td>
		<td><form:form action="${pageContext.request.contextPath}/logout"
				method="POST">
				<button class="btn btn-sm" type="submit">logout</button>
			</form:form></td>
		<td><a href="?lang=en">en</a> <a href="?lang=ru">ru</a></td>

	</tr>
	<tr>
	</tr>
</table>
<hr />
