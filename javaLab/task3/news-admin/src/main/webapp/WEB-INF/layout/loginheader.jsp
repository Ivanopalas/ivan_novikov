<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<hr />
<table style="width: 100%">
	<tr>
		<td align="left">
			<h3>
				<spring:message code="local.header.title" />
			</h3>
		</td>
		<td align="right">
			<div></div> <a href="?lang=en">en</a> <a href="?lang=ru">ru</a>
		</td>

	</tr>
	<tr>
	</tr>
</table>
<hr />
