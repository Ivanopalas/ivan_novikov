<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script>
	onload = function() {
		for (var lnk = document.links, j = 0; j < lnk.length; j++)
			if (lnk[j].href == window.location.href.split('?')[0])
				lnk[j].style.cssText = 'color:black;text-decoration:none;background: #C2D9FF;';
	}
</script>

<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/css/sidebar.css"
	rel="stylesheet">

<div id="wrapper">
	<div id="sidebar-wrapper">
		<ul class="sidebar-nav">
			<!-- <li class="active"><a href="#">Overview <span class="sr-only">(current)</span></a></li> -->
			<li><a href="${pageContext.request.contextPath}/news"><spring:message
						code="local.sidebar.news" /></a></li>
			<li><a href="${pageContext.request.contextPath}/addNews"><spring:message
						code="local.sidebar.addNews" /></a></li>
			<li><a href="${pageContext.request.contextPath}/updateAuthors"><spring:message
						code="local.sidebar.updateAuthors" /></a></li>
			<li><a href="${pageContext.request.contextPath}/updateTags"><spring:message
						code="local.sidebar.updateTags" /></a></li>
		</ul>

	</div>
</div>


