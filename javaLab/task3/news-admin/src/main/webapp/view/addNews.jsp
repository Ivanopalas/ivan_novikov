<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>



<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/multiple-select.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/single-select.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.css" />
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery.multiple.select.js"></script>


<form:form method="POST" style="width: 350px; margin: 0 auto; "
	modelAttribute="newsData"
	action="${pageContext.request.contextPath}/addNews">
	<div class="form-group"> 
		<form:label path="title">
			<spring:message code="local.news.title" />
		</form:label>
		<form:input class="form-control" path="title"
			value="${news.title}" />
		<form:errors path="title" cssStyle="color: red;" />
	</div>
	<div class="form-group">
		<form:label path="modificationDate">
			<spring:message code="local.news.modificationDate" />
		</form:label>
		<form:input class="form-control" type="date" value="${fmtDate}"
			path="modificationDate" />
		<form:errors path="modificationDate" cssStyle="color: red;" />

	</div>
	<div class="form-group">
		<form:label path="shortText">
			<spring:message code="local.news.brief" />
		</form:label>
		<form:textarea class="form-control" style=" resize:vertical;" rows="2"
			path="shortText" value="${news.shortText}" />
		<form:errors path="shortText" cssStyle="color: red;" />

	</div>
	<div class="form-group">
		<form:label path="fullText">
			<spring:message code="local.news.content" />
		</form:label>
		<form:textarea class="form-control" style=" resize:vertical;" rows="5"
			path="fullText" value="${news.fullText}" />
		<form:errors path="fullText" cssStyle="color: red;" />
	</div>
	<div align="center">
		<table>
			<tr>
				<td><form:select class="ss" path="author.idAuthor">
						<div class="col-xs-2">
							<form:options items="${authorList}" itemValue="idAuthor"
								itemLabel="name" />
						</div>
						<form:errors path="author.idAuthor" cssStyle="color: red;" />
					</form:select></td>
				<td><form:select multiple="true" id="ms" path="tags">
						<form:options items="${tagList}" itemValue="idTag"
							itemLabel="tagName" />
					</form:select></td>
			</tr>
		</table>
	</div>
	<hr />

	<button type="submit" style="width: 100%;" class="btn btn-lg">
		<spring:message code="local.news.add" />
	</button>

</form:form>

<script>
	$('#ms').multipleSelect();
</script>

