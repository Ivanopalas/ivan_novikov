<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script>
function edit(inputId,editButtonId,updateButtonId,expireButtonId,cancelButtonId){
	
	$("#" + inputId)[0].disabled = false;

	$("#" + editButtonId)[0].style.display='none';
	$("#" + updateButtonId)[0].style.display='block';
	$("#" + expireButtonId)[0].style.display='block';
	$("#" + cancelButtonId)[0].style.display='block';
}

function cancel(inputId,editButtonId,updateButtonId,expireButtonId,cancelButtonId){
	
	$("#" + inputId)[0].disabled = true;

	$("#" + editButtonId)[0].style.display='block';
	$("#" + updateButtonId)[0].style.display='none';
	$("#" + expireButtonId)[0].style.display='none';
	$("#" + cancelButtonId)[0].style.display='none';
}
function submitEdit(formId) {
	
	$("#" + formId).attr("action", "${pageContext.request.contextPath}/updateTags/edit");
	$("#" + formId)[0].submit();
}
function submitExpire(formId) {
	$("#" + formId).attr("action", "${pageContext.request.contextPath}/updateTags/delete");
	$("#" + formId)[0].submit();
};
function submitSave(formId) {
	$("#" + formId)[0].submit();
};
</script>
<div align="center">
	<table class="table table-hover" style="width: 50%;">
		<form:form id="saveForm" method="POST"
			style="width: 350px; margin: 0 auto; " modelAttribute="tagDataForm"
			action="${pageContext.request.contextPath}/addTag">
			<form:errors cssStyle="color: red;" path="tagName" />
			<tr>
				<td>
					<div class="form-group">
						<form:label path="tagName">Add tag</form:label>
						<form:input class="form-control" path="tagName" value="${tagName}" />

					</div>
				</td>
			</tr>
			<tr>
				<td align="center"><button class="btn btn-sm"
						onclick="submitSave('saveForm')">save</button></td>
			</tr>
		</form:form>
	</table>
</div>
<c:forEach items="${tagList}" var="tagData">
	<c:set var="formId" value="${tagData.idTag}" />
	<div align="center">
		<table class="table table-hover" style="width: 40%;">
			<tr>
				<td colspan="3"><form:form id="${formId}" method="POST"
						style="width: 350px; margin: 0 auto; "
						modelAttribute="tagDataForm">
						<div class="form-group">
							<form:input id="${formId}i" disabled="true" class="form-control"
								path="tagName" value="${tagData.tagName}" />
							<form:hidden path="idTag" value="${tagData.idTag}" />

						</div>

					</form:form></td>
			</tr>
			<tr>
				<td align="center"><button class="btn btn-sm" id="${formId}bu"
						onclick="submitEdit(${formId})" style="display: none">update</button></td>
				<td align="center">

					<button class="btn btn-sm" id="${formId}be"
						onclick="edit('${formId}i','${formId}be','${formId}bu','${formId}bex','${formId}bc')">
						edit</button>
					<button class="btn btn-sm" id="${formId}bc"
						onclick="cancel('${formId}i','${formId}be','${formId}bu','${formId}bex','${formId}bc')"
						style="display: none">cancel</button>
				</td>
				<td align="center">
					<button class="btn btn-sm" id="${formId}bex"
						onclick="submitExpire(${formId})" style="display: none">delete</button>
				</td>

			</tr>
		</table>
	</div>
</c:forEach>
