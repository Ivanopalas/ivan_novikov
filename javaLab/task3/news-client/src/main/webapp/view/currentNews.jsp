<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<a href="${pageContext.request.contextPath}/news">back</a>

<c:set var="newsData" value="${pageData.newsDataList[0]}" />
<table class="table table-hover" style="align: center;">
	<tr bgcolor="#cccccc">
		<td><em style="font-size: 12pt;"> <b><c:out
						value="${newsData.news.title}" /></b></em> <span style="font-size: 8pt;">(by
				<c:out value="${newsData.author.name}" />)
		</span></td>
		<td align="right"><i><c:out
					value="${newsData.news.modificationDate}" /></i></td>
	</tr>
	<tr>
		<td colspan="2"><c:out value="${newsData.news.fullText}" /></td>
	</tr>
</table>
<hr />
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script
	src="${pageContext.request.contextPath}/js/commentFunctionality.js"></script>
<script src="${pageContext.request.contextPath}/js/dateFormatter.js"></script>
<script>
var prefix = '${pageContext.request.contextPath}';



$(document).ready(function() {
    $('#addComment').click(function(){
    	commentText = $('#commentText').val();
    	var commentInfo=  {
	   			"commentText": commentText,
	     		"idNews": "${newsData.news.idNews}"
    	};
    	
	 
	    $.ajax({
	        type: 'POST',
	        url:  prefix + '/addComment',
	        contentType: 'application/json; charset=utf-8',
	     
	    	 data: JSON.stringify(commentInfo),
	        dataType: "json",
	        contentType: "application/json",
	        async: true,
	        success: function(result) {
	        	createTableDate(result);
	        	document.getElementById('commentText').value='';
	        	/*  document.getElementById("newCommentDate").innerHTML += createTableDate(result); 
	        	 document.getElementById("newCommentText").innerHTML += createTableText(result);  */
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	            alert(jqXHR.status + ' cant add comment' + jqXHR.responseText);
	        }
	    });
    });
});
</script>




<table style="width: 100%;" >
	<tr>
		<td style="text-align: left" ><c:if test="${pageData.currentPage  != 0}">
				<spring:url value="/news/${pageData.currentPage - 1}"
					var="pagedLink">
				</spring:url>
				<a href="${pagedLink}">previous page</a>
			</c:if></td>
		<td style="text-align: right"><c:if
				test="${pageData.currentPage + 1 ne pageData.allNewsCount}">
				<spring:url value="/news/${pageData.currentPage + 1}"
					var="pagedLink">
				</spring:url>
				<a href="${pagedLink}">next page</a>
			</c:if></td>
	</tr>

</table>

<table id="commentTable" class="table table-condensed table-striped"
	style="margin: 0 auto; width: 30%; table-layout: fixed; word-wrap: break-word;">


	<tbody id="insertComment">
	</tbody>
	<c:if test="${!empty newsData.comments}">

		<c:forEach items="${newsData.comments}" var="comments">
			<tr>
				<th class="col-sm-2"><fmt:formatDate
						value="${comments.creationDate}" pattern="MM/dd/yyyy HH:mm" />
				</th>
			</tr>
			<tr>
				<td bgcolor="#d2d2d2"><c:out value="${comments.commentText}" /></td>
			</tr>
		</c:forEach>
	</c:if>
	<tr>
		<td><textarea style="width: 100%; resize: none;" id="commentText"></textarea>

			<div style="text-align: center;">
				<button id="addComment" style="align: center;" class="btn btn-lg">add
					comment</button>
			</div></td>
	</tr>

</table>








