package com.epam.newsmanager.dao;

import com.epam.newsmanager.entity.Author;

import java.util.List;

public interface AuthorDao {
	/**
	 * Add news author to database
	 * @return author id in database
	 */
	public Long addAuthor(Author author) throws DaoException;
	/**
	 * Set author as expired
	 */
	public void deleteAuthor(Long idAuthor) throws DaoException;

	public List<Author> getActualAuthors() throws DaoException;
	
	public List<Author> getAllAuthors() throws DaoException;
	
	public void editAuthor(Author author) throws DaoException;
	
	public Author getAuthorById(Long authorId) throws DaoException;
}
