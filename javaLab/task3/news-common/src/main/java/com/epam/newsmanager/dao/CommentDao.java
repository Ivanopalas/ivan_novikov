package com.epam.newsmanager.dao;

import java.util.List;

import com.epam.newsmanager.entity.Comment;

public interface CommentDao {
	/**
	 * Add new comment to curtain news.
	 * @param comment Comment data.
	 * @return id of added comment
	 */
	public Long addComment(Comment comment) throws DaoException;
	/**
	 * Delete comment by id
	 * @param idComment id of comment you want delete.
	 */
	public void deleteComment(Long idComment) throws DaoException;



    public Comment getCommentById(Long commentId) throws DaoException;
}
