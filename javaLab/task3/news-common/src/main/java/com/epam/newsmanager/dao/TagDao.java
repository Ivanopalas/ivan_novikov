package com.epam.newsmanager.dao;

import java.util.List;

import com.epam.newsmanager.entity.Tag;
import com.epam.newsmanager.service.ServiceException;

public interface TagDao {
    /**
     * Create new tag in database.
     * @param tag Tag data.
     * @return id of added tag
     */
	public Long addTag(Tag tag) throws DaoException;


    public Tag getTagById(Long tagId) throws DaoException;
    
    public List<Tag> getAllTags() throws DaoException;
    
    public void editTag(Tag tag) throws DaoException;
	
	public void deleteTag(Tag tag) throws DaoException;

}
 