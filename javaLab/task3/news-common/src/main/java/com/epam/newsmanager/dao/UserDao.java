package com.epam.newsmanager.dao;

import com.epam.newsmanager.entity.UserInfo;

public interface UserDao {
	public boolean addUser(UserInfo user);

}
