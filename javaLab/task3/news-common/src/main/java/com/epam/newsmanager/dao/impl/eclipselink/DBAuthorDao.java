package com.epam.newsmanager.dao.impl.eclipselink;

import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.entity.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
public class DBAuthorDao implements AuthorDao{

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public Long addAuthor(Author author) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction et = entityManager.getTransaction();
        et.begin();
        entityManager.persist(author);
        et.commit();
        return author.getIdAuthor();
    }


    public void deleteAuthor(Long idAuthor) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction et = entityManager.getTransaction();
        Author author = entityManager.find(Author.class, idAuthor);
		if(author != null){
			et.begin();
			Date date = new Date();
			author.setExpiredDate(new Timestamp(date.getTime()));
			et.commit();
		}
    }

    public List<Author> getActualAuthors() throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<Author> authors = entityManager
                .createQuery("SELECT a FROM Author a where a.expiredDate is null", Author.class)
                .getResultList();
        return authors;
    }


    public List<Author> getAllAuthors() throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<Author> authors = entityManager
                .createQuery("SELECT a FROM Author a",Author.class)
                .getResultList();
        return authors;
    }


    public void editAuthor(Author author) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction et = entityManager.getTransaction();
        Author authorInner = entityManager.find(Author.class, author.getIdAuthor());
		if(authorInner != null){}
			et.begin();
			authorInner.setName(author.getName());
			et.commit();
		}
    }


    public Author getAuthorById(Long authorId) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Author author = entityManager.find(Author.class, authorId);
        return author;
    }
}
