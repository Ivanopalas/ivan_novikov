package com.epam.newsmanager.dao.impl.eclipselink;


import com.epam.newsmanager.dao.CommentDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.entity.Comment;
import com.epam.newsmanager.entity.NewsData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

@Repository
public class DBCommentDao implements CommentDao {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }


    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }


    public Long addComment(Comment comment) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction et = entityManager.getTransaction();

        Long idComment = null;
        Long idNews = comment.getIdNews();
        try {
            et.begin();
            NewsData newsDataOfComment = entityManager.find(NewsData.class, idNews);
            comment.setNewsData(newsDataOfComment);
            if (comment.getCreationDate() == null) {
                comment.setCreationDate(new java.util.Date());
            }
            entityManager.persist(comment);
            et.commit();
        }catch (PersistenceException ex){
            throw new DaoException(ex);
        }

        return comment.getIdComment();
    }

    public void deleteComment(Long idComment) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction et = entityManager.getTransaction();
		 Comment comment = entityManager.find(Comment.class,idComment);
		 if(comment != null){
			et.begin();
			entityManager.remove(comment);
			et.commit();
		}
    }

    public Comment getCommentById(Long commentId) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Comment comment = entityManager.find(Comment.class,commentId);
        return comment;
    }
}
