package com.epam.newsmanager.dao.impl.eclipselink;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.NewsDao;
import com.epam.newsmanager.entity.NewsData;
import com.epam.newsmanager.entity.SearchCriteria;

import org.hibernate.LockMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Repository
public class DBNewsDao implements NewsDao {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public Long addNews(NewsData news) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction et = entityManager.getTransaction();
        et.begin();
        entityManager.persist(news);
        et.commit();
        return news.getIdNews();
    }


    public void editNews(NewsData news) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction et = entityManager.getTransaction();

        et.begin();
        entityManager.merge(news);
        et.commit();
    }


    public void deleteNews(Long idNews) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction et = entityManager.getTransaction();
        
        NewsData newsData = entityManager.find(NewsData.class,idNews);
		if(newsData != null){
			et.begin();
			entityManager.remove(newsData);
			et.commit();
		}
    }


    public List<NewsData> getSortedNews() throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
     /*   CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();

        Root root = cq.from(NewsData.class);

        cq.distinct(true);

       // CollectionJoin<NewsData, Order> orders = root.join(NewsData_.orders, JoinType.LEFT);
        cq.orderBy(cb.desc(cb.count(root.get("comments"))),cb.desc(root.get("modificationDate")));
     //   cq.orderBy(cb.desc(root.get("modificationDate")));


        Query query = entityManager.createQuery(cq);
        List<NewsData> news  = query.getResultList();
 */
        List<NewsData> news = entityManager
                .createQuery(
                        "SELECT DISTINCT  n FROM NewsData n JOIN n.comments c ORDER BY COUNT(c) ", NewsData.class)
                .getResultList();

      //  news.get(0).getComments().size();
        return news;
    }



    public NewsData getSingleNews(Long idNews) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        NewsData newsData = entityManager.find(NewsData.class,idNews);
        entityManager.lock(newsData, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
        return newsData;
    }


    public List<NewsData> searchNews(SearchCriteria criteria) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Object[] criterias = createCriteria(criteria);
        CriteriaBuilder cb = (CriteriaBuilder)criterias[0];
        CriteriaQuery cq = (CriteriaQuery)criterias[1];

        List<NewsData> newsData = entityManager
                .createQuery(cq)
                .setFirstResult(criteria.getNewsStartIndex().intValue())
                .setMaxResults(criteria.getNewsLastIndex().intValue() - criteria.getNewsStartIndex().intValue())
                .getResultList();

        /*newsData.sort(new  Comparator(){
			public int compare(Object arg0, Object arg1) {
				NewsData arg0_ = (NewsData)arg0;
				NewsData arg1_ = (NewsData)arg1;
				if(arg0_.getComments().size() == arg1_.getComments().size()){
					return 0;
				}
				if(arg0_.getComments().size() < arg1_.getComments().size()){
					return 1;
				}else{
					return -1;
				}	
			}
        });*/
        return newsData;
    }


    public Long countNews(SearchCriteria criteria) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Object[] criterias = createCriteria(criteria);
        CriteriaBuilder cb = (CriteriaBuilder)criterias[0];
        CriteriaQuery cq = (CriteriaQuery)criterias[1];
        return new Long( entityManager
                .createQuery(cq)
                .getResultList().size());
    }
    private Object[] createCriteria(SearchCriteria searchCriteria) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root root = cq.from(NewsData.class);
        cq.distinct(true);
        if (searchCriteria != null) {
            if (searchCriteria.getAuthorId() != null) {
                Join authorJoin = root.join("authorSet", JoinType.LEFT);
               cq.where(cb.equal(authorJoin.get("idAuthor"),searchCriteria.getAuthorId()));
              //  criteria.add(Restrictions.eq("author.idAuthor", searchCriteria.getAuthorId()));
            }
            if (searchCriteria.getTagsId() != null && searchCriteria.getTagsId().size() != 0) {
                Join tagJoin = root.join("tags", JoinType.LEFT);
                Expression<String> exp = tagJoin.get("idTag");
                Predicate predicate = exp.in(searchCriteria.getTagsId());
                cq.where(predicate);
            //    cq.where(cb.(tagJoin.get("idTag"),searchCriteria.getTagsId()));
            }
        }

        cq.orderBy();
       /* criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)

                .addOrder(Order.desc("commentCount"))
                .addOrder(Order.desc("modificationDate"));*/
        return new Object[]{cb,cq} ;
    }
}
