package com.epam.newsmanager.dao.impl.eclipselink;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.util.List;

@Repository
public class DBTagDao implements TagDao{

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public Long addTag(Tag tag) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction et = entityManager.getTransaction();
        et.begin();
        entityManager.persist(tag);
        et.commit();
        return tag.getIdTag();
    }


    public Tag getTagById(Long tagId) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Tag tag = entityManager.find(Tag.class,tagId);
        return tag;
    }


    public List<Tag> getAllTags() throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<Tag> tags = entityManager
                .createQuery("SELECT t FROM Tag t",Tag.class)
                .getResultList();
        return tags;
    }


    public void editTag(Tag tag) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction et = entityManager.getTransaction();
        Tag tagInner = entityManager.find(Tag.class, tag.getIdTag());
        et.begin();
        if(tagInner != null) {
            tagInner.setTagName(tag.getTagName());
        }
        et.commit();
    }


    public void deleteTag(Tag tag) throws DaoException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction et = entityManager.getTransaction();
        et.begin();
        Tag deleteTag = entityManager.find(Tag.class,tag.getIdTag());
        if(deleteTag != null) {
            entityManager.remove(deleteTag);
        }
        et.commit();
    }
}
