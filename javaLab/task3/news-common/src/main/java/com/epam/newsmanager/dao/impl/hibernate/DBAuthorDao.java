package com.epam.newsmanager.dao.impl.hibernate;

import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.entity.Author;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.Date;
import java.util.List;

@Transactional
@Repository
public class DBAuthorDao implements AuthorDao {
    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession(){
        return sessionFactory.getCurrentSession();
    }


	public Long addAuthor(Author author) throws DaoException {
        Session session = sessionFactory.getCurrentSession();
        
        Long idAuthor = null;
        try{
            idAuthor = (Long) session.save(author);
          //  session.flush();
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }
        return idAuthor;
    }

    public void editAuthor(Author author) throws DaoException {
        Session session = sessionFactory.getCurrentSession();
        try{
            Author authorInner = (Author) session.load(Author.class, author.getIdAuthor());
            authorInner.setName(author.getName());
  //          session.flush();
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }
    }

	public void deleteAuthor(Long idAuthor) throws DaoException {
        Session session = sessionFactory.getCurrentSession();
        try{
            Author author = (Author) session.load(Author.class, idAuthor);
            Date date = new Date();
            author.setExpiredDate(new Timestamp(date.getTime()));
   //         session.flush();
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }
    }
 
	 public Author getAuthorById(Long authorId) throws DaoException {
	        Session session = sessionFactory.getCurrentSession();
	        try{
	            Author author = (Author) session.get(Author.class, authorId);
	            return author;
	        }catch(HibernateException ex){
	            throw new DaoException(ex);
	        }
	    }
	 


	public List<Author> getActualAuthors() throws DaoException {
        Session session = sessionFactory.openSession();
        try{
            List<Author> authorList = session.createCriteria(Author.class)
                    .add(Restrictions.isNull("expiredDate")).list();
            return authorList;
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }finally {
            session.close();
        }
	}

	public List<Author> getAllAuthors() throws DaoException {
        Session session = sessionFactory.openSession();
        try{
            List<Author> authorList = session.createCriteria(Author.class).list();
            return authorList;
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }finally {
            session.close();
        }
	}
}
