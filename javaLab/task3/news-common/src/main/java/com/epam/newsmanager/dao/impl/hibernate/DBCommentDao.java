package com.epam.newsmanager.dao.impl.hibernate;

import com.epam.newsmanager.dao.CommentDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.util.DataSourceHelper;
import com.epam.newsmanager.entity.Author;
import com.epam.newsmanager.entity.Comment;

import com.epam.newsmanager.entity.NewsData;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

@Transactional
@Repository
public class DBCommentDao implements CommentDao {

	@Autowired
	private SessionFactory sessionFactory;

	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}


	
	public Long addComment(Comment comment) throws DaoException {
		 Session session = sessionFactory.getCurrentSession();
	        Long idComment = null;
            Long idNews = comment.getIdNews();
	        try{
                NewsData newsDataOfComment = (NewsData)session.load(NewsData.class,idNews);
                comment.setNewsData(newsDataOfComment);
                if(comment.getCreationDate() == null){

                    comment.setCreationDate(new java.util.Date());
                }
                idComment = (Long) session.save(comment);
                session.flush();
	        }catch(HibernateException ex){
	            throw new DaoException(ex);
	        }
	        return idComment;
	}

	public void deleteComment(Long idComment) throws DaoException {
        Session session = sessionFactory.getCurrentSession();
        try{
            Comment comment = (Comment) session.load(Comment.class,idComment);
            if(comment != null){
                session.delete(comment);
            }
  //          session.flush();
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }
	}




	public Comment getCommentById(Long commentId) throws DaoException {
        Session session = sessionFactory.openSession();
        try{
            Comment comment = (Comment) session.get(Comment.class, commentId);
            return comment;
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }finally {
            session.close();
        }
	}
}
