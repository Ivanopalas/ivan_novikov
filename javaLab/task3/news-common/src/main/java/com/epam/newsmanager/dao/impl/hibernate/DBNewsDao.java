package com.epam.newsmanager.dao.impl.hibernate;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.NewsDao;
import com.epam.newsmanager.entity.NewsData;
import com.epam.newsmanager.entity.SearchCriteria;

import org.hibernate.*;
import org.hibernate.Session.LockRequest;
import org.hibernate.criterion.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@Transactional
@Repository
public class DBNewsDao implements NewsDao {

	
	@Autowired
	private SessionFactory sessionFactory;

	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
    @Autowired
    private DataSource dataSource;




    public static final String NEWS_ID = "NEWS_ID";
    public static final String TITLE = "TITLE";
    public static final String SHORT_TEXT = "SHORT_TEXT";
    public static final String FULL_TEXT = "FULL_TEXT";
    public static final String CREATION_DATE = "CREATION_DATE";
    public static final String MODIFICATION_DATE = "MODIFICATION_DATE";

    public static final String NEWS_COUNT = "COUNT";


    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(oracle.jdbc.pool.OracleDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Long addNews(NewsData news) throws DaoException {
        Session session = sessionFactory.getCurrentSession();
        Long idNews = null;
        try{
        	
        	news.setCreationDate( new Timestamp(new java.util.Date().getTime()));
            idNews = (Long) session.save(news);
            session.flush();
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }
        return idNews;
    }

    public void editNews(NewsData news) throws DaoException {
        Session session = sessionFactory.getCurrentSession();
         try{
            // NewsData newsInner = (NewsData) 
            session.update(news);//(NewsData.class, news.getIdNews(),LockOptions.UPGRADE);
            /* newsInner.setTags(news.getTags());
             newsInner.setShortText(news.getShortText());
             newsInner.setTitle(news.getTitle());
             newsInner.setAuthorSet(news.getAuthorSet());
             newsInner.setFullText(news.getFullText());
             newsInner.setModificationDate(news.getModificationDate());
             */
   //          session.flush();
         }catch(StaleObjectStateException  opt){
        	 throw opt;
         }catch(HibernateException ex){
             throw new DaoException(ex);
         }
    }

    public void deleteNews(Long idNews) throws DaoException {
    	Session session = sessionFactory.getCurrentSession();
        try{
        	NewsData news = (NewsData) session.load(NewsData.class, idNews);
        	if(news != null){
                session.delete(news);
            }
//            session.flush();
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }
    }

    public List<NewsData> getSortedNews() throws DaoException{
        Session session = sessionFactory.getCurrentSession();
        try{
            List<NewsData> newsList = session.createCriteria(NewsData.class)
                    .addOrder(Order.desc("commentCount"))
                    .addOrder(Order.desc("modificationDate"))
                    .list();
            return newsList;
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }
    }

    public NewsData getSingleNews(Long idNews) throws DaoException {
         Session session = sessionFactory.getCurrentSession();
         try{
        	 LockRequest lockRequest = session.buildLockRequest(LockOptions.UPGRADE);
        	 NewsData news = (NewsData) session.get(NewsData.class, idNews);
        	 lockRequest.lock(news);
        	// session.lock(news, LockMode.OPTIMISTIC_FORCE_INCREMENT);
        	/* Hibernate.initialize(news.getAuthorSet());
        	 Hibernate.initialize(news.getTags());
        	 Hibernate.initialize(news.getComments());
        	 session.evict(news);*/
             return news;
         }catch(HibernateException ex){
             throw new DaoException(ex);
         }
    }

    public Long countNews(SearchCriteria searchCriteria) throws DaoException {
        try{
            Long  newsCount = (Long)createCriteria(searchCriteria)
                    .setProjection(Projections.countDistinct("idNews"))
                    .uniqueResult();
            return newsCount;
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }
    }

    public List<NewsData> searchNews(SearchCriteria searchCriteria) throws DaoException{
        try{
            List<NewsData> newsList = createCriteria(searchCriteria)
            		.setFirstResult(searchCriteria.getNewsStartIndex().intValue())
                    .setMaxResults((searchCriteria.getNewsLastIndex().intValue() - searchCriteria.getNewsStartIndex().intValue()))
                    .list();
            return newsList;
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }
    }
    private Criteria createCriteria(SearchCriteria searchCriteria) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(NewsData.class);
            if (searchCriteria != null) {
                if (searchCriteria.getAuthorId() != null) {
                    criteria.createAlias("authorSet", "author");
                    criteria.add(Restrictions.eq("author.idAuthor", searchCriteria.getAuthorId()));
                }
                if (searchCriteria.getTagsId() != null && searchCriteria.getTagsId().size() != 0) {
                    criteria.createAlias("tags", "tag");
                    criteria.add(Restrictions.in("tag.idTag", searchCriteria.getTagsId().toArray()));
                }
            }
            criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                    
                    .addOrder(Order.desc("commentCount"))
                    .addOrder(Order.desc("modificationDate"));
        return criteria;
    }


}
