package com.epam.newsmanager.dao.impl.hibernate;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.dao.util.DataSourceHelper;
import com.epam.newsmanager.entity.Tag;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public class DBTagDao implements TagDao{

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }


	public Long addTag(Tag tag) throws DaoException {
        Session session = sessionFactory.getCurrentSession();

        Long idTag = null;
        try{
            idTag = (Long) session.save(tag);
  //          session.flush();
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }
        return idTag;
    }



    public Tag getTagById(Long tagId) throws DaoException {
        Session session = sessionFactory.getCurrentSession();
        try{
            Tag tag = (Tag) session.get(Tag.class, tagId);
            return tag;
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }
    }


    public List<Tag> getAllTags() throws DaoException {
    	 Session session = sessionFactory.getCurrentSession();
        try{
            List<Tag> tagList = session.createCriteria(Tag.class).list();
            return tagList;
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }
    }

	public void editTag(Tag tag) throws DaoException {
		 Session session = sessionFactory.getCurrentSession();
       
        try{
           
            Tag tagInner = (Tag) session.load(Tag.class, tag.getIdTag());
            tagInner.setTagName(tag.getTagName());
          
        }catch(HibernateException ex){
            throw new DaoException(ex);
        }
	}

	public void deleteTag(Tag tag) throws DaoException {
		 Session session = sessionFactory.getCurrentSession();
        try{
           
            Tag tagEdit = (Tag) session.load(Tag.class,tag.getIdTag());
            if(tagEdit != null){
                session.delete(tagEdit);
            }
          
        }catch(HibernateException ex){
            
            throw new DaoException(ex);
        }
	}


}
