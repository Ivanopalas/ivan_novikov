package com.epam.newsmanager.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;


public class DataSourceHelper {
	private final static Logger logger = LogManager.getLogger(DataSourceHelper.class);

	private final static String CLOSE_STMNT_ERR = "Didn't close statement";
    private final static String CLOSE_RSET_ERR = "Didn't close result set";

	private DataSourceHelper() {}

	public static void closeConnection(
			Connection connection, DataSource dataSource, Statement statement) {
		try {
            DataSourceUtils.releaseConnection(connection, dataSource);
			statement.close();
		} catch (SQLException ex) {
			logger.warn(CLOSE_STMNT_ERR);
		}
	}

	public static void closeConnection(Connection connection, DataSource dataSource, Statement statement,
			ResultSet resultSet) {
		closeConnection(connection, dataSource, statement);
		try {
            if(resultSet != null) {
                resultSet.close();
            }
		} catch (SQLException ex) {
			logger.warn(CLOSE_RSET_ERR);
		}
	}
}
