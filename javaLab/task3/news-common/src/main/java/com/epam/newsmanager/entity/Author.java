package com.epam.newsmanager.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;


@Entity
@Table(name="AUTHORS")
public class Author implements Serializable{
    private static final long serialVersionUID = 889508330181708305L;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence_author")
    @SequenceGenerator(name = "id_sequence_author", sequenceName = "SEQ_AUTHORS_ID")
    @Column(name="AUTHOR_ID")
	private Long idAuthor;

    @Column(name="AUTHOR_NAME")
    @NotBlank
    @Length(max = 30)
    private String name;

    @Column(name="EXPIRED")
    private Timestamp expiredDate;


    @ManyToMany(cascade=CascadeType.ALL,mappedBy = "authorSet",fetch = FetchType.LAZY)
    private List<NewsData> newsData;

    public List<NewsData> getNewsData() {
        return newsData;
    }
    public void setNewsData(List<NewsData> newsData) {
        this.newsData = newsData;
    }
    public Long getIdAuthor() {
		return idAuthor;
	}
	public void setIdAuthor(Long idAuthor) {
		this.idAuthor = idAuthor;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Timestamp getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(Timestamp expiredDate) {
		this.expiredDate = expiredDate;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expiredDate == null) ? 0 : expiredDate.hashCode());
		result = prime * result + ((idAuthor == null) ? 0 : idAuthor.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (expiredDate == null) {
			if (other.expiredDate != null)
				return false;
		} else if (!expiredDate.equals(other.expiredDate))
			return false;
		if (idAuthor == null) {
			if (other.idAuthor != null)
				return false;
		} else if (!idAuthor.equals(other.idAuthor))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AuthorInfo [idAuthor=" + idAuthor + ", name=" + name + ", expiredDate=" + expiredDate + "]";
	}
	
	
}
