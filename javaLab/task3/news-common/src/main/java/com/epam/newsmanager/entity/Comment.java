package com.epam.newsmanager.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="COMMENTS")
public class Comment implements Serializable {
	private static final long serialVersionUID = 889213310181708305L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_sequence_comment")
	@SequenceGenerator(name = "id_sequence_comment", sequenceName = "SEQ_COMMENTS_ID")
	@Column(name = "COMMENT_ID")
	private Long idComment;

    @Column(name="(select count(1) from Comments c where c.NEWS_ID = NEWS_ID)")
    private Long commentCount;
    @Transient
   // @Column(name = "NEWS_ID")
    private Long idNews;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NEWS_ID", nullable = false)
	private NewsData newsData;
	
	@Column(name="COMMENT_TEXT")
    @NotBlank
    @Length(max = 100)
	private String commentText;


    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE")
	private Date creationDate;



    public Long getIdNews() {
        return idNews;
    }

    public void setIdNews(Long idNews) {
        this.idNews = idNews;
    }

    public Long getIdComment() {
        return idComment;
    }

    public void setIdComment(Long idComment) {
        this.idComment = idComment;
    }

    public NewsData getNewsData() {
        return newsData;
    }

    public void setNewsData(NewsData newsData) {
        this.newsData = newsData;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comment comment = (Comment) o;

        if (commentText != null ? !commentText.equals(comment.commentText) : comment.commentText != null) return false;
        if (creationDate != null ? !creationDate.equals(comment.creationDate) : comment.creationDate != null)
            return false;
        if (idComment != null ? !idComment.equals(comment.idComment) : comment.idComment != null) return false;
        if (newsData != null ? !newsData.equals(comment.newsData) : comment.newsData != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idComment != null ? idComment.hashCode() : 0;
        result = 31 * result + (newsData != null ? newsData.hashCode() : 0);
        result = 31 * result + (commentText != null ? commentText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "idComment=" + idComment +
            //    ", newsData=" + newsData +
                ", commentText='" + commentText + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
