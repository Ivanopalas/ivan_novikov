package com.epam.newsmanager.entity;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.SqlFragmentAlias;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.context.annotation.Lazy;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "NEWS")
public class NewsData implements Serializable{
    private static final long serialVersionUID = 549508310181708305L;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence_news")
    @SequenceGenerator(name = "id_sequence_news", sequenceName = "SEQ_NEWS_ID")
    @Column(name="NEWS_ID")
    private Long idNews;

    @Column(name="TITLE")
    @NotBlank
    @Length(max = 50)
    private String title;

    @Column(name="SHORT_TEXT")
    @NotBlank
    @Length(max = 300)
    private String shortText;

    @Column(name="FULL_TEXT")
    @NotBlank
    @Length(max = 2000)
    private String fullText;

    
    @Version
  //  @DateTimeFormat(pattern = "yyyy-MM-dd")

    @Column(name="CREATION_DATE")
    private Timestamp creationDate;

   
   // private int version;
    
   // @DateTimeFormat(pattern = "yyyy-MM-dd ")
    @Temporal(TemporalType.DATE)
    @Column(name="MODIFICATION_DATE")
    @NotNull
    private Date modificationDate;

    @ManyToMany(cascade=CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinTable(name="NEWS_AUTHORS",
            joinColumns={@JoinColumn(name="NEWS_ID")},
            inverseJoinColumns={@JoinColumn(name="AUTHOR_ID")})
   
	private List<Author> authorSet;

 
    @Transient
    private Author author;

    @ManyToMany(cascade = CascadeType.DETACH,fetch = FetchType.EAGER )
    @JoinTable(
            name = "NEWS_TAGS",
            joinColumns = @JoinColumn(name = "T_NEWS_ID"),
            inverseJoinColumns = @JoinColumn(name = "TAG_ID")
    )
	private List<Tag> tags;

    @Transient
//    @Column(name="(select count(1) from Comments c where c.NEWS_ID = NEWS_ID)")// as commentCount")
    //@Formula(value = "(select count(1) from Comments where Comments.NEWS_ID = NEWS_ID)")
    private int commentCount;


   // @OrderBy("commentCount")
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "newsData",fetch = FetchType.EAGER)
	private List<Comment> comments;

    
    
   
	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Author> getAuthorSet() {

        return authorSet;
    }
	

    public void setAuthorSet(List<Author> authorSet) {
		this.authorSet = authorSet;
	}

	public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }


    public Long getIdNews() {
        return idNews;
    }

    public void setIdNews(Long idNews) {
        this.idNews = idNews;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

   /* public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
*/
    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsData newsData = (NewsData) o;

        if (authorSet != null ? !authorSet.equals(newsData.authorSet) : newsData.authorSet != null) return false;
        if (comments != null ? !comments.equals(newsData.comments) : newsData.comments != null) return false;
        if (creationDate != null ? !creationDate.equals(newsData.creationDate) : newsData.creationDate != null)
            return false;
        if (fullText != null ? !fullText.equals(newsData.fullText) : newsData.fullText != null) return false;
        if (idNews != null ? !idNews.equals(newsData.idNews) : newsData.idNews != null) return false;
        if (modificationDate != null ? !modificationDate.equals(newsData.modificationDate) : newsData.modificationDate != null)
            return false;
        if (shortText != null ? !shortText.equals(newsData.shortText) : newsData.shortText != null) return false;
        if (tags != null ? !tags.equals(newsData.tags) : newsData.tags != null) return false;
        if (title != null ? !title.equals(newsData.title) : newsData.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idNews != null ? idNews.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
   //      result = 31 * result + (authorSet != null ? authorSet.hashCode() : 0);
   //     result = 31 * result + (tags != null ? tags.hashCode() : 0);
   //     result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NewsData{" +
                "idNews=" + idNews +
                ", title='" + title + '\'' +
                ", shortText='" + shortText + '\'' +
                ", fullText='" + fullText + '\'' +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                ", authorSet=" + authorSet +
                ", tags=" + tags +
                ", comments=" + comments +
                '}';
    }
}
