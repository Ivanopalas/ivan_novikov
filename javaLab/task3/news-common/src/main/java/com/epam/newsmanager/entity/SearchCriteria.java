package com.epam.newsmanager.entity;

import java.io.Serializable;
import java.util.List;

public class SearchCriteria implements Serializable{
    private static final long serialVersionUID = 889523410181708305L;
	private List<Long> tagsId;
	private Long authorId;
    private Long newsStartIndex;
    private Long newsLastIndex;

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public Long getNewsStartIndex() {
        return newsStartIndex;
    }

    public void setNewsStartIndex(Long newsStartIndex) {
        this.newsStartIndex = newsStartIndex;
    }

    public Long getNewsLastIndex() {
        return newsLastIndex;
    }

    public void setNewsLastIndex(Long newsLastIndex) {
        this.newsLastIndex = newsLastIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchCriteria criteria = (SearchCriteria) o;

        if (authorId != null ? !authorId.equals(criteria.authorId) : criteria.authorId != null) return false;
        if (newsLastIndex != null ? !newsLastIndex.equals(criteria.newsLastIndex) : criteria.newsLastIndex != null)
            return false;
        if (newsStartIndex != null ? !newsStartIndex.equals(criteria.newsStartIndex) : criteria.newsStartIndex != null)
            return false;
        if (tagsId != null ? !tagsId.equals(criteria.tagsId) : criteria.tagsId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tagsId != null ? tagsId.hashCode() : 0;
        result = 31 * result + (authorId != null ? authorId.hashCode() : 0);
        result = 31 * result + (newsStartIndex != null ? newsStartIndex.hashCode() : 0);
        result = 31 * result + (newsLastIndex != null ? newsLastIndex.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SearchCriteria{" +
                "tagsId=" + tagsId +
                ", authorId=" + authorId +
                ", newsStartIndex=" + newsStartIndex +
                ", newsLastIndex=" + newsLastIndex +
                '}';
    }
}
