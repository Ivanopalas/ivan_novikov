package com.epam.newsmanager.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

@Entity
@Table(name = "TAGS")
public class Tag implements Serializable{
    private static final long serialVersionUID = 789508310181708305L;

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "id_sequence_tag")
    @SequenceGenerator(name = "id_sequence_tag", sequenceName = "SEQ_TAGS_ID")
    @Column(name="TAG_ID")
	private Long idTag;

    @Column(name="TAG_NAME")
    @NotBlank
    @Length(max = 30)
	private String tagName;

    @ManyToMany(mappedBy = "tags")
    private List<NewsData> newsDataList;

	public Long getIdTag() {
		return idTag;
	}

	public void setIdTag(Long idTag) {
		this.idTag = idTag;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public List<NewsData> getNewsDataList() {
		return newsDataList;
	}

	public void setNewsDataList(List<NewsData> newsDataList) {
		this.newsDataList = newsDataList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idTag == null) ? 0 : idTag.hashCode());
		result = prime * result + ((newsDataList == null) ? 0 : newsDataList.hashCode());
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (idTag == null) {
			if (other.idTag != null)
				return false;
		} else if (!idTag.equals(other.idTag))
			return false;
		if (newsDataList == null) {
			if (other.newsDataList != null)
				return false;
		} else if (!newsDataList.equals(other.newsDataList))
			return false;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Tag [idTag=" + idTag + ", tagName=" + tagName; //+ ", newsDataList=" + newsDataList + "]";
	}
    
    
}
