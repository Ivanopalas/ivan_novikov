package com.epam.newsmanager.entity;

public enum UserRole {
	ADMIN,AUTHOR,USER,UNLOGINED
}
