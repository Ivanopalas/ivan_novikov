package com.epam.newsmanager.service;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.entity.Author;

import java.util.List;

public interface IAuthorService {
	/**
	 * Add news author to database
	 * @return author id in database
	 */
	public Long addAuthor(Author author) throws ServiceException;
	/**
	 * Set author as expired
	 */
	public void deleteAuthor(Long idAuthor) throws ServiceException;


    public List<Author> getActualAuthors() throws ServiceException;
    
    public List<Author> getAllAuthors() throws ServiceException;

    public void editAuthor(final Author author) throws ServiceException;
    
    public Author getAuthorById(Long authorId) throws ServiceException;

}
