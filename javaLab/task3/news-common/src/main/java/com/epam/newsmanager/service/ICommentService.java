package com.epam.newsmanager.service;

import java.util.List;

import com.epam.newsmanager.entity.Comment;

public interface ICommentService {
	/**
	 * Add comment to news
	 * @return comment id in database
	 */
	public Long addComment(final Comment comment) throws ServiceException;
	/**
	 * Delete comment.
	 * @param idComment id of comment in database
	 */
	public void deleteComment(final Long idComment) throws ServiceException;


    public Comment getCommentById(Long commentId) throws ServiceException;
}
