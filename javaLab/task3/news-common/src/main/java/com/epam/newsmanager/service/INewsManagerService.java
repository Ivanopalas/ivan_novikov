package com.epam.newsmanager.service;

import java.util.List;

import com.epam.newsmanager.entity.Author;
import com.epam.newsmanager.entity.Comment;
import com.epam.newsmanager.entity.NewsData;
import com.epam.newsmanager.entity.SearchCriteria;
import com.epam.newsmanager.entity.Tag;

public interface INewsManagerService {
	/**
	 * Add new news and link it with tags and author
	 * 
	 * @param news All news data to add
	 * @return id of added news
	 */
	public Long addNews(final NewsData news) throws ServiceException;

	/**
	 * Edit already situated news
	 * 
	 * @param newsData just news part, without changing comments author and so on to edit
	 */
	public void editNews(final NewsData newsData) throws ServiceException;

	/**
	 * Delete news and detach all links in database with current news
	 */
	public void deleteNews(final Long idNews) throws ServiceException;

	/**
	 * Get news sorted by comments number and modification date
	 * 
	 * @return list of sorted news
	 */
	public List<NewsData> getSortedNews() throws ServiceException;

	/**
	 * Get news by id
	 */
	public NewsData getNewsByNewsId(final Long idNews) throws ServiceException;

	/**
	 * Add new author in database
	 * 
	 * @return id of added author
	 */
	public Long addAuthor(final Author author) throws ServiceException;

	/**
	 * Set current author as expired
	 */
	public void deleteAuthor(final Long idAuthor) throws ServiceException;

	/**
	 * Add comment to certain news
	 * 
	 * @return comment id
	 */
	public Long addComment(final Comment comment) throws ServiceException;

	/**
	 * Get list of news sorted by criteria
	 */
	public List<NewsData> searchNews(final SearchCriteria criteria) throws ServiceException;

	/**
	 * Add new tag in database
	 * 
	 * @return id of added tag
	 */
	public Long addTag(final Tag tag) throws ServiceException;


	/**
	 * Delete single comment in database
	 */
	public void deleteSingleComment(final Long idComment) throws ServiceException;

	/**
	 * Count news in database
	 */
	public Long countNews(SearchCriteria criteria) throws ServiceException;

    public List<Tag> getAllTags() throws ServiceException;

    public List<Author> getActualAuthors() throws ServiceException;
    
    public List<Author> getAllAuthors() throws ServiceException;

    public Comment getCommentById(Long comment) throws ServiceException;

    public Tag getTagById(Long tagId) throws ServiceException;
    
    public void editAuthor(final Author author) throws ServiceException;
    
	public void editTag(final Tag tag) throws ServiceException;
	
	public void deleteTag(final Tag tag) throws ServiceException;

}
