package com.epam.newsmanager.service;

import java.util.List;

import com.epam.newsmanager.entity.NewsData;
import com.epam.newsmanager.entity.SearchCriteria;

public interface INewsService {
	/**
	 * Add new news in database
	 * @param news entity should contain all required fields
	 * @return id of added news in database
	 */
	public Long addNews(NewsData news) throws ServiceException;
	/**
	 * Update news in database
	 * @param news entity should contain new data and id of old news
	 */
	public void editNews(NewsData news) throws ServiceException;
	/**
	 * Delete news from database by id
	 * @param idNews id of current news in database
	 */
	public void deleteNews(Long idNews) throws ServiceException;
	/**
	 * Get list of sorted news
	 * @return list with news in right order
	 * @throws ServiceException
	 */
	public List<NewsData> getSortedNews() throws ServiceException;
	/**
	 * Get all news data by id
	 * @param idNews id of news in database
	 * @return news entity with all data
	 */
	public NewsData getNews(Long idNews) throws ServiceException;
	/**
	 * Get news filtered by criteria
	 * @param criteria criteria of filtering news, contain tag list and author
	 * @return List of news filtered by criteria
	 */
	public List<NewsData> searchByCriteria(SearchCriteria criteria) throws ServiceException;
	/**
	 * Count all news in database
	 * @return Number of all news if database
	 */
	public Long countNews(SearchCriteria criteria) throws ServiceException;

}
