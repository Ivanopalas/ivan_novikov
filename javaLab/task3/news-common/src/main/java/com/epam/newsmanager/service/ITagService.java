package com.epam.newsmanager.service;

import java.util.List;

import com.epam.newsmanager.entity.Tag;

public interface ITagService {
	/**
	 * Add new tag to database
	 * @param tag entity with all tag parameters
	 * @return id of added tag
	 */
	public Long addTag(Tag tag) throws ServiceException;

    public List<Tag> getAllTags() throws ServiceException;

    public Tag getTagById(Long tagId) throws ServiceException;
    
	public void editTag(final Tag tag) throws ServiceException;
	
	public void deleteTag(final Tag tag) throws ServiceException;

}
 