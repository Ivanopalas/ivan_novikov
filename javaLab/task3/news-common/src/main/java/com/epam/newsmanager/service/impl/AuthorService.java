package com.epam.newsmanager.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.entity.Author;
import com.epam.newsmanager.service.IAuthorService;
import com.epam.newsmanager.service.ServiceException;

import java.util.List;

@Service
public class AuthorService implements IAuthorService{
    private final static Logger logger = LogManager.getLogger(AuthorService.class);

    private final static String linkNewsErrorMessage = "Can't link news with author";
    private final static String detachNewsErrorMessage = "Can't detach news and author";

    @Autowired
    private AuthorDao authorDao;


    public Long addAuthor(Author author) throws ServiceException {
        Long idAuthor = null;
        try{
            idAuthor = authorDao.addAuthor(author);
        }catch(DaoException ex){
            throw new ServiceException(ex);
        }
        return idAuthor;
    }

    public void editAuthor(Author author) throws ServiceException {
        try{
        	authorDao.editAuthor(author);
        }catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }
    
    public void deleteAuthor(Long idAuthor) throws ServiceException {
        try {
            authorDao.deleteAuthor(idAuthor);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }




    public List<Author> getActualAuthors() throws ServiceException {
        List<Author> allAuthors = null;
        try{
            allAuthors = authorDao.getActualAuthors();
        }catch(DaoException ex){
            throw new ServiceException(ex);
        }
        return allAuthors;
    }
    public List<Author> getAllAuthors() throws ServiceException {
        List<Author> allAuthors = null;
        try{
            allAuthors = authorDao.getAllAuthors();
        }catch(DaoException ex){
            throw new ServiceException(ex);
        }
        return allAuthors;
    }

	public Author getAuthorById(Long authorId) throws ServiceException {
		Author author = null;
        try{
            author = authorDao.getAuthorById(authorId);
        }catch(DaoException ex){
            logger.error(detachNewsErrorMessage);
            throw new ServiceException(ex);
        }
        return author;
	}


}
