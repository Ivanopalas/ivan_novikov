package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.dao.CommentDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.entity.Comment;
import com.epam.newsmanager.service.ICommentService;
import com.epam.newsmanager.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService implements ICommentService {

    @Autowired
    private CommentDao commentDao;


    public Long addComment(Comment comment) throws ServiceException {
        Long idComment = null;
        try {
            idComment = commentDao.addComment(comment);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        return idComment;

    }


    public void deleteComment(Long idComment) throws ServiceException {
        try {
            commentDao.deleteComment(idComment);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

  
    public Comment getCommentById(Long commentId) throws ServiceException {
        Comment comment = null;
        try {
            comment =  commentDao.getCommentById(commentId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        return comment;
    }


}
