package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.entity.*;
import com.epam.newsmanager.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
//@Transactional(rollbackFor=ServiceException.class)
public class NewsManagerService implements INewsManagerService {
    private final static Logger logger = LogManager.getLogger(NewsManagerService.class);

    private final static String ADD_NEWS_ERROR = "Can't add new news";
    private final static String EDIT_NEWS_ERROR = "Edit news problems";
    private final static String DELETE_NEWS_ERROR = "Can't delete news";
    private final static String GET_NEWS_ERROR = "Can't get news";
    private final static String ADD_AUTHOR_ERROR = "Can't add author";
    private final static String DELETE_AUTHOR_ERROR = "Can't add author";
    private final static String SEARCH_NEWS_ERROR = "Error, can't find news";
    private final static String ADD_TAG_ERROR = "Error, can't add tag";
    private final static String attachTagError = "Error, can't attach tag to news";
    private final static String DELETE_COMMENT_ERROR = "Can't delete comment";
    private final static String COUNT_NEWS_ERROR = "Can't get news number";
    private final static String ADD_COMMENT_ERROR = "Can't add new comment";

    @Autowired
    private IAuthorService authorService;
    @Autowired
    private ICommentService commentService;
    @Autowired
    private INewsService newsService;
    @Autowired
    private ITagService tagService;


    public Long addNews(NewsData news) throws ServiceException {
        try {
        	Long authorId = news.getAuthor().getIdAuthor();
        	Author author = authorService.getAuthorById(authorId);
        	news.setAuthor(author);
            Long newsId = newsService.addNews(news);
            return newsId;
        }catch (ServiceException ex){
            logger.error(ADD_NEWS_ERROR);
            throw ex;
        }
    }

    public List<NewsData> getSortedNews() throws ServiceException {
        List<NewsData> sortedNewsData = null;
        try {
            newsService.getSortedNews();
        } catch (ServiceException ex) {
            logger.error(DELETE_NEWS_ERROR);
            throw ex;
        }
        return sortedNewsData;
    }
    public void editNews(NewsData newsData) throws ServiceException {
        try {
        	Long authorId = newsData.getAuthor().getIdAuthor();
        	Author author = authorService.getAuthorById(authorId);
        	newsData.setAuthor(author);
        	newsService.editNews(newsData);
        } catch (ServiceException ex) {
            logger.error(EDIT_NEWS_ERROR);
            throw ex;
        }
    }

    public void editAuthor(Author author) throws ServiceException {
        try {
            authorService.editAuthor(author);
        } catch (ServiceException ex) {
            logger.error(EDIT_NEWS_ERROR);
            throw ex;
        }
    }

    
    public void deleteNews(Long idNews) throws ServiceException {
        try {
            newsService.deleteNews(idNews);
        } catch (ServiceException ex) {
            logger.error(DELETE_NEWS_ERROR);
            throw ex;
        }
    }


    public NewsData getNewsByNewsId(Long idNews) throws ServiceException {
        try {
            NewsData newsData =  newsService.getNews(idNews);
            return newsData;
        } catch (ServiceException ex) {
            logger.error(GET_NEWS_ERROR);
            throw ex;
        }
    }

    public Long addAuthor(Author author) throws ServiceException {
        Long idAuthor = null;
        try {
            idAuthor = authorService.addAuthor(author);
        } catch (ServiceException ex) {
            logger.error(ADD_AUTHOR_ERROR);
            throw ex;
        }
        return idAuthor;
    }

    public List<NewsData> searchNews(SearchCriteria criteria) throws ServiceException {
        List<NewsData> newsData = null;
        try{
            newsData = newsService.searchByCriteria(criteria);
        }catch (ServiceException ex){
            logger.error(ADD_AUTHOR_ERROR);
            throw ex;
        }
    	return newsData;
    }

    public Long addTag(Tag tag) throws ServiceException {
        Long idTag = null;
        try {
            idTag = tagService.addTag(tag);
        } catch (ServiceException ex) {
            logger.error(ADD_TAG_ERROR);
            throw ex;
        }
        return idTag;
    }



    public void deleteSingleComment(Long idComment) throws ServiceException {
        try {
            commentService.deleteComment(idComment);
        } catch (ServiceException ex) {
            logger.error(DELETE_COMMENT_ERROR);
            throw ex;
        }
    }

    public Long countNews(SearchCriteria criteria) throws ServiceException {
        Long newsLength = null;
        try {
            newsLength = newsService.countNews(criteria);
        } catch (ServiceException ex) {
            logger.error(COUNT_NEWS_ERROR);
            throw ex;
        }
        return newsLength;
    }

   
    public List<Tag> getAllTags() throws ServiceException {
        List<Tag> allTags = null;
        try {
            allTags = tagService.getAllTags();
        } catch (ServiceException ex) {
            logger.error("Can't get tags");
            throw ex;
        }
        return allTags;
    }

    
    public List<Author> getActualAuthors() throws ServiceException {
        List<Author> allAuthors = null;
        try {
            allAuthors = authorService.getActualAuthors();
        } catch (ServiceException ex) {
            logger.error("Can't get authors");
            throw ex;
        }
        return allAuthors;
    }

    public List<Author> getAllAuthors() throws ServiceException {
        List<Author> allAuthors = null;
        try {
            allAuthors = authorService.getAllAuthors();
        } catch (ServiceException ex) {
            logger.error("Can't get authors");
            throw ex;
        }
        return allAuthors;
    }


    public Comment getCommentById(Long commentId) throws ServiceException {
        Comment comment = null;
        try {
            comment = commentService.getCommentById(commentId);
        } catch (ServiceException ex) {
            logger.error("Can't get authors");
            throw ex;
        }
        return comment;
    }

    public Long addComment(Comment comment) throws ServiceException {
        Long addedId = null;
        try {
            addedId = commentService.addComment(comment);
        } catch (ServiceException ex) {
            logger.error(ADD_COMMENT_ERROR);
            throw ex;
        }
        return addedId;
    }

    public void deleteAuthor(Long idAuthor) throws ServiceException {
        try {
            authorService.deleteAuthor(idAuthor);
        } catch (ServiceException ex) {
            logger.error(DELETE_AUTHOR_ERROR);
            throw ex;
        }
    }

    public Tag getTagById(Long tagId) throws ServiceException {
        Tag tag = null;
        try {
            tag = tagService.getTagById(tagId);
        } catch (ServiceException ex) {
            throw new ServiceException(ex);
        }
        return tag;
    }

    public void editTag(Tag tag) throws ServiceException {
         try {
             tagService.editTag(tag);
         } catch (ServiceException ex) {
             throw new ServiceException(ex);
         }
	}

	public void deleteTag(Tag tag) throws ServiceException {
		try {
       //     tagService.detachAllLinksFromTag(tag.getIdTag());
            tagService.deleteTag(tag);
        } catch (ServiceException ex) {
            throw new ServiceException(ex);
        }
	}

    public IAuthorService getAuthorService() {
        return authorService;
    }

    public void setAuthorService(IAuthorService authorService) {
        this.authorService = authorService;
    }

    public ICommentService getCommentService() {
        return commentService;
    }

    public void setCommentService(ICommentService commentService) {
        this.commentService = commentService;
    }

    public INewsService getNewsService() {
        return newsService;
    }

    public void setNewsService(INewsService newsService) {
        this.newsService = newsService;
    }

    public ITagService getTagService() {
        return tagService;
    }

    public void setTagService(ITagService tagService) {
        this.tagService = tagService;
    }


	


}
