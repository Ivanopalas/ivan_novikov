package com.epam.newsmanager.dao.impl;

import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.util.DataSourceHelper;
import com.epam.newsmanager.entity.Author;
import com.epam.newsmanager.entity.NewsData;
import com.epam.newsmanager.entity.Tag;
import com.epam.newsmanager.util.ApplicationConfig;
import com.epam.newsmanager.util.ApplicationConfigEclipseLinkTest;
import com.epam.newsmanager.util.ApplicationConfigTest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

import static org.junit.Assert.*;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfigEclipseLinkTest.class})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@Transactional

public class DBAuthorDaoTest {
    private final static String AUTHOR_COUNT_QUERY = "select count(1) from Authors";
    private final static String AUTHOR_EDIT_QUERY = "select AUTHOR_NAME from Authors where AUTHOR_ID = 1";
    private final static String CHECK_IS_EXPIRED_AUTHOR_QUERY = "select EXPIRED from Authors where AUTHOR_ID = ?";
    private final static String CHECK_IS_LINKED_NEWS_AND_AUTHOR_QUERY =
            "select NEWS_ID from News_Authors where AUTHOR_ID = ?";

    @Autowired
    private AuthorDao authorDao;
    @Autowired
    private DataSource dataSource;



    @Test
    public void addAuthorTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);

        Statement st = con.createStatement();
        ResultSet rs = null;

        rs = st.executeQuery(AUTHOR_COUNT_QUERY);

        rs.next();
        int authorsCountBefore = rs.getInt(1);

        Author author = new Author();
        author.setName("test author");
        authorDao.addAuthor(author);

        rs = st.executeQuery(AUTHOR_COUNT_QUERY);
        int authorsCountAfter = -1;
        if(rs.next()) {
            authorsCountAfter = rs.getInt(1);
        }

        assertEquals(authorsCountBefore + 1, authorsCountAfter);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }

    @DatabaseSetup(value = "/testdata/author/singleAuthorData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void editAuthorTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);

        Statement st = con.createStatement();
        ResultSet rs = null;

        String expectedAuthorName = "test author after edit";
        Author author = new Author();
        author.setIdAuthor(1L);
        author.setName(expectedAuthorName);
        authorDao.editAuthor(author);

        rs = st.executeQuery(AUTHOR_EDIT_QUERY);
        String actualAuthorName = null;
        if(rs.next()) {
            actualAuthorName = rs.getString(1);
        }

        assertEquals( expectedAuthorName,actualAuthorName);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }


    @DatabaseSetup(value = "/testdata/author/singleAuthorData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void getAuthorsTest() throws DaoException,SQLException{

        List<Author> authors = authorDao.getActualAuthors();
        for(Author author : authors){
            System.out.println(author.getIdAuthor()+"   ");
        }

           /* int expectedLength = 4;
            int actualLength = news.size();
            assertEquals(expectedLength, actualLength);*/
    }


    @DatabaseSetup(value = "/testdata/author/singleAuthorData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void deleteAuthorTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);

        Statement sat = con.createStatement();
        ResultSet ras = null;

        ras = sat.executeQuery("select * from Authors where AUTHOR_ID = 1");

        ras.next();
        int authorsCountBefore = ras.getInt(1);

        System.out.println(authorsCountBefore);

        Long testId = 1L;

        authorDao.deleteAuthor(testId);

        PreparedStatement st = con.prepareStatement(CHECK_IS_EXPIRED_AUTHOR_QUERY);
        st.setLong(1, testId);
        ResultSet rs;
        rs = st.executeQuery();
        rs.next();
        Timestamp authorExpiredDate =  rs.getTimestamp(1);

        assertNotNull(authorExpiredDate);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }
}
