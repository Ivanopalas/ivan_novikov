package com.epam.newsmanager.dao.impl;


import static org.junit.Assert.*;

import com.epam.newsmanager.dao.CommentDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.NewsDao;
import com.epam.newsmanager.dao.util.DataSourceHelper;
import com.epam.newsmanager.entity.Comment;
import com.epam.newsmanager.entity.NewsData;
import com.epam.newsmanager.util.ApplicationConfigEclipseLinkTest;
import com.epam.newsmanager.util.ApplicationConfigTest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfigEclipseLinkTest.class})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@Transactional
public class DBCommentDaoTest {

    @Autowired
    private CommentDao commentDao;

   /* @Autowired
    private NewsDao newsDao;*/
    @Autowired
    private DataSource dataSource;

    private final static String CHECK_NEWS_COMMENT =
            "select NEWS_ID from Comments where COMMENT_TEXT=\'test comment\'";
    private final static String CHECK_IS_DELETED_COMMENT =
            "select COMMENT_ID from Comments where COMMENT_ID = ?";
    private final static String CHECH_IS_NEWS_HAS_COMMENTS =
            "select COMMENT_ID from News n join Comments c on c.NEWS_ID=n.NEWS_ID where n.NEWS_ID=?";

    @DatabaseSetup(value = "/testdata/news/newsData.xml", type= DatabaseOperation.INSERT)
    @Test
    public void addCommentTest() throws DaoException,SQLException {
        Connection con = DataSourceUtils.doGetConnection(dataSource);

        Statement st = con.createStatement();
        ResultSet rs = null;

        Long expectedNewsId = 1L;
        Comment comment = new Comment();
        comment.setCommentText("test comment");
        comment.setIdNews(expectedNewsId);

        commentDao.addComment(comment);

        rs = st.executeQuery(CHECK_NEWS_COMMENT);
        rs.next();
        Long actualNewsId = rs.getLong(1);

        assertEquals(expectedNewsId, actualNewsId);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }

    @DatabaseSetup(value = "/testdata/comment/commentData.xml", type= DatabaseOperation.INSERT)
    @Test
    public void deleteCommentTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);

        Long testId = 1L;

        commentDao.deleteComment(testId);

        PreparedStatement st = con.prepareStatement(CHECK_IS_DELETED_COMMENT);
        st.setLong(1, testId);
        ResultSet rs;
        rs = st.executeQuery();
        Long commentId = null;
        if(rs.next()){
            commentId  =  rs.getLong(1);
        }
        assertNull(commentId);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }

    /*@DatabaseSetup(value = "/testdata/comment/commentData.xml", type= DatabaseOperation.INSERT)
    @Test
    public void deleteAllCommentsFromNewsTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);

        Long testId = 1L;

        commentDao.deleteAllCommentsFromNews(testId);

        PreparedStatement st = con.prepareStatement(CHECH_IS_NEWS_HAS_COMMENTS);
        st.setLong(1, testId);
        ResultSet rs;
        rs = st.executeQuery();
        Long commentId = null;
        if(rs.next()){
            commentId  =  rs.getLong(1);
        }
        assertNull(commentId);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }
*/
  /*  @DatabaseSetup(value = "/testdata/comment/commentData.xml", type= DatabaseOperation.INSERT)
    @Test
    public void getCommentsByNewsIdTest() throws DaoException,SQLException{

        Long testId = 1L;

        List<Comment> comments = commentDao.getCommentsByNewsID(testId);


        Integer expectedLength = 4;
        Integer actualLength = comments.size();
        assertEquals(expectedLength, actualLength);
    }*/

}
