package com.epam.newsmanager.dao.impl;

import static org.junit.Assert.*;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.NewsDao;
import com.epam.newsmanager.dao.util.DataSourceHelper;
import com.epam.newsmanager.entity.*;
import com.epam.newsmanager.util.ApplicationConfigEclipseLinkTest;
import com.epam.newsmanager.util.ApplicationConfigTest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.orm.hibernate4.SessionHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.sql.DataSource;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfigEclipseLinkTest.class})
//@TransactionConfiguration(defaultRollback=true)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@Transactional
public class DBNewsTest {

    private final static String NEWS_COUNT_QUERY = "SELECT count(1) FROM News";
    private final static String CHECK_IS_DELETED_NEWS =
            "select NEWS_ID from News where NEWS_ID = ?";


    private Session session;
    private SessionHolder holder;
    @Autowired
    private NewsDao newsDao;
    @Autowired
    private DataSource dataSource;


/*
    @Before
    public void onSetUp() throws Exception {
        System.out.println("On SetUP----");

        EntityTransaction tx = entityManagerFactory.createEntityManager().getTransaction();
       // session = sessionFactory.openSession();
        TransactionSynchronizationManager.bindResource(tx, new SessionHolder(session));

    }

    @After
    public void onTearDown() throws Exception {
        System.out.println("On onTearDown----");
// unbind and close the session.
        holder = (SessionHolder)TransactionSynchronizationManager.getResource(entityManagerFactory);
        session = holder.getSession();
        session.flush();
        SessionFactoryUtils.closeSession(session);
        TransactionSynchronizationManager.unbindResource(entityManagerFactory);
// teardown code here
    }*/


    @Test
    public void addNewsTest() throws DaoException,SQLException{
        Connection con = DataSourceUtils.doGetConnection(dataSource);
        Statement st = con.createStatement();
        ResultSet rs = null;

        rs = st.executeQuery(NEWS_COUNT_QUERY);

        rs.next();
        int newsCountBefore = rs.getInt(1);

        NewsData newsData = new NewsData();

        newsData.setFullText("test full text");
        newsData.setShortText("test short text");
        newsData.setTitle("test title");
        newsData.setModificationDate(new java.util.Date());
        newsData.setCreationDate( new Timestamp(new java.util.Date().getTime()));
     //   newsData.setNews(news);
        newsDao.addNews(newsData);
        rs = st.executeQuery(NEWS_COUNT_QUERY);
        int newsCountAfter = -1;
        if(rs.next()) {
            newsCountAfter = rs.getInt(1);
        }
        assertEquals(newsCountBefore + 1, newsCountAfter);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }

    @DatabaseSetup(value = "/testdata/news/newsData.xml", type= DatabaseOperation.INSERT)
    @Test
    public void editNewsTest() throws DaoException,SQLException,ParseException{
        String expectedFullText = "edited test full text";
        String expectedShortText = "edited test short text";
        String expectedTitle = "edited test title";

        NewsData news = newsDao.getSingleNews(1L);
        NewsData news1 = new NewsData();

        news1.setIdNews(1L);
        news1.setFullText(expectedFullText);
        news1.setShortText(expectedShortText);
        news1.setTitle(expectedTitle);
        news1.setModificationDate(news.getModificationDate());
        news1.setCreationDate(news.getCreationDate());
    //    news1.setModificationDate( new Timestamp(new java.util.Date().getTime()));


        newsDao.editNews(news1);

        NewsData editedNews = newsDao.getSingleNews(1L);

        assertEquals( expectedFullText,editedNews.getFullText());
        assertEquals( expectedShortText,editedNews.getShortText());
        assertEquals( expectedTitle,editedNews.getTitle());
    }

    @DatabaseSetup(value = "/testdata/news/linkedNewsData.xml", type= DatabaseOperation.INSERT)
    @Test
    public void deleteNewsTest() throws DaoException,SQLException{


        Long testId = 1L;

        newsDao.deleteNews(testId);
        Connection con = DataSourceUtils.doGetConnection(dataSource);
        PreparedStatement st = con.prepareStatement(CHECK_IS_DELETED_NEWS);
        st.setLong(1, testId);
        ResultSet rs;
        rs = st.executeQuery();
        Long newsId = null;
        if(rs.next()){
            newsId  =  rs.getLong(1);
        }
        assertNull(newsId);
        DataSourceHelper.closeConnection(con, dataSource, st, rs);
    }


    @DatabaseSetup(value = "/testdata/news/newsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void getSingleNewsTest() throws DaoException,SQLException{
        Long testId = 1L;

        NewsData news = newsDao.getSingleNews(testId);

        String expectedFullText = "test full";
        String expectedShortText = "test short";
        String expectedTitle = "test title";

        assertEquals(expectedFullText,news.getFullText());
        assertEquals(expectedShortText,news.getShortText());
        assertEquals(expectedTitle,news.getTitle());
    }

  //  @DatabaseSetup(value = "/testdata/news/linkedNewsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void getSortedNewsTest() throws DaoException,SQLException{

            List<NewsData> news = newsDao.getSortedNews();
        for(NewsData newsData : news){
        //    List<Tag> tags = newsData.getTags();
            System.out.println(newsData);
          /*  for(Tag tag: tags){
                System.out.println(tag.getTagName());
            }*/

        }

           /* int expectedLength = 4;
            int actualLength = news.size();
            assertEquals(expectedLength, actualLength);*/
    }

   // @DatabaseSetup(value = "/testdata/news/linkedNewsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void countNewsTest() throws DaoException,SQLException{
        SearchCriteria criteria = new SearchCriteria();
        criteria.setNewsLastIndex(100L);
        criteria.setNewsStartIndex(0L);
        criteria.setAuthorId(1L);
        List<Long> tags = new ArrayList<Long>();

        tags.add(2L);
        tags.add(1L);
        criteria.setTagsId(tags);
        List<NewsData> searchedNews = newsDao.searchNews(criteria);

        for(NewsData nd : searchedNews){
            System.out.println(nd.getIdNews());
        }
        Long actualLength = newsDao.countNews(criteria);
        System.out.println("A"+actualLength);
        Long expectedLength = 4L;
       // assertEquals(expectedLength, actualLength);
    }

   // @DatabaseSetup(value = "/testdata/news/linkedNewsData.xml", type=DatabaseOperation.INSERT)
    @Test
    public void searchNewsTest() throws DaoException,SQLException{
       // List<News> news;// = newsDao.getSortedNews();

        SearchCriteria criteria = new SearchCriteria();


        criteria.setNewsLastIndex(10L);
        criteria.setNewsStartIndex(0L);
        List<NewsData> searchedNews = newsDao.searchNews(criteria);

        for(NewsData nd : searchedNews){
            System.out.println(nd.getIdNews());
        }

        int expectedLength = 4;
      //  assertEquals(expectedLength, searchedNews.size());


        criteria.setAuthorId(1L);

        searchedNews = newsDao.searchNews(criteria);
        for(NewsData nd : searchedNews){
            System.out.println(nd.getIdNews() + "a1");
        }
        expectedLength = 2;
    //    assertEquals(expectedLength, searchedNews.size());

        List<Long> tags = new ArrayList<Long>();


        tags.add(1L);
        criteria.setTagsId(tags);

        searchedNews = newsDao.searchNews(criteria);
        for(NewsData nd: searchedNews){
            System.out.println(nd.getIdNews()+"a1t1");
        }
        expectedLength = 1;
     //   assertEquals(expectedLength, searchedNews.size());

        criteria = new SearchCriteria();

        criteria.setNewsLastIndex(100L);
        criteria.setNewsStartIndex(0L);
        tags.add(2L);
        criteria.setTagsId(tags);

        searchedNews = newsDao.searchNews(criteria);
        for(NewsData nd : searchedNews){
            System.out.println(nd.getIdNews()+"t1t2");
        }
        expectedLength = 3;
     //   assertEquals(expectedLength, searchedNews.size());
    }

}
