package com.epam.newsmanager.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.entity.Author;
import com.epam.newsmanager.service.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {
	@Mock private AuthorDao authorDao;

	@InjectMocks private AuthorService authorService ;
	
	@Test
	public void addAuthorTest() throws ServiceException, DaoException{
		Author author = mock(Author.class);
		
		authorService.addAuthor(author);
		
		verify(authorDao).addAuthor(author);
	}
	
	@Test(expected=ServiceException.class)
	public void addAuthorExceptionTest() throws ServiceException, DaoException{
		Author author = mock(Author.class);
		
		
		doThrow(DaoException.class).when(authorDao).addAuthor(author);
		
		authorService.addAuthor(author);
	}
	
	@Test
	public void deleteAuthorTest() throws ServiceException, DaoException{
		Long idAuthor = anyLong();
		
		authorService.deleteAuthor(idAuthor);
		
		verify(authorDao).deleteAuthor(idAuthor);
	}
	
	@Test(expected=ServiceException.class)
	public void deleteAuthorExceptionTest() throws ServiceException, DaoException{
		doThrow(DaoException.class).when(authorDao).deleteAuthor(anyLong());
		
		authorService.deleteAuthor(anyLong());
	}
	

}
