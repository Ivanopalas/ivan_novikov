package com.epam.newsmanager.service.impl;



import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

import com.epam.newsmanager.dao.CommentDao;
import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.entity.Comment;
import com.epam.newsmanager.service.ServiceException;
import com.epam.newsmanager.service.impl.CommentService;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {
	@Mock private CommentDao commentDao;
	
	@InjectMocks private CommentService commentService;
	
	@Test
	public void addCommentTest() throws ServiceException, DaoException {
        Comment comment = mock(Comment.class);
		Long idNews = anyLong();

		commentService.addComment(comment);
		
		verify(commentDao).addComment(comment);
	}
	
	@Test(expected=ServiceException.class)
	public void addAuthorExceptionTest() throws ServiceException, DaoException{
		doThrow(DaoException.class).when(commentDao).addComment(any(Comment.class));
		
		commentService.addComment(any(Comment.class));
	}
	


	
	@Test
	public void deleteCommentTest() throws ServiceException, DaoException{
		Long idComment = anyLong();
		
		commentService.deleteComment(idComment);
		
		verify(commentDao).deleteComment(idComment);
	}
	
	@Test(expected=ServiceException.class)
	public void deleteCommentExceptionTest() throws ServiceException, DaoException{
		doThrow(DaoException.class).when(commentDao).deleteComment(anyLong());
		
		commentService.deleteComment(anyLong());
	}
	

	
	
	
}
