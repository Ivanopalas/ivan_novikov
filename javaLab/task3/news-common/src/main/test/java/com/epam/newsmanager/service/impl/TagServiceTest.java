package com.epam.newsmanager.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

import com.epam.newsmanager.dao.DaoException;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.entity.Tag;
import com.epam.newsmanager.service.ServiceException;
import org.springframework.context.annotation.ComponentScan;

@RunWith(MockitoJUnitRunner.class)

public class TagServiceTest {
	
    @Mock private TagDao tagDao;

    @InjectMocks private TagService tagService;

    @Test
    public void addTagTest() throws ServiceException, DaoException{
        Tag tag= any(Tag.class);

        tagService.addTag(tag);

        verify(tagDao).addTag(tag);
    }

    @Test(expected=ServiceException.class)
    public void addTagExceptionTest() throws ServiceException, DaoException{
        doThrow(DaoException.class).when(tagDao).addTag(any(Tag.class));

        tagService.addTag(any(Tag.class));
    }
    

    


}
