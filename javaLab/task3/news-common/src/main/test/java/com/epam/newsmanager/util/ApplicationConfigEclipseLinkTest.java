package com.epam.newsmanager.util;

import oracle.jdbc.pool.OracleDataSource;
import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.instrument.classloading.LoadTimeWeaver;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

//@Configuration
@EnableTransactionManagement
//temp till u need dataSource
@ImportResource({"classpath:/spring/springConfigurationTest.xml"})
@PropertySource({ "classpath:databaseConfig.properties" })
@ComponentScan(basePackages={"com.epam.newsmanager.dao.linkimpl"})
@EnableLoadTimeWeaving
public class ApplicationConfigEclipseLinkTest implements LoadTimeWeavingConfigurer {
    @Autowired
    private Environment env;



    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        EclipseLinkJpaVendorAdapter vendorAdapter = new EclipseLinkJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);
        vendorAdapter.setShowSql(false);

        LocalContainerEntityManagerFactoryBean factory =
                new LocalContainerEntityManagerFactoryBean();

        Map<String, String> jpaProperties = new HashMap<String, String>();
        jpaProperties.put("eclipselink.weaving", "false");
        jpaProperties.put("loadTimeWeaver","org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver");
        jpaProperties.put("jpaDialect"," org.springframework.orm.jpa.vendor.EclipseLinkJpaDialect");
        factory.setJpaPropertyMap(jpaProperties);

        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("com.epam.newsmanager.entity");
        factory.setDataSource(dataSource());
        factory.setJpaProperties(jpaProperties());
        return factory;
    }

    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return txManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    public DataSource dataSource() {
       /* BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(env.getProperty("jdbc.driver"));
        dataSource.setUrl(env.getProperty("jdbc.url"));
        dataSource.setUsername(env.getProperty("jdbc.user"));
        dataSource.setPassword(env.getProperty("jdbc.password"));
        dataSource.setConnectionProperties("characterEncoding=utf8");

        return dataSource;*/
        OracleDataSource dataSource = null;
        try {
            dataSource = new OracleDataSource();
            // dataSource.setD(env.getProperty("jdbc.driver"));
            dataSource.setURL(env.getProperty("jdbc.url"));
            dataSource.setUser("test");
            dataSource.setPassword("test");

        }catch (SQLException ex){
            System.out.println("no dataSource");
        }
        return dataSource;

    }

    private Properties jpaProperties() {
        Properties properties = new Properties();
        //use whatever EclipseLink properties you like
        return properties;
    }

   /* @Bean
    public InstrumentationLoadTimeWeaver instrumentationLoadTimeWeaver(){
        InstrumentationLoadTimeWeaver instrumentationLoadTimeWeaver = new InstrumentationLoadTimeWeaver();
        return  instrumentationLoadTimeWeaver;
    }*/
    //@Override
    public LoadTimeWeaver getLoadTimeWeaver() {
        InstrumentationLoadTimeWeaver instrumentationLoadTimeWeaver = new InstrumentationLoadTimeWeaver();
        return  instrumentationLoadTimeWeaver;
    }
}
