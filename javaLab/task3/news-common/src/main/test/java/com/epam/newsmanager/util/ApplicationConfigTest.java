package com.epam.newsmanager.util;

import oracle.jdbc.pool.OracleDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;


//@Configuration
@EnableTransactionManagement
//temp till u need dataSource

@PropertySource({ "classpath:databaseConfig.properties" })

@ImportResource({"classpath:/spring/springConfigurationTest.xml"})
@ComponentScan(basePackages={"com.epam.newsmanager.dao.impl"})

public class ApplicationConfigTest {

    @Autowired
    private Environment env;
    @Autowired
    private DataSource dataSource;
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(restDataSource());
        sessionFactory.setPackagesToScan(new String[] { "com.epam.newsmanager.entity" });
        sessionFactory.setHibernateProperties(hibernateProperties());

        return sessionFactory;
    }

   /* @Bean
    public DatabaseConfigBean databaseConfigBean(){
        DatabaseConfigBean dcb = new DatabaseConfigBean();
        dcb.setDatatypeFactory(new OracleDataTypeFactory());
        return dcb;
    }
    @Bean
    public DatabaseDataSourceConnectionFactoryBean databaseDataSourceConnectionFactoryBean(){
        DatabaseDataSourceConnectionFactoryBean ddscfb = new DatabaseDataSourceConnectionFactoryBean();
        ddscfb.setDataSource(restDataSource());
        ddscfb.setDatabaseConfig(databaseConfigBean());
        ddscfb.setSchema("TEST");
        return ddscfb;
    }*/

    public DataSource restDataSource(){
        try {
            OracleDataSource dataSource = new OracleDataSource();
            // dataSource.setD(env.getProperty("jdbc.driver"));
            dataSource.setURL(env.getProperty("jdbc.url"));
            dataSource.setUser("test");
            dataSource.setPassword("test");
        }catch (SQLException ex){
            System.out.println("no dataSource");
        }
        return dataSource;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory);

        return txManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    private Properties hibernateProperties() {
        return new Properties() {
            {
              //  setProperty("hibernate.hbm2ddl.auto", "create-drop");
             //   setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
                setProperty("org.hibernate.transaction","true");
                setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
                setProperty("hibernate.globally_quoted_identifiers", "true");
                setProperty("hibernate.show_sql","true");
            }
        };
    }
}
