package by.epam.tasks.soundrecord.constant;

/**
 * Contains all available genres of song
 */
public enum Genres{
    ROCK,
    METAL,
    CLASSIC,
    POP,
    BALAD,
    OTHER
}
