package by.epam.tasks.soundrecord.constant;

/**
 * Contains all available instruments of orchestra
 */
public enum Instruments {
    DRUM,
    CELLO,
    BASS,
    CLARINET,
    ORGAN,
    PIANO,
    TUBA,
    TROMBONE,
    VIOLIN,
    OTHER
}
