package by.epam.tasks.soundrecord.constant;

/**
 * Contains all song types
 */
public enum SongType {
    AUTHORED,
    FOLK,
    GROUP,
    ORCHESTRA
}
