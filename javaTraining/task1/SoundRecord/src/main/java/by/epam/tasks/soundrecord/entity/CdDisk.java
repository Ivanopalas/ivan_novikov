package by.epam.tasks.soundrecord.entity;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * CdDisk class contains list of song
 */
public class CdDisk {
    private ArrayList<Song> songList;

    /**
     * Constructs CD disk with empty array of songs.
     */
    public CdDisk(){
        songList = new ArrayList<Song>();
    }
    /**
     * Constructs CD disk using array of songs.
     *
     * @param songListArg Array of songs you want to place on disk
     */
    public CdDisk(Song[] songListArg){
        this();
        setSongList(songListArg);
    }
    /**
     * Constructs CD disk using arrayList of songs.
     *
     * @param songListArg ArrayList of songs you want to place on disk
     */
    public CdDisk(ArrayList<Song> songListArg){
        this();
        this.songList.addAll(songListArg);
    }

    public ArrayList<Song> getSongList(){
        return songList;
    }

    /**
     * Get song in array
     * @return Array with song
     */
    public Song[] getArrayOfSongs(){
        return songList.toArray(new Song[songList.size()]);
    }
    public void setSongList(ArrayList<Song> songListArg){
        this.songList.clear();
        this.songList.addAll(songListArg);
    }

    /**
     * Overloaded set method. Initialize song list with array of songs.
     * @param songListArg Array of songs.
     */
    public void setSongList(Song[] songListArg){
        this.songList.clear();
        this.songList.addAll(Arrays.asList(songListArg));
    }
    /**
     * Get length of all songs on disk
     *
     * @return Time value of all songs
     */
    public Time getDiskLength(){
        Time diskLength = new Time(0L);
        if(null != songList || 0 != songList.size()){
            for(Song i: songList){
                diskLength.setTime(diskLength.getTime() + i.getLength().getTime());
            }
        }
        return diskLength;
    }

    public boolean equals(Object obj){
        if(this == obj){
            return false;
        }
        if(null == obj){
            return false;
        }
        if(obj.getClass() != this.getClass()){
            return false;
        }
        CdDisk toCompare = (CdDisk) obj;
        if(this.songList.size() != toCompare.songList.size()){ return false; }
        for(int i = 0 ; i < toCompare.songList.size() ; i++){
            Song fst = this.songList.get(i);
            Song snd = toCompare.songList.get(i);
            if(!fst.equals(snd)){
                return false;
            }
        }
        return true;
    }
    public int hashCode(){
        return 31 * songList.hashCode();
    }
    public String toString(){
        StringBuilder line = new StringBuilder(this.getClass().getName());
        line.append("@");
        line.append("song list: {");
        if(0 != this.songList.size() && null != this.songList) {
            for (Song i : this.songList) {
                line.append(i.toString());
                line.append(" ;");
            }
        }
        line.append("}");
        return line.toString();
    }
}
