package by.epam.tasks.soundrecord.entity;

import by.epam.tasks.soundrecord.constant.Genres;
import by.epam.tasks.soundrecord.constant.SongType;

import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Song is a super class of all song entities.
 */
public abstract class Song {
    private String title;
    private Time length;
    private Genres genre;
    private int year;

    /**
     * Constructs with base parameters
     */
    protected Song(){
        this.title = "untitled";
        this.length = new Time(0,0,0);
        this.genre = Genres.OTHER;
        this.year = 0;
    }
    /**
     * Constructs Song class with base parameters.
     *
     * @param title Name of song
     * @param length Length of song
     * @param genre Genre of song
     * @param year Year of making song
     * @throws IllegalArgumentException
     */
    protected Song(String title, Time length, Genres genre, int year) throws IllegalArgumentException{
        setLength(length);
        setGenre(genre);
        setYear(year);
        setTitle(title);
    }
    /**
     * Abstract method.
     *
     * @return {@link by.epam.tasks.soundrecord.constant.SongType} of current song
     */
    public abstract SongType getSongType();

    public void setLength(Time length) {
        this.length = length;
    }

    public void setGenre(Genres genre) {
        this.genre = genre;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }
    public Time getLength() {
        return length;
    }
    /**
     *
     * @return {@link by.epam.tasks.soundrecord.constant.Genres} of current song
     */
    public Genres getGenre() {
        return genre;
    }
    public int getYear() { return year; }

}
