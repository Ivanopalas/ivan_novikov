package by.epam.tasks.soundrecord.entity.song;

import by.epam.tasks.soundrecord.constant.Genres;
import by.epam.tasks.soundrecord.constant.SongType;
import by.epam.tasks.soundrecord.entity.Song;

import java.sql.Time;

/**
 * This class defines authored type of song.
 */
public class AuthoredSong extends Song {
    private String authorName = "unknown";

    /**
     * Constructs with base parameters
     */
    public AuthoredSong(){
        super();
    }
    /**
     * Constructs authored song class with base parameters.
     *
     * @param title Name of song
     * @param length Length of song
     * @param genre Genre of song
     * @param year Year of making song
     * @param authorName Name of author who wrote this song
     */
    public AuthoredSong(String title, Time length, Genres genre, int year, String authorName){
        super(title,length,genre,year);
        setAuthorName(authorName);
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
    public SongType getSongType(){
        return SongType.AUTHORED;
    }
    public String getAuthorName() {
        return authorName;
    }

    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AuthoredSong toCompare = (AuthoredSong) obj;
        if(!this.getTitle().equals(toCompare.getTitle())) { return false; }
        if(!this.getLength().equals(toCompare.getLength())) { return false; }
        if(this.getGenre() != toCompare.getGenre()) { return false; }
        if(this.getYear() != toCompare.getYear()) { return false; }
        if(!this.authorName.equals(toCompare.authorName)) { return false; }
        return true;
    }
    public int hashCode(){
        return  31*this.getYear() +
                37*(this.getTitle() == null ? 0 : this.getTitle().hashCode()) +
                41*this.getLength().hashCode() +
                43*this.getGenre().hashCode() +
                47*(authorName == null ? 0 : authorName.hashCode());
    }
    public String toString(){
        StringBuilder line = new StringBuilder();
        line.append(this.getClass().getName());
        line.append("@");
        line.append("title: ");
        line.append(this.getTitle());
        line.append(" ,length: ");
        line.append(this.getLength());
        line.append(" ,genre: ");
        line.append(this.getGenre());
        line.append(" ,year: ");
        line.append(this.getYear());
        line.append(" ,authorName ");
        line.append(authorName);
        return line.toString();
    }
}
