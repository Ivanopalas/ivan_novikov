package by.epam.tasks.soundrecord.entity.song;

import by.epam.tasks.soundrecord.constant.Genres;
import by.epam.tasks.soundrecord.constant.SongType;
import by.epam.tasks.soundrecord.entity.Song;

import java.sql.Time;

/**
 * This class defines folk type of song.
 */
public class FolkSong extends Song {
    private String country = "unknown";
    private String songStory = "unknown";

    /**
     * Constructs with base parameters
     */
    public FolkSong(){
        super();
    }
    /**
     * Constructs folk song class with base parameters.
     *
     * @param title Name of song
     * @param length Length of song
     * @param genre Genre of song
     * @param year Year of making song
     * @param country Country where this song is native
     * @param songStory Short story or history about song
     */
    public FolkSong(String title, Time length, Genres genre, int year, String country, String songStory) {
        super(title, length, genre, year);
        setSongStory(songStory);
        setCountry(country);
    }

    public void setCountry(String country) {
        this.country = country;
    }
    public void setSongStory(String songStory) {
        this.songStory = songStory;
    }
    public SongType getSongType(){
        return SongType.FOLK;
    }
    public String getCountry() {
        return country;
    }
    public String getSongStory() {
        return songStory;
    }

    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FolkSong toCompare = (FolkSong) obj;
        if(!this.getTitle().equals(toCompare.getTitle())) { return false; }
        if(!this.getLength().equals(toCompare.getLength())) { return false; }
        if(this.getGenre() != toCompare.getGenre()) { return false; }
        if(this.getYear() != toCompare.getYear()) { return false; }
        if(!this.country.equals(toCompare.country)) { return false; }
        if(!this.songStory.equals(toCompare.songStory)) { return false;}
        return true;
    }
    public int hashCode(){
        return  31*this.getYear() +
                37*(this.getTitle() == null ? 0 : this.getTitle().hashCode()) +
                41*this.getLength().hashCode() +
                43*this.getGenre().hashCode() +
                47*(country == null ? 0 : country.hashCode()) +
                53*(songStory == null ? 0 : songStory.hashCode());
    }
    public String toString(){
        StringBuilder line = new StringBuilder();
        line.append(this.getClass().getName());
        line.append("@");
        line.append("title: ");
        line.append(this.getTitle());
        line.append(" ,length: ");
        line.append(this.getLength());
        line.append(" ,genre: ");
        line.append(this.getGenre());
        line.append(" ,year: ");
        line.append(this.getYear());
        line.append(" ,country: ");
        line.append(country);
        line.append(" ,song story: ");
        line.append(songStory);
        return line.toString();
    }
}
