package by.epam.tasks.soundrecord.entity.song;

import by.epam.tasks.soundrecord.constant.Genres;
import by.epam.tasks.soundrecord.constant.SongType;
import by.epam.tasks.soundrecord.entity.Song;

import java.sql.Time;

/**
 * This class defines group type of song.
 */
public class GroupSong extends Song {
    private String groupName = "unknown";
    private String album = "unknown";
    private String recordLabel = "unknown";

    /**
     * Constructs with base parameters
     */
    public GroupSong(){
        super();
    }
    /**
     * Constructs group song class with base parameters.
     *
     * @param title Name of song
     * @param length Length of song
     * @param genre Genre of song
     * @param year Year of making song
     * @param groupName Name of group which plays this song
     * @param album Album of group with this song
     * @param recordLabel Label recorded this song
     */
    public GroupSong(String title, Time length, Genres genre, int year, String groupName,
                      String album, String recordLabel){
        super(title,length,genre,year);
        setGroupName(groupName);
        setAlbum(album);
        setRecordLabel(recordLabel);
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    public void setAlbum(String album) {
        this.album = album;
    }
    public void setRecordLabel(String recordLabel) {
        this.recordLabel = recordLabel;

    }
    public SongType getSongType(){
        return SongType.GROUP;
    }
    public String getGroupName() {
        return groupName;
    }
    public String getAlbum() {
        return album;
    }
    public String getRecordLabel() {
        return recordLabel;
    }

    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GroupSong toCompare = (GroupSong) obj;
        if(!this.getTitle().equals(toCompare.getTitle())) { return false; }
        if(!this.getLength().equals(toCompare.getLength())) { return false; }
        if(this.getGenre() != toCompare.getGenre()) { return false; }
        if(this.getYear() != toCompare.getYear()) { return false; }
        if(!this.groupName.equals(toCompare.groupName)) { return false; }
        if(!this.album.equals(toCompare.album)) { return false; }
        if(!this.recordLabel.equals(toCompare.recordLabel)) { return false; }
        return true;
    }
    public int hashCode(){
        return  31*this.getYear() +
                37*(this.getTitle() == null ? 0 : this.getTitle().hashCode()) +
                41*this.getLength().hashCode() +
                43*this.getGenre().hashCode() +
                47*(groupName == null ? 0 : groupName.hashCode()) +
                53*(album == null ? 0 : album.hashCode()) +
                59*(recordLabel == null ? 0 : recordLabel.hashCode());
    }
    public String toString(){
        StringBuilder line = new StringBuilder();
        line.append(this.getClass().getName());
        line.append("@");
        line.append("title: ");
        line.append(this.getTitle());
        line.append(" ,length: ");
        line.append(this.getLength());
        line.append(" ,genre: ");
        line.append(this.getGenre());
        line.append(" ,year: ");
        line.append(this.getYear());
        line.append(" ,group name: ");
        line.append(groupName);
        line.append(" ,album: ");
        line.append(album);
        line.append(" ,record lable: ");
        line.append(recordLabel);
        return line.toString();
    }
}
