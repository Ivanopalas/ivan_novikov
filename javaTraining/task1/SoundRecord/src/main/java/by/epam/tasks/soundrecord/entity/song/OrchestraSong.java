package by.epam.tasks.soundrecord.entity.song;

import by.epam.tasks.soundrecord.constant.Genres;
import by.epam.tasks.soundrecord.constant.Instruments;
import by.epam.tasks.soundrecord.constant.SongType;
import by.epam.tasks.soundrecord.entity.Song;

import java.sql.Time;

/**
 * This class defines orchestra type of song.
 */
public class OrchestraSong extends Song {

    private String orchestraName = "unknown";
    private String conductorName = "unknown";
    private Instruments[] instrumentList = {Instruments.OTHER};

    /**
     * Constructs with base parameters
     */
    public OrchestraSong(){
        super();
    }
    /**
     * Constructs orchestra song class with base parameters.
     *
     * @param title Name of song
     * @param length Length of song
     * @param genre Genre of song
     * @param year Year of making song
     * @param orchestraName Name of orchestra which plays current song
     * @param conductorName Name of conductor which conduct current orchestra
     * @param instrumentList List of all instruments in current orchestra
     */
    public OrchestraSong(String title, Time length, Genres genre, int year,String orchestraName,
                         String conductorName, Instruments[] instrumentList) {
        super(title, length, genre, year);
        setConductorName(conductorName);
        setOrchestraName(orchestraName);
        setInstrumentList(instrumentList);
    }

    public void setConductorName(String conductorName) {
        this.conductorName = conductorName;
    }
    public void setOrchestraName(String orchestraName) {
        this.orchestraName = orchestraName;
    }
    public void setInstrumentList(Instruments[] instrumentList) {
        Instruments[] createInstruments = new Instruments[instrumentList.length];
        for (int i = 0; i < instrumentList.length; i++) {
            createInstruments[i] = instrumentList[i];
        }
        this.instrumentList = createInstruments;
    }
    public SongType getSongType(){
        return SongType.ORCHESTRA;
    }
    public String getConductorName() {
        return conductorName;
    }
    public String getOrchestraName() {
        return orchestraName;
    }
    public Instruments[] getInstrumentList() {
        return instrumentList;
    }

    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        OrchestraSong toCompare = (OrchestraSong) obj;
        if(!this.getTitle().equals(toCompare.getTitle())) { return false; }
        if(!this.getLength().equals(toCompare.getLength())) { return false; }
        if(this.getGenre() != toCompare.getGenre()) { return false; }
        if(this.getYear() != toCompare.getYear()) { return false; }
        if(!this.orchestraName.equals(toCompare.orchestraName)) { return false; }
        if(!this.conductorName.equals(toCompare.conductorName)) { return false; }
        if(this.instrumentList.length != toCompare.instrumentList.length) { return false; }
        for (int i = 0 ; i < this.instrumentList.length ; i++){
            if( this.instrumentList[i] != toCompare.instrumentList[i])
                return false;
        }
        return true;
    }
    public int hashCode(){
        int instrumentsHashCode = 1;
        for(int i = 0 ; i < instrumentList.length ; i++){
            instrumentsHashCode = instrumentsHashCode + (i+1)*instrumentList[i].hashCode();
        }
        return  31*this.getYear() +
                37*(this.getTitle() == null ? 0 : this.getTitle().hashCode()) +
                41*this.getLength().hashCode() +
                43*this.getGenre().hashCode() +
                47*(this.orchestraName == null ? 0 : this.orchestraName.hashCode()) +
                53*(this.conductorName == null ? 0 : this.conductorName.hashCode()) +
                59*instrumentsHashCode;
    }
    public String toString(){
        StringBuilder line = new StringBuilder();
        line.append(this.getClass().getName());
        line.append("@");
        line.append("title: ");
        line.append(this.getTitle());
        line.append(" ,length: ");
        line.append(this.getLength());
        line.append(" ,genre: ");
        line.append(this.getGenre());
        line.append(" ,year: ");
        line.append(this.getYear());
        line.append(" ,orchestra name: ");
        line.append(orchestraName);
        line.append(" ,conductor name ");
        line.append(conductorName);
        line.append(" ,instruments: { " );
        if(0 != this.instrumentList.length && null != this.instrumentList) {
            for (Instruments i : this.instrumentList) {
                line.append(i);
                line.append(" ");
            }
        }
        line.append(" }");
        return line.toString();
    }
}
