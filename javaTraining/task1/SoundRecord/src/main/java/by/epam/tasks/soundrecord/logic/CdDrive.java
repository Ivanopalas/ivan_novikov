package by.epam.tasks.soundrecord.logic;

import by.epam.tasks.soundrecord.entity.CdDisk;
import by.epam.tasks.soundrecord.entity.Song;
import by.epam.tasks.soundrecord.logic.comparator.SongComparator;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Logic class performs some actions with CdDisk and array of songs.
 */
public class CdDrive{
    private CdDisk disk;

    /**
     * Constructs CD Drive with reference on CD disk object.
     *
     * @param diskArg reference on CD disk object
     */
    public CdDrive(CdDisk diskArg){
        setDisk(diskArg);
    }
    public CdDisk getDisk(){
        return this.disk;
    }
    /**
     * does not construct new field. Just keep reference.
     * @param diskArg reference on CD disk object
     */
    public void setDisk(CdDisk diskArg){
        this.disk = diskArg;
    }

    /**
     * Put array of songs in disk class.
     *
     * @param songListArg Array of songs you want to write
     * @throws IllegalArgumentException
     */
    public void burn(Song[] songListArg) throws IllegalArgumentException{
        this.disk.setSongList(songListArg);
    }

    /**
     * Sort songs on disk by genre using default comparator.
     */
    public void sortByGenre(){
        Collections.sort(disk.getSongList(), SongComparator.getInstance());
    }
    /**
     * Find song on disk in specified range.
     *
     * @param from start of range
     * @param to end of range
     * @return Array of song which satisfy specified range
     */
    public ArrayList<Song> findInRange(Time from, Time to){
        ArrayList<Song> songList = new ArrayList<Song>();
        ArrayList<Song> currSongList = this.disk.getSongList();
        if(currSongList != null && currSongList.size() != 0) {
            for (Song i : currSongList){
                Time tmp = i.getLength();
                if(tmp.getTime() < to.getTime() && tmp.getTime() > from.getTime()){
                    songList.add(i);
                }
            }
        }
        return songList;
    }
}
