package by.epam.tasks.soundrecord.logic;

import by.epam.tasks.soundrecord.constant.SongType;
import by.epam.tasks.soundrecord.entity.Song;
import by.epam.tasks.soundrecord.entity.song.AuthoredSong;
import by.epam.tasks.soundrecord.entity.song.FolkSong;
import by.epam.tasks.soundrecord.entity.song.GroupSong;
import by.epam.tasks.soundrecord.entity.song.OrchestraSong;

/**
 * Fabric constructs different types of song depend on what parameter-type you passed to the method.
 */
public class SongFabric {
    /**
     * Creates a new song basing by type
     * @param type Type of song you want to make
     * @return Reference on song extended object
     * @throws IllegalArgumentException
     */
    public static Song createSong(SongType type) throws IllegalArgumentException{
        switch (type){
            case AUTHORED:
                return new AuthoredSong();
            case FOLK:
                return new FolkSong();
            case GROUP:
                return new GroupSong();
            case ORCHESTRA:
                return new OrchestraSong();
            default:
                throw new IllegalArgumentException();
        }
    }
}
