package by.epam.tasks.soundrecord.logic.comparator;

import by.epam.tasks.soundrecord.entity.Song;

import java.util.Comparator;

public class SongComparator implements Comparator<Song>{
    private static SongComparator instance;

    public static SongComparator getInstance() {
            if (instance == null) {
                instance = new SongComparator();
            }
            return instance;
        }
    public int compare(Song song, Song t1) {
        String s1 = song.getGenre().toString();
        String s2 = t1.getGenre().toString();
        return s1.compareTo(s2);
    }
}
