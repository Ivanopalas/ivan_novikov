package by.epam.tasks.soundrecord.main;

import by.epam.tasks.soundrecord.constant.Genres;
import by.epam.tasks.soundrecord.constant.Instruments;
import by.epam.tasks.soundrecord.constant.SongType;
import by.epam.tasks.soundrecord.entity.CdDisk;
import by.epam.tasks.soundrecord.entity.Song;
import by.epam.tasks.soundrecord.entity.song.AuthoredSong;
import by.epam.tasks.soundrecord.entity.song.GroupSong;
import by.epam.tasks.soundrecord.entity.song.OrchestraSong;
import by.epam.tasks.soundrecord.logic.SongFabric;
import by.epam.tasks.soundrecord.manager.Controller;

import java.sql.Time;

/**
 * Main class acting as stub and performing some tests.
 */
public class Main {
    /**
     * Initializing classes by constant parameters
     *
     * @returns Array with initialized classes
     */
    public static Song[] initStub(){
        Song[] songList = new Song[4];

        songList[0] = SongFabric.createSong(SongType.FOLK);
        songList[0].setTitle("demo song");
        songList[0].setLength(new Time(0, 3, 1));

        songList[1] = SongFabric.createSong(SongType.GROUP);
        songList[1].setTitle("Bleed");
        songList[1].setLength(new Time(0, 5, 23));
        songList[1].setGenre(Genres.METAL);
        songList[1].setYear(2011);
        ((GroupSong)songList[1]).setAlbum("Obzen");
        ((GroupSong)songList[1]).setGroupName("Meshuggah");
        ((GroupSong)songList[1]).setRecordLabel("Nuclear Blast");

        songList[2]= SongFabric.createSong(SongType.ORCHESTRA);
        songList[2].setTitle("Run");
        songList[2].setLength(new Time(0, 10, 2));
        songList[2].setGenre(Genres.CLASSIC);
        songList[2].setYear(2000);
        ((OrchestraSong)songList[2]).setOrchestraName("Belarusian national orchestra");
        ((OrchestraSong)songList[2]).setConductorName("Alexandr Anisimov");
        ((OrchestraSong)songList[2]).setInstrumentList(new Instruments[]{Instruments.VIOLIN,
                Instruments.TROMBONE, Instruments.CELLO});

        songList[3] = SongFabric.createSong(SongType.AUTHORED);
        songList[3].setTitle("Pop song");
        songList[3].setLength(new Time(0, 3, 31));
        songList[3].setGenre(Genres.POP);
        songList[3].setYear(2010);
        ((AuthoredSong)songList[3]).setAuthorName("Ketty Perry");

        return songList;
    }
    /**
     * Demonstrating usage of classes.
     *
     * @param args arguments of command line.
     */
    public static void main(String[] args) {
        Song[] allSongList;
        allSongList = initStub();
        System.out.println("Get string value of song classes:");
        for(Song i: allSongList) {
            System.out.println(i.toString());
        }
        CdDisk disk = new CdDisk();
        System.out.println(Controller.execute(Controller.WRITE, allSongList, disk));
        System.out.println(Controller.execute(Controller.COUNT_LENGTH,disk));
        System.out.println(Controller.execute(Controller.SORT_BY_GENRE,disk));
        System.out.println(Controller.execute(Controller.FIND_IN_RANGE,disk,new Time(0,0,0), new Time(0,7,0)));

    }
}
