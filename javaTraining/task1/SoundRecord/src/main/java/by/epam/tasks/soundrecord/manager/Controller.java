package by.epam.tasks.soundrecord.manager;


import by.epam.tasks.soundrecord.entity.CdDisk;
import by.epam.tasks.soundrecord.entity.Song;
import by.epam.tasks.soundrecord.logic.CdDrive;

import java.sql.Time;
import java.util.ArrayList;

/**
 * <h>Class controller.</h>
 *
 * <p>Usage:</p>
 * <p>Put command in first parameter and other see in command description</p>
 * Commands: <p>WRITE - write array of songs on disk ( parameters: Song[], CdDisk )</p>
 *           <p>COUNT_LENGTH - get length of all songs on disk ( parameters: CdDisk )</p>
 *           <p>SORT_BY_GENRE - sort songs on disk by genre ( parameters: CdDisk )</p>
 *           <p>FIND_IN_RANGE - find songs on disk considering given range
 *                           ( parameters: CdDisk, Time, Time )</p>
 */
public final class Controller {
    public final static int WRITE = 0;
    public final static int COUNT_LENGTH = 1;
    public final static int SORT_BY_GENRE = 2;
    public final static int FIND_IN_RANGE = 3;

    private Controller(){}
    /**
     * Method executes command you passed to the method.
     * @param command Command to execute
     * @param params Parameters depend on command
     * @return Result message
     */
    public static String execute(int command, Object... params) {
        if (!validator(command, params)) {
            throw new IllegalArgumentException();
        }
        StringBuilder resultLine = new StringBuilder();
        switch (command) {
            case WRITE:
                Song[] songList = (Song[]) params[0];
                CdDisk diskWrite = (CdDisk) params[1];
                CdDrive drive = new CdDrive(diskWrite);

                drive.burn(songList);
                resultLine.append("Wrote songs: \n");
                for(Song i: songList){
                    resultLine.append(i.getTitle() + "\n");
                }
                return resultLine.toString();
            case COUNT_LENGTH:
                CdDisk disk = (CdDisk) params[0];
                return disk.getDiskLength().toString();
            case SORT_BY_GENRE:
                CdDisk diskToSort = (CdDisk)params[0];
                CdDrive cdDrive = new CdDrive(diskToSort);

                cdDrive.sortByGenre();
                resultLine.append("Disk sorted by genre");
                return resultLine.toString();
            case FIND_IN_RANGE:
                CdDisk diskToFind = (CdDisk)params[0];
                Time from = (Time)params[1];
                Time to = (Time)params[2];
                CdDrive driveCd = new CdDrive(diskToFind);

                ArrayList<Song> listOfSongs = driveCd.findInRange(from, to);
                if(listOfSongs.size() == 0){
                    resultLine.append("No songs");
                }else{
                    for(Song i: listOfSongs){
                        resultLine.append(i.getTitle()+" "+i.getLength()+"\n");
                    }
                }
                return resultLine.toString();
            default:
                resultLine.append("Wrong command");
                return resultLine.toString();
        }
    }
    private static boolean validator(int command, Object... params){
        switch (command){
            case Controller.WRITE:
                if(params.length > 2){
                    return false;
                }
                if(params[0] == null) {
                    return false;
                }
                if(params[0].getClass() != Song[].class) {
                    return false;
                }
                Song[] tmpSong = (Song[]) params[0];
                if (tmpSong.length == 0) {
                    return false;
                }
                if(params[1] == null){
                    return false;
                }
                if(params[1].getClass() != CdDisk.class){
                    return false;
                }
                return true;
            case Controller.COUNT_LENGTH:
                if (params.length > 1){
                    return false;
                }
                if(params[0] == null){
                    return false;
                }
                if(params[0].getClass() != CdDisk.class){
                    return false;
                }
                return true;
            case Controller.SORT_BY_GENRE:
                if(params.length > 1){
                    return false;
                }
                if(params[0] == null){
                    return false;
                }
                if(params[0].getClass() != CdDisk.class){
                    return false;
                }
                return true;
            case Controller.FIND_IN_RANGE:
                if (params.length > 3){
                    return false;
                }
                if(params[0] == null){
                    return false;
                }
                if(params[0].getClass() != CdDisk.class){
                    return false;
                }
                if(params[1] == null){
                    return false;
                }
                if(params[1].getClass() != Time.class){
                    return false;
                }
                if(params[2] == null){
                    return false;
                }
                if(params[2].getClass() != Time.class){
                    return false;
                }
                return true;
            default:
                return false;

        }

    }
}
