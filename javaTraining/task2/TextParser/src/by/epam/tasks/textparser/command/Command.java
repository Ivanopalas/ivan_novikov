package by.epam.tasks.textparser.command;

import by.epam.tasks.textparser.controller.bean.TextParseRequest;
import by.epam.tasks.textparser.controller.bean.TextParseResponse;

public interface Command {
    public TextParseResponse execute(TextParseRequest req);
}
