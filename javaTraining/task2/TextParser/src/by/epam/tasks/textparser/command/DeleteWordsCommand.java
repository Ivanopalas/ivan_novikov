package by.epam.tasks.textparser.command;

import by.epam.tasks.textparser.controller.bean.TextParseRequest;
import by.epam.tasks.textparser.controller.bean.TextParseResponse;
import by.epam.tasks.textparser.entity.text.Text;
import by.epam.tasks.textparser.logic.LogicException;
import by.epam.tasks.textparser.logic.TextAction;
import org.apache.log4j.Logger;

public class DeleteWordsCommand implements Command {
    private static final Logger logger =  Logger.getLogger(DeleteWordsCommand.class);
    private static boolean debugEnabled = true;


    @Override
    public TextParseResponse execute(TextParseRequest req) {
        TextParseResponse response;
        TextAction actions = new TextAction();
        Text resultText;
        try{
            resultText = new Text();
            resultText.setTextComponentList(req.getText().getTextComponentList());
            response = new TextParseResponse();
            if(req.getLengthForDeleteAction() != 0) {
                actions.deleteByLength(resultText,req.getLengthForDeleteAction());
            }
            response.setText(resultText);
        }catch (LogicException ex){
            logger.error("Deleting words error", ex);
            response =  new TextParseResponse("Deleting words exception",ex);
        }
       return response;
    }
}
