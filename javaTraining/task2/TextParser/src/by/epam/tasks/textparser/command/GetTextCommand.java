package by.epam.tasks.textparser.command;

import by.epam.tasks.textparser.controller.bean.TextParseRequest;
import by.epam.tasks.textparser.controller.bean.TextParseResponse;
import by.epam.tasks.textparser.entity.text.Text;
import by.epam.tasks.textparser.logic.LogicException;
import by.epam.tasks.textparser.logic.TextAction;
import org.apache.log4j.Logger;

public class GetTextCommand implements Command {
    private static final Logger logger =  Logger.getLogger(GetTextCommand.class);
    private static boolean debugEnabled = true;


    @Override
    public TextParseResponse execute(TextParseRequest request) {
        TextParseResponse response;
        TextAction actions = new TextAction();
        Text textFromFile;
        try{
            textFromFile = actions.getTextFromFile(request.getFileName());
            response = new TextParseResponse();
            response.setText(textFromFile);
        }catch (LogicException ex){
            logger.error("Getting text error", ex);
            response = new TextParseResponse("Text getting error",ex);
        }
        return response;
    }
}
