package by.epam.tasks.textparser.command;

import by.epam.tasks.textparser.controller.bean.TextParseRequest;
import by.epam.tasks.textparser.controller.bean.TextParseResponse;
import by.epam.tasks.textparser.entity.text.Text;
import by.epam.tasks.textparser.logic.LogicException;
import by.epam.tasks.textparser.logic.TextAction;
import org.apache.log4j.Logger;

public class SwapWordsCommand implements Command {
    private static final Logger logger =  Logger.getLogger(SwapWordsCommand.class);
    private static boolean debugEnabled = true;


    @Override
    public TextParseResponse execute(TextParseRequest req) {
        TextParseResponse response;
        TextAction actions = new TextAction();
        Text resultText;
        try {
            resultText = new Text();
            resultText.setTextComponentList(req.getText().getTextComponentList());
            actions.swapWords(resultText);
            response = new TextParseResponse();
            response.setText(resultText);
        }catch (LogicException ex){
            logger.error("Swapping words error", ex);
            response = new TextParseResponse("Swapping words exception", ex);
        }
        return response;
    }
}
