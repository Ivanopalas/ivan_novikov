package by.epam.tasks.textparser.controller;

import by.epam.tasks.textparser.command.Command;
import by.epam.tasks.textparser.command.DeleteWordsCommand;
import by.epam.tasks.textparser.command.GetTextCommand;
import by.epam.tasks.textparser.command.SwapWordsCommand;

import java.util.HashMap;
import java.util.Map;

public class CommandHelper {
    private Map<CommandName,Command> commands = new HashMap<CommandName, Command>();
    public CommandHelper(){
        commands.put(CommandName.GET_TEXT, new GetTextCommand());
        commands.put(CommandName.DELETE_WORDS, new DeleteWordsCommand());
        commands.put(CommandName.SWAP_WORDS, new SwapWordsCommand());
    }
    public Command getCommand(CommandName cmnd){
        return commands.get(cmnd);
    }

}
