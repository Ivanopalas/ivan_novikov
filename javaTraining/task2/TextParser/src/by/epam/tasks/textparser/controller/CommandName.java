package by.epam.tasks.textparser.controller;

public enum CommandName {
    DELETE_WORDS,
    GET_TEXT,
    SWAP_WORDS
}
