package by.epam.tasks.textparser.controller;


import by.epam.tasks.textparser.command.Command;
import by.epam.tasks.textparser.controller.bean.TextParseRequest;
import by.epam.tasks.textparser.controller.bean.TextParseResponse;
import org.apache.log4j.Logger;

public class TextParseManager {
    private static final Logger logger =  Logger.getLogger(TextParseManager.class);
    private static CommandHelper ch = new CommandHelper();
    private static boolean debugEnabled = true;

    public TextParseResponse execute(CommandName commandToEx, TextParseRequest req){
        TextParseResponse response;
        try {
            CommandName cmd = commandToEx;
            Command command;
            command = ch.getCommand(cmd);
            if(debugEnabled) {
                logger.debug("starting executing " + cmd + " command");
            }
            response = command.execute(req);
            if(debugEnabled) {
                logger.debug("command executed successful");
            }

        }catch (IllegalArgumentException ex){
            logger.error("wrong arguments in manager");
            response = new TextParseResponse("Wrong argument", ex);
        }catch (NullPointerException ex){
            logger.error("NullPointerException in manager");
            response = new TextParseResponse("Null pointer", ex);
        }
        return response;
    }
}
