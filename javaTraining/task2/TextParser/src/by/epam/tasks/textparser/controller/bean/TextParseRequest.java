package by.epam.tasks.textparser.controller.bean;

import by.epam.tasks.textparser.entity.text.Text;

public class TextParseRequest {
    private String fileName;
    private int lengthForDeleteAction;
    private Text text;

    public void setFileName(String fileNameArg){
        this.fileName = fileNameArg;
    }

    public void setLengthForDeleteAction(int lengthForDeleteActionArg) {
        this.lengthForDeleteAction = lengthForDeleteActionArg;
    }

    public void setText(Text textArg) {
        this.text = textArg;
    }

    public int getLengthForDeleteAction() {
        return lengthForDeleteAction;
    }

    public String getFileName() {
        return fileName;
    }

    public Text getText() {
        return text;
    }
}
