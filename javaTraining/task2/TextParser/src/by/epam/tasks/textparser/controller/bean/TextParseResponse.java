package by.epam.tasks.textparser.controller.bean;

import by.epam.tasks.textparser.entity.text.Text;

public class TextParseResponse {
    private boolean error;
    private String errorMessage;
    private Exception exception;
    private Text text;
    public TextParseResponse(){
        text = null;
    }
    public TextParseResponse(String err, Exception ex){
        this.error = true;
        this.errorMessage = err;
        this.exception = ex;
    }

    public Text getText() {
        return text;
    }
    public Exception getException() {
        return exception;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public boolean isError(){
        return error;
    }
}
