package by.epam.tasks.textparser.dao;

public class DaoException extends Exception {

    private Exception hiddenException;

    public DaoException(String err) {
        super(err);
    }

    public DaoException(String err, Exception e) {
        super(err);
        hiddenException = e;
    }

    public Exception getDaoException() {
        return hiddenException;
    }

}
