package by.epam.tasks.textparser.dao;

import by.epam.tasks.textparser.dao.parser.ParagraphParser;
import by.epam.tasks.textparser.dao.parser.SentenceParser;
import by.epam.tasks.textparser.dao.parser.TextParser;
import by.epam.tasks.textparser.entity.text.Text;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Scanner;


public class TextDao {
    private static final Logger logger =  Logger.getLogger(TextDao.class);
    private static boolean debugEnabled = true;

    public static boolean isDebugEnabled(){
        return debugEnabled;
    }

    public static void setDebugEnabled(boolean debugEnabled) {
        TextDao.debugEnabled = debugEnabled;
    }

    public Text readFile(String filename) throws DaoException{
        StringBuilder allText = new StringBuilder();
        try {
            Scanner scan = new Scanner(new File(filename));
            if(isDebugEnabled()){
                logger.debug("file " + filename + " read");
            }
            while (scan.hasNextLine()) {
                allText.append(scan.nextLine());
                allText.append("\n");
            }
        }catch (FileNotFoundException ex){
            logger.error("file not found", ex);
            throw new DaoException("source not found", ex);
        }catch (Exception ex){
            logger.error("unknown exception",ex);
            throw new DaoException("unknown exception", ex);
        }
        SentenceParser sentenceParser = new SentenceParser();
        ParagraphParser paragraphParser = new ParagraphParser(sentenceParser);
        TextParser textParser = new TextParser(paragraphParser);
        return textParser.parse(allText.toString());
    }


}