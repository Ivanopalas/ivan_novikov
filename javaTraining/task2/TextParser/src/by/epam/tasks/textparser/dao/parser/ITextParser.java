package by.epam.tasks.textparser.dao.parser;

import by.epam.tasks.textparser.entity.TextComponent;

public interface ITextParser {
    public TextComponent parse(String lineToParse);

}
