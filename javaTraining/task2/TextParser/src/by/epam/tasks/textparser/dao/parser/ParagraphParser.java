package by.epam.tasks.textparser.dao.parser;

import by.epam.tasks.textparser.entity.text.Paragraph;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ParagraphParser implements ITextParser{
    private static final Logger logger =  Logger.getLogger(ParagraphParser.class);
    private static String REG_EX = "(?s)(([A-Z1-9]|1\\.|1\\.1\\.|1\\.2\\.)" +
            "[^\\.]{5,}((Statement \\n)+|(s \\n)+|[^1]\\.\\n?))";
    private static Pattern PATTERN = Pattern.compile(ParagraphParser.REG_EX,Pattern.DOTALL);
    private static boolean debugEnabled = true;
    private ITextParser nextParser;


    public ParagraphParser(){}
    public ParagraphParser(ITextParser parserArg){
        setNextParser(parserArg);
    }
    public void setNextParser(ITextParser nextParserArg){
        this.nextParser = nextParserArg;
    }

    public Paragraph parse(String paragraphToParse){
        Matcher matcher = PATTERN.matcher(paragraphToParse.toString());
        ArrayList<String> sentencesString = new ArrayList<String>();
        while (matcher.find()){
            sentencesString.add(matcher.group());
        }
        Paragraph parag = new Paragraph();
        if(nextParser != null) {
            for (String i : sentencesString) {
                parag.add(nextParser.parse(i));
            }
        }
        if(debugEnabled) {
            logger.debug("paragraph parsed");
        }
        return parag;
    }
}
