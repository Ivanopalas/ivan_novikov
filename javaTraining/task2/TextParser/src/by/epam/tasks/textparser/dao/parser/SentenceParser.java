package by.epam.tasks.textparser.dao.parser;

import by.epam.tasks.textparser.entity.TextComponent;
import by.epam.tasks.textparser.entity.text.LetterBlock;
import by.epam.tasks.textparser.entity.text.Sentence;
import by.epam.tasks.textparser.entity.text.UnknownTextPart;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class SentenceParser implements ITextParser{
    private static final Logger logger =  Logger.getLogger(SentenceParser.class);
    private static boolean debugEnabled = true;

    private static String REG_EX = "[A-Za-z]+|([0-9-\\.:\\(\\),\"'%=>;\\n])";
    private static Pattern PATTERN = Pattern.compile(SentenceParser.REG_EX);


    public Sentence parse(String sentenceToParse){
        Matcher match = PATTERN.matcher(sentenceToParse);
        Sentence sent = new Sentence();
        TextComponent componentToAdd;
        while(match.find()){
            if(match.group(1) != null) {
                componentToAdd = new UnknownTextPart(match.group(1));
            }else{
                componentToAdd = new LetterBlock(match.group(0));
            }
                 sent.add(componentToAdd);
        }
        if(debugEnabled){
            logger.debug("sentence parsed");
        }
        return sent;
    }
}
