package by.epam.tasks.textparser.dao.parser;

import by.epam.tasks.textparser.entity.text.LetterBlock;
import by.epam.tasks.textparser.entity.text.Text;
import org.apache.log4j.Logger;


import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class TextParser implements ITextParser{
    private static final Logger logger =  Logger.getLogger(TextParser.class);
    private static boolean debugEnabled = true;

    private static String REG_EX = "(?s)(class|void).{0,20}[\\{].{60,420}[\\}]( \\n)?";
    private static Pattern PATTERN = Pattern.compile(TextParser.REG_EX, Pattern.DOTALL);
    private ITextParser nextParser;

    public TextParser(){
        nextParser = null;
    }
    public TextParser(ITextParser parserArg){
        setNextParser(parserArg);
    }

    public void setNextParser(ITextParser nextParserArg){
        this.nextParser = nextParserArg;
    }
    public Text parse(String textToParse){
        Text parsedText = new Text();
        Matcher matcher = PATTERN.matcher(textToParse.toString());
        ArrayList<String> codeBlocks = new ArrayList<String>();

        while(matcher.find()){
            codeBlocks.add(matcher.group());
        }

        String[] stringArrayValue = PATTERN.split(textToParse.toString());
        if(nextParser != null) {
            for (int i = 0; i < codeBlocks.size(); ++i) {
                parsedText.add(nextParser.parse(stringArrayValue[i]));
                parsedText.add(new LetterBlock(codeBlocks.get(i)));
            }
            int length = stringArrayValue.length - 1;
            parsedText.add(nextParser.parse(stringArrayValue[length]));
        }
        if(debugEnabled){
            logger.debug("text parsed");
        }
        return parsedText;

    }

}
