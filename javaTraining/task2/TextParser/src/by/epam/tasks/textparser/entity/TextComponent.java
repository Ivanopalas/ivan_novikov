package by.epam.tasks.textparser.entity;

public interface TextComponent {
    public void add(TextComponent component);
    public boolean remove(TextComponent component);
    public String getStringValue();
}
