package by.epam.tasks.textparser.entity.text;

import by.epam.tasks.textparser.entity.TextComponent;

public class LetterBlock implements TextComponent {
    private String word;

    public LetterBlock(String wordArg){
        setWord(wordArg);
    }
    @Override
    public void add(TextComponent component) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(TextComponent component) {
        throw new UnsupportedOperationException();
    }

    public void setWord(String wordArg) {
        this.word = wordArg;
    }
    public String getWord(){
        return word;
    }

    @Override
    public String getStringValue() {
        return " " + word;
    }

    public boolean equals(Object obj){
        if(obj == null){
            return false;
        }
        if(this == obj){
            return true;
        }
        if(obj.getClass() != this.getClass()){
            return false;
        }
        LetterBlock toCompare = (LetterBlock)obj;
        return word.equals(toCompare.getWord());
    }
    public int hashCode(){
        return 31 * word.hashCode();
    }
    public String toString(){
        return this.getClass() + "@" + "Word: " + word;
    }
}
