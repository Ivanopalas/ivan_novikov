package by.epam.tasks.textparser.entity.text;

import by.epam.tasks.textparser.entity.TextComponent;

import java.util.ArrayList;

public class Paragraph implements TextComponent {
    private ArrayList<Sentence> sentenceList;

    public Paragraph(){
        sentenceList = new ArrayList<Sentence>();
    }

    public void setSentenceList(ArrayList<Sentence> sentenceListArg) {
        this.sentenceList = new ArrayList<Sentence>();
        this.sentenceList.addAll(sentenceListArg);
    }
    public ArrayList<Sentence> getSentenceList() {
        return sentenceList;
    }

    @Override
    public void add(TextComponent component) {
        sentenceList.add((Sentence)component);
    }

    @Override
    public boolean remove(TextComponent component) {
        return sentenceList.remove(component);
    }

    @Override
    public String getStringValue() {
        StringBuilder line = new StringBuilder();
        for(Sentence i : sentenceList){
            line.append(i.getStringValue());
        }
        return line.toString() + "\n";
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if(this == obj){
            return true;
        }
        if(obj.getClass() != this.getClass()){
            return false;
        }
        Paragraph toCompare = (Paragraph)obj;
        ArrayList<Sentence> toCompareList = toCompare.getSentenceList();
        if(sentenceList.size() != toCompareList.size()){
            return false;
        }
        for(int i = 0 ; i < sentenceList.size() ; i++){
            if(!sentenceList.get(i).equals(toCompareList.get(i))){
                return false;
            }
        }
        return true;
    }
    public int hashCode(){
        return 31 * sentenceList.hashCode();
    }
    public String toString(){
        StringBuilder line = new StringBuilder();
        line.append(getClass());
        line.append("@");
        line.append("wordList: { ");
        for(Sentence i : sentenceList){
            line.append(i.getStringValue());
            line.append(", ");
        }
        line.append("}");
        return line.toString();
    }
}
