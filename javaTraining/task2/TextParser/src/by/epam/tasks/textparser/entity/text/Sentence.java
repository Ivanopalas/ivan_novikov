package by.epam.tasks.textparser.entity.text;

import by.epam.tasks.textparser.entity.TextComponent;

import java.util.ArrayList;

public class Sentence implements TextComponent {
    private ArrayList<LetterBlock> wordList;

    public Sentence(){
        wordList = new ArrayList<LetterBlock>();
    }

    public void setWordList(ArrayList<LetterBlock> wordList) {
        this.wordList = new ArrayList<LetterBlock>();
        this.wordList.addAll(wordList);
    }
    public ArrayList<LetterBlock> getWordList() {
        return wordList;
    }
    @Override
    public void add(TextComponent component) {
        if(!(component instanceof LetterBlock)){
            throw new IllegalArgumentException();
        }
        wordList.add((LetterBlock)component);
    }

    @Override
    public boolean remove(TextComponent component) {
        return wordList.remove(component);
    }

    @Override
    public String getStringValue() {
        StringBuilder line = new StringBuilder();
        for(LetterBlock i : wordList){
            line.append(i.getStringValue());
        }
        return line.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if(this == obj){
            return true;
        }
        if(obj.getClass() != this.getClass()){
            return false;
        }
        Sentence toCompare = (Sentence)obj;
        ArrayList<LetterBlock> toCompareList = toCompare.getWordList();
        if(wordList.size() != toCompareList.size()){
            return false;
        }
        for(int i = 0 ; i < wordList.size() ; i++){
            if(!wordList.get(i).equals(toCompareList.get(i))){
                return false;
            }
        }
        return true;
    }
    public int hashCode(){
        return 31 * wordList.hashCode();
    }
    public String toString(){
        StringBuilder line = new StringBuilder();
        line.append(getClass());
        line.append("@");
        line.append("wordList: { ");
        for(LetterBlock i : wordList){
            line.append(i.getStringValue());
            line.append(", ");
        }
        line.append("}");
        return line.toString();
    }
}
