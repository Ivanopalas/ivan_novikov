package by.epam.tasks.textparser.entity.text;

import by.epam.tasks.textparser.entity.TextComponent;

import java.util.ArrayList;

public class Text implements TextComponent {
    private ArrayList<TextComponent> textComponentList;

    public Text(){
        textComponentList = new ArrayList<TextComponent>();
    }

    public void setTextComponentList(ArrayList<TextComponent> textComponentListArg) {
        this.textComponentList = new ArrayList<TextComponent>();
        this.textComponentList.addAll(textComponentListArg);
    }

    public ArrayList<TextComponent> getTextComponentList() {
        return textComponentList;
    }

    public TextComponent getComponent(int key){
        TextComponent obj = textComponentList.get(key);
        return obj;
    }
    @Override
    public void add(TextComponent component) {
        textComponentList.add(component);
    }

    @Override
    public boolean remove(TextComponent component) {
        return textComponentList.remove(component);
    }

    @Override
    public String getStringValue() {
        StringBuilder line = new StringBuilder();
        for(TextComponent i : textComponentList){
            line.append(i.getStringValue());
        }
        return line.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if(this == obj){
            return true;
        }
        if(obj.getClass() != this.getClass()){
            return false;
        }
        Text toCompare = (Text)obj;
        ArrayList<TextComponent> toCompareList = toCompare.getTextComponentList();
        if(textComponentList.size() != toCompareList.size()){
            return false;
        }
        for(int i = 0 ; i < textComponentList.size() ; i++){
            if(!textComponentList.get(i).equals(toCompareList.get(i))){
                return false;
            }
        }
        return true;
    }

    public int hashCode(){
        return 31 * textComponentList.hashCode();
    }

    public String toString(){
        StringBuilder line = new StringBuilder();
        line.append(getClass());
        line.append("@");
        line.append("wordList: { ");
        for(TextComponent i : textComponentList){
            line.append(i.getStringValue());
            line.append("; ");
        }
        line.append("}");
        return line.toString();
    }
}
