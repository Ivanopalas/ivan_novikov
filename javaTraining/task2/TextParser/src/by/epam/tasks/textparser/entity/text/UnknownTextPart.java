package by.epam.tasks.textparser.entity.text;

public class UnknownTextPart extends LetterBlock {
    private Character cymbal;

    public UnknownTextPart(String word){
        super(word);
        if(word.length() == 1){
            cymbal = new Character(word.charAt(0));
        }
    }

    public void setCymbal(Character cymbal) {
        this.cymbal = cymbal;
    }

    public boolean isCymbal(){
        if(cymbal != null){
            return true;
        }
        return false;
    }
    @Override
    public String getStringValue() {
        return super.getWord();
    }

    @Override
    public String toString() {
        return this.getClass()+"@"+"cymbal: "+cymbal.toString();
    }
    public boolean equals(Object obj){
        if(obj == null){
            return false;
        }
        if(this == obj){
            return true;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        UnknownTextPart toCompare = (UnknownTextPart)obj;
        if(this.cymbal != toCompare.cymbal){
            return false;
        }
        return true;
    }
    public int hashCode(){
        return 31*cymbal.hashCode();
    }
}