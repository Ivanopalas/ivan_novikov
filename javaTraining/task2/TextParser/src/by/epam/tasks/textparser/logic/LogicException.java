package by.epam.tasks.textparser.logic;

public class LogicException extends Exception {

    private Exception hiddenException;
    public LogicException(String err) {
        super(err);
    }

    public LogicException(String err, Exception e) {
        super(err);
        hiddenException = e;
    }

    public Exception getLogicException() {
        return hiddenException;
    }

}
