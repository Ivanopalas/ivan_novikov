package by.epam.tasks.textparser.logic;

import by.epam.tasks.textparser.dao.DaoException;
import by.epam.tasks.textparser.dao.TextDao;
import by.epam.tasks.textparser.entity.*;
import by.epam.tasks.textparser.entity.text.*;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;


public class TextAction {
    private static Logger logger = Logger.getLogger(TextAction.class);
    private boolean debugEnabled = true;
    private static final int DELETE_COMMAND = 0;
    private static final int SWAP_COMMAND = 1;

    public Text getTextFromFile(String filename) throws LogicException{
        TextDao daoLayer = new TextDao();
        Text textFromFile;
        try {
             textFromFile = daoLayer.readFile(filename);
        }catch (DaoException ex){
            logger.error("Getting text from file logic exception", ex);
            throw new LogicException("file reading exception",ex);
        }
        logger.debug("Got file from file: " + filename);
        return textFromFile;
    }
    public void swapWords(Text currentText) throws LogicException{
        if(currentText == null){
            logger.error("wrong argument in swapWords method");
            throw new LogicException("wrong argument",new IllegalArgumentException());
        }
        passThroughText(currentText,SWAP_COMMAND,0);


    }
    public void deleteByLength(Text currentText,int length) throws LogicException{
        if(currentText == null){
            logger.error("wrong argument in deleteByLength method");
            throw new LogicException("wrong argument",new IllegalArgumentException());
        }
        passThroughText(currentText,DELETE_COMMAND,length);
    }

    private void passThroughText(Text currentText, int command,int length){
        ArrayList<TextComponent> paragraphs = currentText.getTextComponentList();

        for( TextComponent i :paragraphs){
            if(i.getClass() == Paragraph.class){
                Paragraph curParag = (Paragraph)i;
                ArrayList<Sentence> sentences = curParag.getSentenceList();
                for(Sentence j: sentences){
                    if(command == SWAP_COMMAND) {
                        swapWordsHelper(j);
                    }
                    if(command == DELETE_COMMAND){
                        deleteWordsHelper(j, length);
                    }
                }
            }
        }
    }

    private void deleteWordsHelper(Sentence currentSentence, int length){
        ArrayList<LetterBlock> words = currentSentence.getWordList();
        for(int i = 0 ; i < words.size(); i++){
            if(words.get(i).getStringValue().length() - 1 == length) {
                if (words.get(i).getStringValue().matches("^ [BCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz].*")) {
                    words.remove(i);
                }
            }
        }
    }
    private void swapWordsHelper(Sentence currentSentence){
        ArrayList<LetterBlock> words = currentSentence.getWordList();
        if(words.size() > 1) {
            int maxIndex = words.size() - 1;
            int firstIndex = 0;
            int lastIndex = maxIndex;

            for (int i = 0; words.get(i).getClass() != LetterBlock.class && i != maxIndex; i++) {
                firstIndex = i;
                firstIndex++;
            }
            for (int i = maxIndex; words.get(i).getClass() != LetterBlock.class && i != 0; i--) {
                lastIndex = i;
                lastIndex--;
            }
            if(firstIndex != maxIndex || firstIndex!= lastIndex) {
                Collections.swap(words, firstIndex, lastIndex);
            }
        }
    }
}
