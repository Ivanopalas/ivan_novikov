package by.epam.tasks.textparser.view;


import by.epam.tasks.textparser.controller.CommandName;
import by.epam.tasks.textparser.controller.TextParseManager;
import by.epam.tasks.textparser.controller.bean.TextParseRequest;
import by.epam.tasks.textparser.controller.bean.TextParseResponse;
import by.epam.tasks.textparser.entity.text.Text;

public class Main {
    public static void main(String[] args) {
        TextParseManager manager = new TextParseManager();
        TextParseRequest request = new TextParseRequest();
        request.setFileName("task.txt");
        TextParseResponse response = manager.execute(CommandName.GET_TEXT,request);
        Text textFromFile;
        if(!response.isError()){
            textFromFile = response.getText();
            System.out.println(textFromFile.getStringValue());
            request.setText(textFromFile);
            request.setLengthForDeleteAction(3);
            response = manager.execute(CommandName.DELETE_WORDS,request);
            Text newTextFromFile = response.getText();
            System.out.println("==============================================================");
            System.out.println(newTextFromFile.getStringValue());
        }

    }

}
