package by.epam.tasks.callcenter.entity;

import by.epam.tasks.callcenter.logic.User;

public class Operator {
    private int id;
    private User currentUser;

    public Operator(int id){
        this.id = id;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public int getId() {
        return id;
    }
}
