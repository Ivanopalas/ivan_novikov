package by.epam.tasks.callcenter.logic;

import by.epam.tasks.callcenter.entity.Operator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class CallCenter {
    private final static Logger log = LogManager.getLogger(CallCenter.class);
    private boolean debugEnable = true;

    private ArrayBlockingQueue<Operator> operatorList;
    private LinkedBlockingQueue<User> userList;
    private ConcurrentHashMap<User,Operator> workingOperators;

    public CallCenter(int operatorSize){

        this.operatorList = new ArrayBlockingQueue<Operator>(operatorSize);
        for(int i = 0 ; i < operatorSize ; i++){
            this.operatorList.add(new Operator(i));
        }
        this.workingOperators = new ConcurrentHashMap<User, Operator>();
        this.userList = new LinkedBlockingQueue<User>();
        if(debugEnable) {
            log.debug("call center initialized");
        }
    }

    public void setDebugEnable(boolean debugEnable) {
        this.debugEnable = debugEnable;
    }

    public boolean makeCall(final User currentUser,final long waitingTime) throws CallCenterException{
        Operator currentOperator;
        try {
            if(debugEnable){
                log.debug(currentUser.getUserName()+" connecting");
            }
            this.userList.put(currentUser); //put user in waiting queue
            currentOperator = lockOperator(currentUser,waitingTime); //trying lock free operator and wait if necessary
            if(currentOperator == null) {
                if(debugEnable){
                    log.debug(currentUser.getUserName() + " failed connecting");
                }
                return false;
            }
            currentUser.setCurrentOperator(currentOperator); //init user
            currentOperator.setCurrentUser(currentUser); //init operator
            this.userList.poll(); //poll user from waiting queue
            if(debugEnable){
                log.debug(currentUser.getUserName() + " connected successfully");
            }
        }catch (InterruptedException ex){
            throw new CallCenterException("connection fail",ex);
        }
        return true;
    }
    public boolean endCall(User currentUser){
        if(debugEnable){
            log.debug(currentUser.getUserName() + " trying disconnect");
        }
        if(!unlockOperator(currentUser)){ //trying unlock operator
            return false;
        }
        if(debugEnable){
            log.debug(currentUser.getUserName() + " ended calling successfully");
        }
        return true;
    }
    private Operator lockOperator(User incomeUser,long timeWaiting) throws CallCenterException{
        if(incomeUser == null){ //valid user
            String errMessage = " wrong income user";
            log.error(errMessage);
            throw new CallCenterException(errMessage ,new IllegalArgumentException());
        }
        Operator currentOperator;
        if(debugEnable){
            log.debug(incomeUser.getUserName() + " getting free operator");
        }
        try {
            //trying get free operator from blocking queue and wait if no free operator
            currentOperator = operatorList.poll(timeWaiting, TimeUnit.SECONDS);
        }catch (InterruptedException ex){
            throw new CallCenterException("user was interrupted while waiting",ex);
        }
        if(currentOperator != null) {
            this.workingOperators.put(incomeUser, currentOperator); //put operator and user in map
            if(debugEnable){
                log.debug(incomeUser.getUserName() + " got operator num " + currentOperator.getId());
            }
        }else{
            if(debugEnable){
                log.debug(incomeUser.getUserName() + " failed getting operator");
            }
        }
        return currentOperator;
    }

    private boolean unlockOperator(User innerUser){
        //check speaking user in system and get operator connected with current user
        Operator workingOperator = workingOperators.get(innerUser);
        if(workingOperator == null){
            log.error("no such user connected with any operator");
            return false;
        }
        try {
            if(debugEnable){
                log.debug(innerUser.getUserName() + " trying unlock operator num " + workingOperator.getId());
            }
            this.workingOperators.remove(innerUser); //remove operator
            this.operatorList.put(workingOperator); //put operator in free-operator array
            workingOperator.setCurrentUser(null);
            innerUser.setCurrentOperator(null);
            if(debugEnable){
                log.debug(innerUser.getUserName() + " unlocked operator num " + workingOperator.getId());
            }
        }catch (InterruptedException ex){
            log.error("disconnecting user error");
            return false;
        }
        return true;
    }
}
