package by.epam.tasks.callcenter.logic;

public class CallCenterException extends Exception {
    private String errorMessage;
    public CallCenterException(String errorMessage, Exception ex){
        super(errorMessage,ex);
        this.errorMessage = errorMessage;
    }
    public CallCenterException(String errorMessage){
        super(errorMessage);
        this.errorMessage = errorMessage;
    }
    public String getErrorMessage() {
        return errorMessage;
    }
}
