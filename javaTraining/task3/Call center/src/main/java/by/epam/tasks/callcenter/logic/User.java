package by.epam.tasks.callcenter.logic;

import by.epam.tasks.callcenter.entity.Operator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class User implements Runnable{
    private final static Logger log = LogManager.getLogger(User.class);

    private String userName;
    private CallCenter usedCallCenter;
    private Operator currentOperator;

    public User(String userName,CallCenter usedCallCenter){
        this.usedCallCenter = usedCallCenter;
        this.userName = userName;
    }


    public void setCurrentOperator(Operator currentOperator) {
        this.currentOperator = currentOperator;
    }

    public boolean makeACall(long waitingTimeInSeconds){
        try {
            if(!usedCallCenter.makeCall(this, waitingTimeInSeconds)){
                return false;
            }
        }catch (CallCenterException ex){
            log.error("calling error",ex);
            return false;
        }
        return true;
    }
    public boolean endCall(){
        return usedCallCenter.endCall(this);
    }
    public String getUserName() {
        return userName;
    }

    public void run(){
        try {
            while (true){
                if(makeACall(4L)){  //call and wait if busy
                    TimeUnit.SECONDS.sleep(4); //speaking imitation
                    endCall();
                }else{
                    TimeUnit.SECONDS.sleep(4); //just wait and call one more time
                }
            }
        }catch (InterruptedException ex){
            log.error("user was interrupted while waiting",ex);
        }
    }


}
