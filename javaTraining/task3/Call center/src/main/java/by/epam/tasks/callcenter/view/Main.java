package by.epam.tasks.callcenter.view;


import by.epam.tasks.callcenter.logic.User;
import by.epam.tasks.callcenter.logic.CallCenter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    //work demonstration
    public static void main(String[] args) {
        CallCenter mainCallCenter = new CallCenter(10);
        Thread[] allUsers = new Thread[25];
        ExecutorService executor = Executors.newFixedThreadPool(25);
        for(int i = 0 ; i < 25 ; i++){
            allUsers[i] = new Thread(new User("user num "+(i+1),mainCallCenter));
            executor.execute(allUsers[i]);
        }
    }
}
