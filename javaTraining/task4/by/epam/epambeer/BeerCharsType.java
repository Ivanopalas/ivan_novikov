//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.03.24 at 02:12:12 AM MSK 
//


package by.epam.epambeer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for beer-chars-type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="beer-chars-type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="alc" type="{http://www.epam.by/epambeer}non-alc-type"/>
 *           &lt;element name="alcohol" type="{http://www.epam.by/epambeer}alcohol-type"/>
 *         &lt;/choice>
 *         &lt;element name="transparency" type="{http://www.epam.by/epambeer}transparency-type"/>
 *         &lt;element name="calories" type="{http://www.epam.by/epambeer}calories-type"/>
 *         &lt;element name="filtered" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="vessels" type="{http://www.epam.by/epambeer}vessels-type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "beer-chars-type", propOrder = {
    "alc",
    "alcohol",
    "transparency",
    "calories",
    "filtered",
    "vessels"
})
public class BeerCharsType {

    protected Boolean alc;
    protected AlcoholType alcohol;
    @XmlElement(required = true)
    protected String transparency;
    protected int calories;
    protected boolean filtered;
    @XmlElement(required = true)
    protected VesselsType vessels;

    /**
     * Gets the value of the alc property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlc() {
        return alc;
    }

    /**
     * Sets the value of the alc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlc(Boolean value) {
        this.alc = value;
    }

    /**
     * Gets the value of the alcohol property.
     * 
     * @return
     *     possible object is
     *     {@link AlcoholType }
     *     
     */
    public AlcoholType getAlcohol() {
        return alcohol;
    }

    /**
     * Sets the value of the alcohol property.
     * 
     * @param value
     *     allowed object is
     *     {@link AlcoholType }
     *     
     */
    public void setAlcohol(AlcoholType value) {
        this.alcohol = value;
    }

    /**
     * Gets the value of the transparency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransparency() {
        return transparency;
    }

    /**
     * Sets the value of the transparency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransparency(String value) {
        this.transparency = value;
    }

    /**
     * Gets the value of the calories property.
     * 
     */
    public int getCalories() {
        return calories;
    }

    /**
     * Sets the value of the calories property.
     * 
     */
    public void setCalories(int value) {
        this.calories = value;
    }

    /**
     * Gets the value of the filtered property.
     * 
     */
    public boolean isFiltered() {
        return filtered;
    }

    /**
     * Sets the value of the filtered property.
     * 
     */
    public void setFiltered(boolean value) {
        this.filtered = value;
    }

    /**
     * Gets the value of the vessels property.
     * 
     * @return
     *     possible object is
     *     {@link VesselsType }
     *     
     */
    public VesselsType getVessels() {
        return vessels;
    }

    /**
     * Sets the value of the vessels property.
     * 
     * @param value
     *     allowed object is
     *     {@link VesselsType }
     *     
     */
    public void setVessels(VesselsType value) {
        this.vessels = value;
    }

}
