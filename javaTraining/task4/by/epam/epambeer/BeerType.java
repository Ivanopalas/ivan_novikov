//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.03.24 at 02:12:12 AM MSK 
//


package by.epam.epambeer;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for beer-type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="beer-type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Dark"/>
 *     &lt;enumeration value="Pale amber"/>
 *     &lt;enumeration value="Unpasteurized beer"/>
 *     &lt;enumeration value="Lager"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "beer-type")
@XmlEnum
public enum BeerType {

    @XmlEnumValue("Dark")
    DARK("Dark"),
    @XmlEnumValue("Pale amber")
    PALE_AMBER("Pale amber"),
    @XmlEnumValue("Unpasteurized beer")
    UNPASTEURIZED_BEER("Unpasteurized beer"),
    @XmlEnumValue("Lager")
    LAGER("Lager");
    private final String value;

    BeerType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BeerType fromValue(String v) {
        for (BeerType c: BeerType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
