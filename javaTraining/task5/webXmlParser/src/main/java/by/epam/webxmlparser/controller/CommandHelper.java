package by.epam.webxmlparser.controller;

import by.epam.webxmlparser.logic.ICommand;
import by.epam.webxmlparser.logic.impl.NoCommand;
import by.epam.webxmlparser.logic.impl.ParseCommand;

import java.util.HashMap;
import java.util.Map;

public class CommandHelper {
    private static final CommandHelper instance = new CommandHelper();
    private Map<CommandName,ICommand> commands;

    public CommandHelper(){
        commands = new HashMap<CommandName, ICommand>();
        commands.put(CommandName.PARSE_COMMAND,new ParseCommand());
        commands.put(CommandName.NO_COMMAND,new NoCommand());
    }
    public static CommandHelper getInstance(){
        return instance;
    }
    public ICommand getCommand(String cName){
        CommandName commandName = CommandName.valueOf(cName.toUpperCase());
        ICommand command;
        if(commandName != null){
            command = commands.get(commandName);
        }else{
            command = commands.get(CommandName.NO_COMMAND);
        }
        return command;
    }
}
