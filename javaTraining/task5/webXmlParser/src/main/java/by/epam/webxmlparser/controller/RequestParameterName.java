package by.epam.webxmlparser.controller;

public class RequestParameterName {
    public static final String PAGE_DATA = "pageData";
    public static final String PARSER_TYPE = "parserType";
    public static final String FILE_NAME = "filename";
    public static final String COMMAND_NAME = "commandName";
}
