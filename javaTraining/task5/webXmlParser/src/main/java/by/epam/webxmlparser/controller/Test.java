package by.epam.webxmlparser.controller;

import by.epam.webxmlparser.dao.impl.XMLSaxParser;
import by.epam.webxmlparser.entity.XMLData;

public class Test {
    public static void main(String[] args) {
        XMLSaxParser sax = new XMLSaxParser();
        try {
           XMLData data =  sax.parse("beer.xml");
            System.out.println(data.getName() +" "+data.getAbv());
        }catch (Exception ex){}

    }
}
