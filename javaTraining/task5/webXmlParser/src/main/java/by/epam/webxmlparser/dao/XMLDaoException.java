package by.epam.webxmlparser.dao;

import by.epam.webxmlparser.exception.WebXmlParserException;

public class XMLDaoException extends WebXmlParserException {
    public XMLDaoException(String message){
        super(message);
    }
    public XMLDaoException(String message, Exception ex){
        super(message,ex);
    }
}
