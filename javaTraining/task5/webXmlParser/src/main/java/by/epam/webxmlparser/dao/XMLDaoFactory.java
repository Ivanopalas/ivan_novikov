package by.epam.webxmlparser.dao;

import by.epam.webxmlparser.dao.impl.XMLDomParser;
import by.epam.webxmlparser.dao.impl.XMLSaxParser;
import by.epam.webxmlparser.dao.impl.XMLStaxParser;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.HashMap;
import java.util.Map;

public class XMLDaoFactory {
    private final static Logger logger = LogManager.getLogger(XMLDaoFactory.class);
    private static final XMLDaoFactory instance = new XMLDaoFactory();
    private boolean isDebugEnable = true;
    private Map<XMLParserType,XMLParser> parsers;

    private XMLDaoFactory(){
        parsers = new HashMap<XMLParserType, XMLParser>();
        parsers.put(XMLParserType.SAX_PARSER,new XMLSaxParser());
        parsers.put(XMLParserType.STAX_PARSER,new XMLStaxParser());
        parsers.put(XMLParserType.DOM_PARSER,new XMLDomParser());
    }
    public static XMLDaoFactory getInstance(){
        return instance;
    }
    public XMLParser getXMLParser(XMLParserType type) throws XMLDaoException{
        XMLParser result;
        result = parsers.get(type);
        if(isDebugEnable){
            logger.debug("XMLFabric made" + result.getClass());
        }
        return result;
    }
    public void setDebugEnable(boolean isDebugEnable) {
        this.isDebugEnable = isDebugEnable;
    }
}
