package by.epam.webxmlparser.dao;

import by.epam.webxmlparser.entity.XMLData;

public interface XMLParser {
    public XMLData parse(String resourceLocation) throws XMLDaoException;
}
