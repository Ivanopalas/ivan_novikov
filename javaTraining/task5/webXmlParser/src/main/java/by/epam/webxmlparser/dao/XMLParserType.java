package by.epam.webxmlparser.dao;

public enum XMLParserType {
    SAX_PARSER,STAX_PARSER,DOM_PARSER
}
