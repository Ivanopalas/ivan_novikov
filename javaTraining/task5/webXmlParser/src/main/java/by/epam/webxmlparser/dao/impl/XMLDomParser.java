package by.epam.webxmlparser.dao.impl;

import by.epam.webxmlparser.dao.XMLDaoException;
import by.epam.webxmlparser.dao.XMLParser;
import by.epam.webxmlparser.dao.impl.util.BeerContentParser;
import by.epam.webxmlparser.entity.XMLData;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.w3c.dom.*;
import org.xml.sax.SAXException;



import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class XMLDomParser implements XMLParser {
    private final static Logger logger = LogManager.getLogger(XMLDomParser.class);
    private XMLData parsedData;
    private File resource;

    public XMLData parse(String resourceLocation) throws XMLDaoException{
        try {
            resource = new File(resourceLocation);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setIgnoringElementContentWhitespace(true);
            DocumentBuilder dBuilder = factory.newDocumentBuilder();
            Document doc = dBuilder.parse(resource);
            doc.getDocumentElement().normalize();
            parsedData = new XMLData();
            Element root = doc.getDocumentElement();
            parsedData.setBeer("-");
            parsedData.setBeerAttr(root.getAttribute("id"));
            if (doc.hasChildNodes()){
                getNextChildData(root.getChildNodes());
            }
        }catch (ParserConfigurationException ex){
            String message = "cant config parser";
            logger.error(message);
            throw  new XMLDaoException(message,ex);
        }catch (SAXException ex){
            String message = "SAX error while parsing " + resourceLocation;
            logger.error(message);
            throw  new XMLDaoException(message,ex);
        }catch (IOException ex){
            String message = "i/o exception while parsing" + resourceLocation;
            logger.error(message);
            throw  new XMLDaoException(message,ex);
        }
        return parsedData;
    }
    private void getNextChildData(NodeList nodeList){
        for(int i = 0 ; i < nodeList.getLength(); i++){
            Node curNode = nodeList.item(i);
            if(curNode.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element)curNode;
                BeerContentParser.getInstance().putData(element.getTextContent(),element.getTagName(),parsedData);
                if(element.hasChildNodes()){
                    getNextChildData(curNode.getChildNodes());
                }
            }
        }

    }
}
