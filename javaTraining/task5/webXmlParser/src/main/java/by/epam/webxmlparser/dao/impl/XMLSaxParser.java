package by.epam.webxmlparser.dao.impl;

import by.epam.webxmlparser.dao.XMLDaoException;
import by.epam.webxmlparser.dao.XMLParser;
import by.epam.webxmlparser.dao.impl.util.BeerContentParser;
import by.epam.webxmlparser.entity.XMLData;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class XMLSaxParser extends DefaultHandler implements XMLParser {
    private final static Logger logger = LogManager.getLogger(XMLSaxParser.class);
    private boolean isDebugEnable = true;
    private XMLData parsedData;
    private String currentTag;
    private File resource;
    private boolean isOpenTag;

    public XMLData parse(String resourceLocation) throws XMLDaoException{
        try {
            this.resource = new File(resourceLocation);
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setNamespaceAware(true);
            SAXParser saxParser = spf.newSAXParser();
            saxParser.parse(new FileInputStream(this.resource), this);
        }catch (FileNotFoundException ex){
            String message = "cant find file " + resourceLocation;
            logger.error(message);
            throw  new XMLDaoException(message,ex);
        }catch (ParserConfigurationException ex){
            String message = "cant config parser";
            logger.error(message);
            throw  new XMLDaoException(message,ex);
        }catch (SAXException ex){
            String message = "SAX error while parsing " + resourceLocation;
            logger.error(message);
            throw  new XMLDaoException(message,ex);
        }catch (IOException ex){
            String message = "i/o exception while parsing" + resourceLocation;
            logger.error(message);
            throw  new XMLDaoException(message,ex);
        }catch (Exception ex){
            String message = "unknown error";
            logger.error(message);
            throw  new XMLDaoException(message,ex);
        }
        return parsedData;
    }
    @Override
    public void startDocument() throws SAXException{
        if(isDebugEnable){
            logger.debug("start parsing document " + resource.getAbsolutePath());
        }
        parsedData = new XMLData();
    }

    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        currentTag = localName;
        if(localName.equals("beer")){
            parsedData.setBeerAttr(atts.getValue("id"));
        }
        isOpenTag = true;
    }
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if(isOpenTag){
            String dataInTag = new String(ch,start,length);
            BeerContentParser.getInstance().putData(dataInTag,currentTag,parsedData);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        isOpenTag = false;
    }

    @Override
    public void endDocument() throws SAXException{
        if(isDebugEnable){
            logger.debug("ending parsing document " + resource.getAbsolutePath());
        }
    }



    public void warning(SAXParseException spe) throws SAXException {
        if(isDebugEnable){
            logger.debug("SAX warning: "+spe.getMessage());
        }
    }
    public void error(SAXParseException spe) throws SAXException {
        logger.error("SAX error: " + spe.getMessage());
    }
    public void fatalError(SAXParseException spe) throws SAXException {
        logger.fatal("SAX fatal error: " + spe.getMessage());
    }
    public void setDebugEnable(boolean isDebugEnable) {
        this.isDebugEnable = isDebugEnable;
    }
}
