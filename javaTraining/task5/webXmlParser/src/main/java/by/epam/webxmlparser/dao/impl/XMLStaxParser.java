package by.epam.webxmlparser.dao.impl;

import by.epam.webxmlparser.dao.XMLDaoException;
import by.epam.webxmlparser.dao.XMLParser;
import by.epam.webxmlparser.dao.impl.util.BeerContentParser;
import by.epam.webxmlparser.entity.XMLData;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class XMLStaxParser implements XMLParser {
    private Logger logger = LogManager.getLogger(XMLStaxParser.class);
    private XMLData parsedData;
    private String currentTag;
    public XMLData parse(String resourceLocation) throws XMLDaoException{
        XMLInputFactory factory = XMLInputFactory.newInstance();
        try {
            FileInputStream in = new FileInputStream(resourceLocation);
            XMLStreamReader reader = factory.createXMLStreamReader(in);
            getData(reader);
        }catch (FileNotFoundException ex) {
            String message = "cant find file " + resourceLocation;
            logger.error(message);
            throw new XMLDaoException(message, ex);
        }catch (XMLStreamException ex){
            String message = "StAX error while parsing " + resourceLocation;
            logger.error(message);
            throw  new XMLDaoException(message,ex);
        }
        return parsedData;
    }
    private void getData(XMLStreamReader reader) throws XMLStreamException{
            while (reader.hasNext()) {
                int event = reader.getEventType();
                switch (event){
                    case XMLStreamConstants.START_DOCUMENT:
                        parsedData = new XMLData();
                        break;
                    case XMLStreamConstants.START_ELEMENT:
                        currentTag = reader.getName().getLocalPart();
                        if(currentTag.equals("beer")) {
                            parsedData.setBeerAttr(reader.getAttributeValue(1));
                        }
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        if(!reader.isWhiteSpace()){
                            BeerContentParser.getInstance().putData(
                                    reader.getText(),currentTag,parsedData);
                        }else {
                            if(currentTag.matches("beer|ingredients|chars|alcohol|vessels")){ 
                                BeerContentParser.getInstance().putData(
                                        reader.getText(),currentTag,parsedData);
                            }
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        break;
                }
                reader.next();
            }
    }
}
