package by.epam.webxmlparser.dao.impl.util;

import by.epam.webxmlparser.entity.XMLData;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class BeerContentParser {
    private final static Logger logger = LogManager.getLogger(BeerContentParser.class);
    private static BeerContentParser instance;

    private BeerContentParser(){}
    public static BeerContentParser getInstance(){
        if(instance == null){
            instance = new BeerContentParser();
        }
        return instance;
    }
    public void putData(String dataInTag, String currentTag,
                               XMLData parsedData){
        BeerTagName tagName = BeerTagName.UNKNOWN;
        try {
            tagName = BeerTagName.valueOf(currentTag.toUpperCase());
        }catch (IllegalArgumentException ex){
            logger.error("unknown tag " + currentTag);
        }
        switch (tagName){
            case BEER:
                parsedData.setBeer("-");
                break;
            case NAME:
                parsedData.setName(dataInTag);
                break;
            case TYPE:
                parsedData.setType(dataInTag);
                break;
            case MANUFACTURER:
                parsedData.setManufacturer(dataInTag);
                break;
            case INGREDIENTS:
                parsedData.setIngredients("-");
                break;
            case INGR:
                if(parsedData.getFirstIngr() == null){
                    parsedData.setFirstIngr(dataInTag);
                }else if(parsedData.getSecontIngr() == null){
                    parsedData.setSecontIngr(dataInTag);
                }else if(parsedData.getThirdIngr() == null){
                    parsedData.setThirdIngr(dataInTag);
                }
                break;
            case CHARS:
                parsedData.setChars("-");
                break;
            case ALCOHOL:
                parsedData.setAlcohol("-");
                break;
            case ALC:
                parsedData.setAlc(new Boolean(dataInTag));
                break;
            case ABV:
                if(parsedData.isAlc()){
                    parsedData.setAbv(Double.valueOf(dataInTag));
                }else {
                    parsedData.setAbv(0.0);
                }
                break;
            case TRANSPARENCY:
                parsedData.setTransparency(dataInTag);
                break;
            case CALORIES:
                parsedData.setCalories(Integer.valueOf(dataInTag));
                break;
            case FILTRED:
                parsedData.setFiltred(new Boolean(dataInTag));
                break;
            case VESSELS:
                parsedData.setVessels("-");
                break;
            case MATERIAL:
                parsedData.setMaterial(dataInTag);
                break;
            case VOL:
                parsedData.setVol(Double.valueOf(dataInTag));
                break;
            default:
                logger.error("didn't parse -> " + currentTag);
                break;
        }

    }
}
