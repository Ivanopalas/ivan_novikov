package by.epam.webxmlparser.entity;

public class XMLData {
    private String beer;
    private String beerAttr;
    private String name;
    private String type;
    private String manufacturer;
    private String ingredients;
    private String firstIngr;
    private String secontIngr;
    private String thirdIngr;
    private String chars;
    private String alcohol;
    private boolean alc;
    private double abv;
    private String transparency;
    private int calories;
    private boolean filtred;
    private String vessels;
    private String material;
    private double vol;

    public String getBeer() {
        return beer;
    }

    public void setBeer(String beer) {
        this.beer = beer;
    }

    public String getBeerAttr() {
        return beerAttr;
    }

    public void setBeerAttr(String beerAttr) {
        this.beerAttr = beerAttr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getFirstIngr() {
        return firstIngr;
    }

    public void setFirstIngr(String firstIngr) {
        this.firstIngr = firstIngr;
    }

    public String getSecontIngr() {
        return secontIngr;
    }

    public void setSecontIngr(String secontIngr) {
        this.secontIngr = secontIngr;
    }

    public String getThirdIngr() {
        return thirdIngr;
    }

    public void setThirdIngr(String thirdIngr) {
        this.thirdIngr = thirdIngr;
    }

    public String getChars() {
        return chars;
    }

    public void setChars(String chars) {
        this.chars = chars;
    }

    public String getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(String alcohol) {
        this.alcohol = alcohol;
    }

    public boolean isAlc() {
        return alc;
    }

    public void setAlc(boolean alc) {
        this.alc = alc;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getTransparency() {
        return transparency;
    }

    public void setTransparency(String transparency) {
        this.transparency = transparency;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public boolean isFiltred() {
        return filtred;
    }

    public void setFiltred(boolean filtred) {
        this.filtred = filtred;
    }

    public String getVessels() {
        return vessels;
    }

    public void setVessels(String vessels) {
        this.vessels = vessels;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public double getVol() {
        return vol;
    }

    public void setVol(double vol) {
        this.vol = vol;
    }

    @Override

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        XMLData xmlData = (XMLData) o;

        if (Double.compare(xmlData.abv, abv) != 0) return false;
        if (alc != xmlData.alc) return false;
        if (calories != xmlData.calories) return false;
        if (filtred != xmlData.filtred) return false;
        if (Double.compare(xmlData.vol, vol) != 0) return false;
        if (alcohol != null ? !alcohol.equals(xmlData.alcohol) : xmlData.alcohol != null) return false;
        if (beer != null ? !beer.equals(xmlData.beer) : xmlData.beer != null) return false;
        if (beerAttr != null ? !beerAttr.equals(xmlData.beerAttr) : xmlData.beerAttr != null) return false;
        if (chars != null ? !chars.equals(xmlData.chars) : xmlData.chars != null) return false;
        if (firstIngr != null ? !firstIngr.equals(xmlData.firstIngr) : xmlData.firstIngr != null) return false;
        if (ingredients != null ? !ingredients.equals(xmlData.ingredients) : xmlData.ingredients != null) return false;
        if (manufacturer != null ? !manufacturer.equals(xmlData.manufacturer) : xmlData.manufacturer != null)
            return false;
        if (material != null ? !material.equals(xmlData.material) : xmlData.material != null) return false;
        if (name != null ? !name.equals(xmlData.name) : xmlData.name != null) return false;
        if (secontIngr != null ? !secontIngr.equals(xmlData.secontIngr) : xmlData.secontIngr != null) return false;
        if (thirdIngr != null ? !thirdIngr.equals(xmlData.thirdIngr) : xmlData.thirdIngr != null) return false;
        if (transparency != null ? !transparency.equals(xmlData.transparency) : xmlData.transparency != null)
            return false;
        if (type != null ? !type.equals(xmlData.type) : xmlData.type != null) return false;
        if (vessels != null ? !vessels.equals(xmlData.vessels) : xmlData.vessels != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = beer != null ? beer.hashCode() : 0;
        result = 31 * result + (beerAttr != null ? beerAttr.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (manufacturer != null ? manufacturer.hashCode() : 0);
        result = 31 * result + (ingredients != null ? ingredients.hashCode() : 0);
        result = 31 * result + (firstIngr != null ? firstIngr.hashCode() : 0);
        result = 31 * result + (secontIngr != null ? secontIngr.hashCode() : 0);
        result = 31 * result + (thirdIngr != null ? thirdIngr.hashCode() : 0);
        result = 31 * result + (chars != null ? chars.hashCode() : 0);
        result = 31 * result + (alcohol != null ? alcohol.hashCode() : 0);
        result = 31 * result + (alc ? 1 : 0);
        temp = Double.doubleToLongBits(abv);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (transparency != null ? transparency.hashCode() : 0);
        result = 31 * result + calories;
        result = 31 * result + (filtred ? 1 : 0);
        result = 31 * result + (vessels != null ? vessels.hashCode() : 0);
        result = 31 * result + (material != null ? material.hashCode() : 0);
        temp = Double.doubleToLongBits(vol);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
