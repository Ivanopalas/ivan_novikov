package by.epam.webxmlparser.exception;

public class WebXmlParserException extends Exception{
    private static final long serialVersionUID = 1L;
    private Exception hiddenException;
    public WebXmlParserException(String massage){
        super(massage);
    }
    public WebXmlParserException(String message,Exception ex){
        super(message,ex);
        hiddenException = ex;
    }
    public Exception getHiddenException(){
        return hiddenException;
    }
}
