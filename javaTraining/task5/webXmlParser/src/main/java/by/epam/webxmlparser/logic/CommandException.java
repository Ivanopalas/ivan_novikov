package by.epam.webxmlparser.logic;

public class CommandException extends Exception{
    public CommandException(Exception ex){
        super(ex);
    }
    public CommandException(Exception ex,String message){
        super(message,ex);
    }
}
