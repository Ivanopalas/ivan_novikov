package by.epam.webxmlparser.logic.impl;

import by.epam.webxmlparser.controller.JspName;
import by.epam.webxmlparser.logic.CommandException;
import by.epam.webxmlparser.logic.ICommand;

import javax.servlet.http.HttpServletRequest;

public class NoCommand implements ICommand{
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        return JspName.INDEX_PAGE;
    }
}
