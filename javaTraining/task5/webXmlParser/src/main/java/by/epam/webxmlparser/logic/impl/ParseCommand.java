package by.epam.webxmlparser.logic.impl;

import by.epam.webxmlparser.controller.JspName;
import by.epam.webxmlparser.controller.RequestParameterName;
import by.epam.webxmlparser.dao.XMLDaoException;
import by.epam.webxmlparser.dao.XMLDaoFactory;
import by.epam.webxmlparser.dao.XMLParser;
import by.epam.webxmlparser.dao.XMLParserType;
import by.epam.webxmlparser.entity.XMLData;
import by.epam.webxmlparser.logic.CommandException;
import by.epam.webxmlparser.logic.ICommand;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import javax.servlet.http.HttpServletRequest;

public class ParseCommand implements ICommand {
    private Logger logger = LogManager.getLogger(ParseCommand.class);

    @Override
    public String execute(HttpServletRequest request)throws CommandException{
        String parserTypeName = request.getParameter(RequestParameterName.PARSER_TYPE).toUpperCase();
        XMLParserType parserType = XMLParserType.valueOf(parserTypeName);
        try{
            XMLParser parser = XMLDaoFactory.getInstance().getXMLParser(parserType);
            String filename = request.getParameter(RequestParameterName.FILE_NAME);
            String resourceLocation = getClass().getResource("/").getPath() + filename;
            System.out.println(resourceLocation);
            XMLData data;
            data = parser.parse(resourceLocation);
            request.setAttribute(RequestParameterName.PAGE_DATA,data);
        }catch (XMLDaoException ex){
            logger.error("executing request error",ex);
            throw new CommandException(ex);
        }
        return JspName.USER_PAGE;
    }
}
