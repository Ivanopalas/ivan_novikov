<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <title>RESULT</title>
  <link type="text/css" rel="stylesheet" href="<c:url value="/css/tableSet.css" />" />
</head>

<body>
  <table class="example" border="1">
    <tr><td align="center" colspan="2"><b><c:out value="${param.parserType}"/></b></td></tr>
      <jsp:useBean id="pageData" class="by.epam.webxmlparser.entity.XMLData" scope="request"/>
        <tr>
          <td><b>beer: </b><i>id = </i><jsp:getProperty name="pageData" property="beerAttr"/></td>
          <td align="center"><jsp:getProperty name="pageData" property="beer"/></td>
        </tr>
        <tr>
          <td>  name:</td>
          <td><jsp:getProperty name="pageData" property="name"/></td>
        </tr>
        <tr>
          <td>  type:</td>
          <td><jsp:getProperty name="pageData" property="type"/></td>
        </tr>
        <tr>
          <td>  manufacturer:</td>
          <td><jsp:getProperty name="pageData" property="manufacturer"/></td>
        </tr>
        <tr>
          <td>  <b>ingredients:</b></td>
          <td align="center"><jsp:getProperty name="pageData" property="ingredients"/></td>
        </tr>
        <tr>
          <td>   <i>ingr:</i></td>
          <td><jsp:getProperty name="pageData" property="firstIngr"/></td>
        </tr>
        <tr>
          <td>   <i>ingr:</i></td>
          <td><jsp:getProperty name="pageData" property="secontIngr"/></td>
        </tr>
        <tr>
          <td>   <i>ingr:</i></td>
          <td><jsp:getProperty name="pageData" property="thirdIngr"/></td>
        </tr>
        <tr>
          <td>  <b>chars:</b></td>
          <td align="center"><jsp:getProperty name="pageData" property="chars"/></td>
        </tr>
        <tr>
          <td>   <b>alcohol:</b></td>
          <td align="center"><jsp:getProperty name="pageData" property="alcohol"/></td>
        </tr>
        <tr>
          <td>    alc:</td>
          <td><jsp:getProperty name="pageData" property="alc"/></td>
        </tr>
        <tr>
          <td>    abv:</td>
          <td><jsp:getProperty name="pageData" property="abv"/></td>
        </tr>
        <tr>
          <td>   transparency:</td>
          <td><jsp:getProperty name="pageData" property="transparency"/></td>
        </tr>
        <tr>
          <td>   calories:</td>
          <td><jsp:getProperty name="pageData" property="calories"/></td>
        </tr>
        <tr>
          <td>   filtred:</td>
          <td><jsp:getProperty name="pageData" property="filtred"/></td>
        </tr>
        <tr>
          <td>   <b>vessels:</b></td>
          <td align="center"><jsp:getProperty name="pageData" property="vessels"/></td>
        </tr>
        <tr>
          <td>    material:</td>
          <td><jsp:getProperty name="pageData" property="material"/></td>
        </tr>
        <tr>
          <td>    vol:</td>
          <td><jsp:getProperty name="pageData" property="vol"/></td>
        </tr>

  </table>
  <form action="result" method="post" align="center">
    <input type="hidden" name="commandName" value="no_command" />
    <input type="submit" value="index page">
  </form>
</body>
</html>
