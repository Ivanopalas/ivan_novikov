package com.epam.it_company.dao.daofactory;

import com.epam.it_company.dao.ClientDao;
import com.epam.it_company.dao.DeveloperDao;
import com.epam.it_company.dao.ManagerDao;
import com.epam.it_company.dao.UserDao;
import com.epam.it_company.dao.impl.DBClientDao;
import com.epam.it_company.dao.impl.DBDeveloperDao;
import com.epam.it_company.dao.impl.DBManagerDao;
import com.epam.it_company.dao.impl.DBUserDao;

/**
 * Dao factory realization for data base data access object.
 */
public class DBDaoFactory extends DaoFactory {
    private static final DBDaoFactory dbDaoFactory = new DBDaoFactory();

    public static DBDaoFactory getInstance(){
        return dbDaoFactory;
    }

    @Override
    public ClientDao getClientDao() {
        return DBClientDao.getInstance();
    }

    @Override
    public DeveloperDao getDeveloperDao() {
        return DBDeveloperDao.getInstance();
    }

    @Override
    public ManagerDao getManagerDao() {
        return DBManagerDao.getInstance();
    }

    @Override
    public UserDao getUserDao() { return DBUserDao.getInstance();    }
}
