package com.epam.it_company.dao.impl;

import com.epam.it_company.controller.UserType;
import com.epam.it_company.dao.DaoException;
import com.epam.it_company.dao.UserDao;
import com.epam.it_company.dao.pool.ConnectionPool;
import com.epam.it_company.dao.pool.ConnectionPoolException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBUserDao implements UserDao {
    private static final DBUserDao dbUserDao= new DBUserDao();

    private static final String EXCEPTION_CONNECT_MESSAGE =
            "Can't get connection from connection pool";
    private static final String EXCEPTION_ACT_MESSAGE =
            "Sql actions exception";

    private final static String GET_LOGIN_PASS_QUERY =
            "SELECT login,password,type FROM itcompanybd.User" +
            " JOIN itcompanybd.User_type ON User.idUser=User_type.idUser;";

    public static DBUserDao getInstance(){
        return dbUserDao;
    }

    @Override
    public String login(String login, String password) throws DaoException{
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            connection = pool.takeConnection();
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            resultSet = statement.executeQuery(GET_LOGIN_PASS_QUERY);
            while (resultSet.next()){
                String bdLogin = resultSet.getString(DatabaseColumnName.LOGIN);
                String bdPassword =  resultSet.getString(DatabaseColumnName.PASSWORD);
                if( bdLogin.equals(login) && bdPassword.equals(password)){
                    String userType = resultSet.getString(DatabaseColumnName.USER_TYPE);
                    return userType;
                }
            }
        }catch (ConnectionPoolException ex){
            throw new DaoException(EXCEPTION_CONNECT_MESSAGE,ex);
        }catch (SQLException ex){
            throw new DaoException(EXCEPTION_ACT_MESSAGE,ex);
        }finally {
            pool.closeConnection(connection,statement,resultSet);
        }
        return UserType.UNLOGINED.toString();
    }
}
